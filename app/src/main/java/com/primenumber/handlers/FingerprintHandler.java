package com.primenumber.handlers;

import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.hardware.fingerprint.FingerprintManager;
import android.os.Build;
import android.os.CancellationSignal;
import android.widget.TextView;

import com.primenumber.activities.SensorReadingActivity;
import com.primenumber.activities.UnlockActivity;
import com.primenumber.data.Database;
import com.primenumber.dividedbyzero.R;
import com.primenumber.services.SecuritySmsService;
import com.primenumber.services.VehicleLockService;

@TargetApi(Build.VERSION_CODES.M)
public class FingerprintHandler extends FingerprintManager.AuthenticationCallback{

    private Context context;
    private Database database;

    public FingerprintHandler(Context context){
        this.context = context;
        database = Database.getInstance(context);
    }

    public void startAuth(FingerprintManager fingerprintManager, FingerprintManager.CryptoObject cryptoObject){

        CancellationSignal cancellationSignal = new CancellationSignal();

        fingerprintManager.authenticate(cryptoObject, cancellationSignal,0, this, null);
    }

    @Override
    public void onAuthenticationError(int errorCode, CharSequence errString) {
        this.update("There was an Auth error " + errString, false);
    }

    @Override
    public void onAuthenticationFailed() {
        this.update("Auth failed", false);
    }

    @Override
    public void onAuthenticationHelp(int helpCode, CharSequence helpString) {
        this.update("Error " + helpString, false);
    }

    @Override
    public void onAuthenticationSucceeded(FingerprintManager.AuthenticationResult result) {
        this.update("You can access the app", true);
        database.updateLockState(0);
        Intent lockService = new Intent(context, VehicleLockService.class);
        context.stopService(lockService);
        Intent securitySmsService = new Intent(context, SecuritySmsService.class);
        context.stopService(securitySmsService);
        Intent intent = new Intent(context, SensorReadingActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        context.startActivity(intent);
    }

    private void update(String message, boolean bool){
        TextView explanation = ((Activity)context).findViewById(R.id.explanation);

        explanation.setText(message);
    }
}
