package com.primenumber.views;

import android.content.Context;
import android.graphics.BlurMaskFilter;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.RectF;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.GradientDrawable;
import android.util.Log;
import android.view.View;

import com.primenumber.data.Database;
import com.primenumber.data.models.CarDetails;
import com.primenumber.dividedbyzero.R;

import android.os.Handler;

import static android.content.res.Configuration.ORIENTATION_LANDSCAPE;

/**
 * Created by novica on 2/11/18.
 */

public class GaugeView extends View {

    private Database database;
    private CarDetails carDetails;
    private Paint gaugePaint, textPaint;
    private int gaugeLimit;
    private String title;
    private float reading, oldReading;
    private Drawable icon;
    private Handler handler;
    private RectF bounds;
    private int lineLength, maxLineLength;
    private int gaugePoints;
    private int thinLine, mediumLine, thickLine, smallText, mediumText, largeText, extraLargeText, superThinLine, superExtraLargeText;
    private float centerX, centerY;
    private float gaugeStartAngle, gaugeSweep, needleAngle, oldNeedleAngle;

    public GaugeView(Context context){
        super(context);
        database = Database.getInstance(context);
        handler = new Handler();
        initialize();
    }

    private Runnable r = new Runnable() {
        @Override
        public void run() {
            invalidate();
        }
    };

    public double roundFloat(float number){
        return Math.round(number*10.0)/10.0;
    }

    private void initialize(){
        carDetails = database.getCarDetails();
        superThinLine = getResources().getDimensionPixelSize(R.dimen.canvas_super_thin_line);
        thinLine = getResources().getDimensionPixelSize(R.dimen.canvas_thin_line);
        mediumLine = getResources().getDimensionPixelSize(R.dimen.canvas_medium_line);
        thickLine = getResources().getDimensionPixelSize(R.dimen.canvas_thick_line);
        smallText = getResources().getDimensionPixelSize(R.dimen.canvas_text_size_small);
        mediumText = getResources().getDimensionPixelSize(R.dimen.canvas_text_size_medium);
        largeText = getResources().getDimensionPixelSize(R.dimen.canvas_text_size_big);
        extraLargeText = getResources().getDimensionPixelSize(R.dimen.canvas_text_extra_large);
        superExtraLargeText = getResources().getDimensionPixelSize(R.dimen.canvas_super_extra_large_text);
        gaugeStartAngle = 30;
        gaugeSweep = 240;
        needleAngle = -30;
        oldNeedleAngle = -30;
        reading = 0;
        gaugePaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        gaugePaint.setStyle(Paint.Style.STROKE);
        gaugePaint.setStrokeWidth(thinLine);
        gaugePaint.setStrokeCap(Paint.Cap.ROUND);
        gaugePaint.setColor(getResources().getColor(R.color.primaryText));

        textPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        textPaint.setStyle(Paint.Style.STROKE);
        textPaint.setStrokeWidth(superThinLine);
        textPaint.setTextSize(largeText);
        textPaint.setStrokeCap(Paint.Cap.ROUND);
        textPaint.setColor(getResources().getColor(R.color.primaryText));

        maxLineLength = getResources().getDimensionPixelSize(R.dimen.gauge_long_line_length);
        lineLength = getResources().getDimensionPixelSize(R.dimen.gauge_short_line_length);
        bounds = new RectF();
        gaugePoints = carDetails.getMaxSpeed()/10;
    }

    public void overSpeed(float reading){
        if(reading >= 160){
            gaugePaint.setColor(getResources().getColor(R.color.danger));
            textPaint.setColor(getResources().getColor(R.color.danger));
        }
        else{
            gaugePaint.setColor(getResources().getColor(R.color.primaryText));
            textPaint.setColor(getResources().getColor(R.color.primaryText));
        }
    }

    public void setNeedleAngle(float reading){
        oldReading = this.reading;
        oldNeedleAngle = (this.reading)*240/carDetails.getMaxSpeed()-30;
        this.reading = reading;
    }

    public int needleMovement(float oldNeedleAngle, float needleAngle){
        if(Math.abs(oldNeedleAngle - needleAngle) > 3){
            return 3;
        }
        return 1;
    }

    public void setTitle(String title){
        this.title = title;
    }

    public void setIcon(Drawable icon){
        this.icon = icon;
    }

    @Override
    protected void onDraw(Canvas canvas) {
        int drawLine;
        int width = this.getWidth();
        int height = this.getHeight();

        int margin = thickLine;
        overSpeed(reading);
        centerX = width/2;
        centerY = height/2;
        canvas.drawColor(getResources().getColor(R.color.textIcons));

        bounds.set(margin, centerY-centerX+margin, width-margin, centerY+centerX-margin);
        canvas.drawArc(bounds, gaugeStartAngle, -240, false, gaugePaint);
        textPaint.setTextSize(superExtraLargeText);

        canvas.drawText("Speed", extraLargeText, 2*extraLargeText, textPaint);
        textPaint.setTextSize(largeText);
        canvas.save();
        float rotation = 0;
        canvas.rotate(-gaugeStartAngle, centerX, centerY);
        rotation+=-gaugeStartAngle;
        for (int i = 0; i <= gaugePoints; i++) {
            if(i%4 == 0){
                drawLine = maxLineLength;
                canvas.rotate(-rotation, margin+drawLine+thickLine+textPaint.measureText(String.valueOf(i*10))/2, centerY-largeText/2);
                canvas.drawText(String.valueOf(i*10), margin+drawLine+thickLine, centerY, textPaint);
                canvas.rotate(rotation, margin+drawLine+thickLine+textPaint.measureText(String.valueOf(i*10))/2, centerY-largeText/2);
            }else{
                drawLine = lineLength;
            }
            canvas.drawLine(margin, centerY, drawLine+margin, centerY, gaugePaint);
            canvas.rotate((float)240/gaugePoints, centerX, centerY);
            rotation+=(float)240/gaugePoints;
        }
        canvas.restore();
        canvas.save();
        gaugePaint.setStrokeWidth(mediumLine);

        //speed numbers
        textPaint.setTextSize(superExtraLargeText);
        canvas.drawText(String.valueOf(roundFloat(reading))+" Km/h",
                centerX-textPaint.measureText(String.valueOf(roundFloat(reading))+" Km/h")/2,
                centerX+centerY/2+2*superExtraLargeText,
                textPaint);
        canvas.drawText(String.valueOf(roundFloat(reading*5/18))+" m/s",
                centerX-textPaint.measureText(String.valueOf(roundFloat(reading*5/18))+" m/s")/2,
                centerX+centerY/2+3*superExtraLargeText,
                textPaint);

        canvas.drawCircle(centerX, centerY, thinLine, gaugePaint);

        if(Math.round(oldNeedleAngle) > Math.round(needleAngle)){
            canvas.rotate(needleAngle+=needleMovement(oldNeedleAngle, needleAngle), centerX, centerY);
            canvas.drawLine(centerX, centerY, margin+maxLineLength, centerY, gaugePaint);
            canvas.restore();
        }else if(Math.round(oldNeedleAngle) < Math.round(needleAngle)){
            canvas.rotate(needleAngle-=needleMovement(oldNeedleAngle, needleAngle), centerX, centerY);
            canvas.drawLine(centerX, centerY, margin+maxLineLength, centerY, gaugePaint);
            canvas.restore();
        }else{
            canvas.rotate(needleAngle, centerX, centerY);
            canvas.drawLine(centerX, centerY, margin+maxLineLength, centerY, gaugePaint);
            canvas.restore();
        }

        //speedometer needle


        gaugePaint.setStrokeWidth(thinLine);

        if(reading >= 160) {
            textPaint.setTextSize(superExtraLargeText*2);
            canvas.drawText("SLOW DOWN", centerX - textPaint.measureText("SLOW DOWN") / 2, height - largeText, textPaint);
            textPaint.setTextSize(largeText);
        }

        handler.postDelayed(r, 10);
    }
}
