package com.primenumber.activities;

import android.Manifest;
import android.app.FragmentManager;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.NavigationView;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.WindowManager;

import com.primenumber.application.DividedByZero;
import com.primenumber.dividedbyzero.R;
import com.primenumber.fragmentsandadapters.sosfragment.SosFragment;
import com.primenumber.fragmentsandadapters.vehicleservice.VehicleServiceCreateFragment;
import com.primenumber.fragmentsandadapters.vehicleservice.VehicleServiceDetailsFragment;
import com.primenumber.fragmentsandadapters.vehicleservice.VehicleServiceListFragment;

/**
 * Created by novica on 2/21/18.
 */

public class VehicleServiceActivity extends AppCompatActivity implements
        VehicleServiceCreateFragment.Listener, VehicleServiceListFragment.OnItemSelectedListener,
        VehicleServiceDetailsFragment.ItemSelectedInterface, NavigationView.OnNavigationItemSelectedListener,
        SosFragment.SosClickListener{

    private static final int CALENDAR_ACCESS_GRANTED = 1;
    private ActionBarDrawerToggle toggle;
    private VehicleServiceCreateFragment createFragment;
    private VehicleServiceListFragment listFragment;
    private VehicleServiceDetailsFragment detailsFragment;
    private FragmentManager fragmentManager;
    private DrawerLayout drawerLayout;
    private SosFragment sosFragment;
    private boolean isTablet;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_vehicle_service_layout);
        setTitle("Service history");
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        isTablet = getResources().getBoolean(R.bool.isTablet);
        fragmentManager = getFragmentManager();
        createFragment = new VehicleServiceCreateFragment();
        listFragment = new VehicleServiceListFragment();
        detailsFragment = new VehicleServiceDetailsFragment();
        sosFragment = new SosFragment();

        fragmentManager.beginTransaction()
                .replace(R.id.fragment_holder, listFragment)
                .replace(R.id.sos_fragment, sosFragment)
                .commit();

        Toolbar toolbar = findViewById(R.id.toolbar);

        drawerLayout = findViewById(R.id.drawer_layout);
        if(!isTablet) {
            toggle = new ActionBarDrawerToggle(this, drawerLayout, toolbar,
                    R.string.navigation_drawer_open, R.string.navigation_drawer_close);
            drawerLayout.addDrawerListener(toggle);
            toggle.syncState();
        }

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        NavigationView navigationView = findViewById(R.id.navigation_view);
        navigationView.setNavigationItemSelectedListener(this);

        if( Build.VERSION.SDK_INT >= Build.VERSION_CODES.M ) {
            if(ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_CALENDAR) != PackageManager.PERMISSION_GRANTED) {
                requestPermissions(new String[] { Manifest.permission.WRITE_CALENDAR}, CALENDAR_ACCESS_GRANTED);
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if(requestCode == 1){
            if(grantResults[0] != PackageManager.PERMISSION_GRANTED) {
                finish();
            }
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (toggle.onOptionsItemSelected(item)){
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void newEntryStored() {
        fragmentManager.popBackStack();
        fragmentManager.beginTransaction()
                .replace(R.id.fragment_holder, listFragment)
                .commit();
    }

    @Override
    public void createNew() {
        fragmentManager.beginTransaction()
                .replace(R.id.fragment_holder, createFragment)
                .addToBackStack(null)
                .commit();
    }

    @Override
    public void serviceSelected(int id) {
        Bundle bundle = new Bundle();
        bundle.putInt("id", id);
        detailsFragment.setArguments(bundle);
        fragmentManager.beginTransaction()
                .replace(R.id.fragment_holder, detailsFragment)
                .addToBackStack(null)
                .commit();
    }

    @Override
    public void itemSelected(int id) {
        detailsFragment.getServiceHistory(id);
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        int id = item.getItemId();

        switch (id){
            case R.id.sensor_reading:
                Intent sensorReading = new Intent(VehicleServiceActivity.this, SensorReadingActivity.class);
                startActivity(sensorReading);
                break;
            case R.id.emergency_sms:
                Intent emergencySms = new Intent(VehicleServiceActivity.this, EmergencySmsActivity.class);
                startActivity(emergencySms);
                break;
            case R.id.vehicle_details:
                Intent vehicleDetails = new Intent(VehicleServiceActivity.this, VehicleDetailsActivity.class);
                startActivity(vehicleDetails);
                break;
            case R.id.service_history:
                break;
            case R.id.bluetooth:
                Intent bluetooth = new Intent(VehicleServiceActivity.this, BluetoothActivity.class);
                startActivity(bluetooth);
                break;
            case R.id.diagnostics:
                Intent diagnostics = new Intent(VehicleServiceActivity.this, DiagnosticsActivity.class);
                startActivity(diagnostics);
                break;
            case R.id.dash_cam:
                Intent dashCam = new Intent(VehicleServiceActivity.this, DashCamActivity.class);
                startActivity(dashCam);
                break;
            case R.id.controls:
                Intent controls = new Intent(VehicleServiceActivity.this, ControlsActivity.class);
                startActivity(controls);
                break;
            case R.id.owner_details:
                Intent ownerDetails = new Intent(VehicleServiceActivity.this, OwnerDetailsActivity.class);
                startActivity(ownerDetails);
                break;
            case R.id.lock:
                Intent lock = new Intent(VehicleServiceActivity.this, PasswordLockActivity.class);
                startActivity(lock);
                break;
            case R.id.gauge:
                Intent gauge = new Intent(VehicleServiceActivity.this, GaugeActivity.class);
                startActivity(gauge);
                break;
        }
        if(!isTablet) {
            DrawerLayout drawerLayout = findViewById(R.id.drawer_layout);
            drawerLayout.closeDrawer(GravityCompat.START);
        }
        return true;
    }

    @Override
    public void onBackPressed() {
        if (drawerLayout.isDrawerOpen(GravityCompat.START)) {
            drawerLayout.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public void turnOnSos() {
        ((DividedByZero) getApplication()).setSosModeOn(true);
    }

    @Override
    public void turnOffSos() {
        ((DividedByZero) getApplication()).setSosModeOn(false);
    }
}
