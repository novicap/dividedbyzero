package com.primenumber.activities;

import android.Manifest;
import android.app.Activity;
import android.app.FragmentManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.graphics.Matrix;
import android.graphics.RectF;
import android.graphics.SurfaceTexture;
import android.hardware.camera2.CameraAccessException;
import android.hardware.camera2.CameraCaptureSession;
import android.hardware.camera2.CameraCharacteristics;
import android.hardware.camera2.CameraDevice;
import android.hardware.camera2.CameraManager;
import android.hardware.camera2.CaptureRequest;
import android.hardware.camera2.params.StreamConfigurationMap;
import android.hardware.display.DisplayManager;
import android.hardware.display.VirtualDisplay;
import android.media.MediaRecorder;
import android.media.projection.MediaProjection;
import android.media.projection.MediaProjectionManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.HandlerThread;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.NavigationView;
import android.support.design.widget.Snackbar;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.Size;
import android.util.SparseIntArray;
import android.view.MenuItem;
import android.view.Surface;
import android.view.TextureView;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;


import com.primenumber.application.DividedByZero;
import com.primenumber.dividedbyzero.R;
import com.primenumber.fragmentsandadapters.sosfragment.SosFragment;
import com.primenumber.services.AccelerometerService;
import com.primenumber.services.BluetoothOBD2Connection;
import com.primenumber.services.LocationService;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

public class DashCamActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener, SosFragment.SosClickListener{

    private static final int RECORDING_PERMISSIONS_GRANTED = 1;
    private static final int REQUEST_CODE = 3;
    private BluetoothOBD2Connection bluetoothOBD2Connection;
    private TextureView textureView;
    private TextureView.SurfaceTextureListener surfaceTextureListener = new TextureView.SurfaceTextureListener() {
        @Override
        public void onSurfaceTextureAvailable(SurfaceTexture surface, int width, int height) {
            setupCamera(width, height);

            transformImage(textureView.getWidth(), textureView.getHeight());
            if(!cameraConnected)
                cameraConnected = connectCamera();
        }

        @Override
        public void onSurfaceTextureSizeChanged(SurfaceTexture surface, int width, int height) {

        }

        @Override
        public boolean onSurfaceTextureDestroyed(SurfaceTexture surface) {
            if(cameraDevice != null){
                closeCamera();
                destroyMediaProjection();
                stopBackgroundThread();
            }
            if(isRecording){
                mediaRecorder.stop();
                mediaRecorder.reset();
                virtualDisplay.release();
                isRecording = false;
            }
            return false;
        }

        @Override
        public void onSurfaceTextureUpdated(SurfaceTexture surface) {

        }
    };
    private CameraDevice cameraDevice;
    private CameraDevice.StateCallback cameraDeviceStateCallback = new CameraDevice.StateCallback() {
        @Override
        public void onOpened(@NonNull CameraDevice camera) {
            cameraDevice = camera;
            if(cameraConnected)
                startPreview();
        }

        @Override
        public void onDisconnected(@NonNull CameraDevice camera) {
            camera.close();
            cameraDevice = null;
        }

        @Override
        public void onError(@NonNull CameraDevice camera, int error) {

        }
    };
    private String cameraId;
    private Size previewSize;
    private CaptureRequest.Builder captureRequestBuilder;
    private HandlerThread backgroundHandlerThread;
    private Handler backgroundHandler;
    private Button record, back;
    private boolean isRecording = false;
    private MediaRecorder mediaRecorder;
    private int screenDensity, screenWidth, screenHeight;
    private MediaProjectionManager projectionManager;
    private MediaProjection mediaProjection;
    private MediaProjectionCallback mediaProjectionCallback;
    private VirtualDisplay virtualDisplay;
    private Surface surface;
    private TextView speedVehicle, speedGps, gforce, rpm;
    private boolean cameraConnected = false, buttonsVisible = true;
    private File videoFolder;
    private String fileName;
    private SosFragment sosFragment;
    private FragmentManager fragmentManager;
    private DrawerLayout drawerLayout;
    private ActionBarDrawerToggle toggle;
    private BroadcastReceiver locationDataReceiver, accelerometerDataReceiver, obdDataReceiver;

    private static SparseIntArray ORIENTATIONS = new SparseIntArray();
    static {
        ORIENTATIONS.append(Surface.ROTATION_0, 0);
        ORIENTATIONS.append(Surface.ROTATION_90, 90);
        ORIENTATIONS.append(Surface.ROTATION_180, 180);
        ORIENTATIONS.append(Surface.ROTATION_270, 270);
    }

    private static class CompareSizeByArea implements Comparator<Size>{

        @Override
        public int compare(Size o1, Size o2) {
            return Long.signum((long) o1.getWidth() * o1.getHeight() /
                    (long) o2.getWidth() * o2.getHeight());
        }
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        setContentView(R.layout.activity_dash_cam_layout);
        createVideoFolder();
        sosFragment = new SosFragment();
        fragmentManager = getFragmentManager();
        fragmentManager.beginTransaction()
                .replace(R.id.sos_fragment, sosFragment)
                .commit();

        View decorView = getWindow().getDecorView();
        decorView.setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY
                | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                | View.SYSTEM_UI_FLAG_FULLSCREEN
                | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION);
        speedGps = findViewById(R.id.speedSpeed);
        speedVehicle = findViewById(R.id.speedSpeedSpeed);
        rpm = findViewById(R.id.rpm);
        gforce = findViewById(R.id.gforce);

        textureView = findViewById(R.id.textureView);
        record = findViewById(R.id.videoOnlineImageButton);
        back = findViewById(R.id.backButton);
        bluetoothOBD2Connection = ((DividedByZero) getApplication()).getBluetoothConnection();

        DisplayMetrics metrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(metrics);
        screenDensity = metrics.densityDpi;
        screenWidth = metrics.widthPixels;
        screenHeight = metrics.heightPixels;
        projectionManager = (MediaProjectionManager)getSystemService(Context.MEDIA_PROJECTION_SERVICE);
        mediaRecorder = new MediaRecorder();

        Toolbar toolbar = findViewById(R.id.toolbar);
        drawerLayout = findViewById(R.id.drawer_layout);
        toggle = new ActionBarDrawerToggle(this, drawerLayout, toolbar,
                R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawerLayout.addDrawerListener(toggle);
        toggle.syncState();

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        NavigationView navigationView = findViewById(R.id.navigation_view);
        navigationView.setNavigationItemSelectedListener(this);

        textureView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(isRecording && buttonsVisible){
                    hideButtons();
                } else {
                    showButtons();
                }
            }
        });

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        record.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onToggleScreenShare();
            }
        });
    }

    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);
        View decorView = getWindow().getDecorView();
        if(hasFocus) {
            decorView.setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                    | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY
                    | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                    | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                    | View.SYSTEM_UI_FLAG_FULLSCREEN
                    | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION);
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode != REQUEST_CODE) {
            return;
        }
        if (resultCode != RESULT_OK) {
            Toast.makeText(this, "Screen Cast Permission Denied", Toast.LENGTH_SHORT).show();
            isRecording = false;
            return;
        }
        prepareRecording();
        mediaProjectionCallback = new MediaProjectionCallback();
        mediaProjection = projectionManager.getMediaProjection(resultCode, data);
        mediaProjection.registerCallback(mediaProjectionCallback, null);
        virtualDisplay = createVirtualDisplay();
        mediaRecorder.start();
        hideButtons();
        record.setText(getString(R.string.stop_recording));
        isRecording = true;
    }

    @Override
    protected void onResume() {
        super.onResume();
        ((DividedByZero) getApplication()).setCameraView(true);
        startBackgroundThread();
        if(locationDataReceiver == null){
            locationDataReceiver = new BroadcastReceiver() {
                @Override
                public void onReceive(Context context, Intent intent) {
                    speedGps.setText(String.format("%.1f", (float)intent.getExtras().get("speed")*18/5)+"km/h (gps)");
                }
            };
        }
        if(accelerometerDataReceiver == null){
            accelerometerDataReceiver = new BroadcastReceiver() {
                @Override
                public void onReceive(Context context, Intent intent) {
                    gforce.setText(String.format("%.2f", intent.getExtras().get("gforce"))+" G");
                }
            };
        }
        if(obdDataReceiver == null){
            obdDataReceiver = new BroadcastReceiver() {
                @Override
                public void onReceive(Context context, Intent intent) {
                    speedVehicle.setText(String.valueOf(intent.getExtras().get("speed"))+ "km/h (vehicle)");
                    rpm.setText(String.valueOf(intent.getExtras().get("rpm")) + "RPM");
                }
            };
        }
        if(textureView.isAvailable()){
            setupCamera(textureView.getWidth(), textureView.getHeight());
            transformImage(textureView.getWidth(), textureView.getHeight());
            if(!cameraConnected) {
                cameraConnected = connectCamera();
            }
        } else {
            textureView.setSurfaceTextureListener(surfaceTextureListener);
        }

        registerReceiver(locationDataReceiver, new IntentFilter("location_service"));
        registerReceiver(accelerometerDataReceiver, new IntentFilter("gforce"));
        registerReceiver(obdDataReceiver, new IntentFilter("obd_reading"));
    }

    @Override
    protected void onPause() {
        closeCamera();
        cameraConnected = false;
        stopBackgroundThread();

        try{
            unregisterReceiver(locationDataReceiver);
        } catch (IllegalArgumentException e){
            e.printStackTrace();
        }

        try{
            unregisterReceiver(accelerometerDataReceiver);
        } catch (IllegalArgumentException e){
            e.printStackTrace();
        }

        try {
            unregisterReceiver(obdDataReceiver);
        } catch (IllegalArgumentException e){
            e.printStackTrace();
        }
        super.onPause();
    }

    private void setupCamera(int width, int height){
        CameraManager cameraManager = (CameraManager)getSystemService(Context.CAMERA_SERVICE);
        try {
            for(String cameraId : cameraManager.getCameraIdList()){
                CameraCharacteristics cameraCharacteristics = cameraManager.getCameraCharacteristics(cameraId);
                if(cameraCharacteristics.get(cameraCharacteristics.LENS_FACING) ==
                        cameraCharacteristics.LENS_FACING_FRONT){
                    continue;
                }
                StreamConfigurationMap map = cameraCharacteristics.get(CameraCharacteristics.SCALER_STREAM_CONFIGURATION_MAP);

                previewSize = chooseOptimalSize(map.getOutputSizes(SurfaceTexture.class), width, height);

                this.cameraId = cameraId;
                return;
            }
        } catch (CameraAccessException e) {
            e.printStackTrace();
        }
    }

    private void closeCamera(){
        if(cameraDevice != null){
            cameraDevice.close();
            cameraDevice = null;
        }
    }

    private boolean connectCamera(){
        CameraManager cameraManager = (CameraManager) getSystemService(Context.CAMERA_SERVICE);
        try {
            if( Build.VERSION.SDK_INT >= Build.VERSION_CODES.M ){
                if(ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA) ==
                        PackageManager.PERMISSION_GRANTED){
                    cameraManager.openCamera(cameraId, cameraDeviceStateCallback, backgroundHandler);
                }
                else{
                    requestPermissions(new String[] { Manifest.permission.CAMERA,
                            Manifest.permission.RECORD_AUDIO,
                            Manifest.permission.WRITE_EXTERNAL_STORAGE },
                            RECORDING_PERMISSIONS_GRANTED);
                }
            } else {
                cameraManager.openCamera(cameraId, cameraDeviceStateCallback, backgroundHandler);
            }
        } catch (CameraAccessException e) {
            e.printStackTrace();
        }

        return true;
    }

    public void startPreview(){
        SurfaceTexture surfaceTexture = textureView.getSurfaceTexture();

        surfaceTexture.setDefaultBufferSize(previewSize.getWidth(), previewSize.getHeight());
        Surface previewSurface = new Surface(surfaceTexture);

        try {
            captureRequestBuilder = cameraDevice.createCaptureRequest(CameraDevice.TEMPLATE_PREVIEW);
            captureRequestBuilder.addTarget(previewSurface);

            cameraDevice.createCaptureSession(Arrays.asList(previewSurface), new CameraCaptureSession.StateCallback() {
                @Override
                public void onConfigured(@NonNull CameraCaptureSession session) {
                    try {
                        session.setRepeatingRequest(captureRequestBuilder.build(),
                                null, backgroundHandler);
                    } catch (CameraAccessException e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onConfigureFailed(@NonNull CameraCaptureSession session) {
                    Toast.makeText(getApplicationContext(), "Something went wrong.", Toast.LENGTH_SHORT).show();
                }
            }, null);
        } catch (CameraAccessException e) {
            e.printStackTrace();
        }
    }

    public void onToggleScreenShare() {
        if (!isRecording) {
            shareScreen();
        } else {
            onBackPressed();
        }
    }

    private void shareScreen() {
        if (mediaProjection == null) {
            startActivityForResult(projectionManager.createScreenCaptureIntent(), REQUEST_CODE);
            return;
        }
        virtualDisplay = createVirtualDisplay();
        mediaRecorder.start();
        isRecording = true;
    }

    private void prepareRecording(){
        try {
            createVideoFile();
        } catch (IOException e) {
            e.printStackTrace();
        }
        try {
            setupMediaRecorder();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void stopScreenSharing() {
        if (virtualDisplay == null) {
            return;
        }
        virtualDisplay.release();
        destroyMediaProjection();
        isRecording = false;
    }

    private VirtualDisplay createVirtualDisplay() {
        return mediaProjection.createVirtualDisplay("DashCamActivity", screenWidth, screenHeight, screenDensity,
                DisplayManager.VIRTUAL_DISPLAY_FLAG_AUTO_MIRROR, surface, null, null);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if(requestCode == RECORDING_PERMISSIONS_GRANTED){
            if(grantResults[0] != PackageManager.PERMISSION_GRANTED){
                Toast.makeText(getApplicationContext(),
                        "Application will not run without camera", Toast.LENGTH_SHORT).show();
            }
        }
    }

    private void startBackgroundThread(){
        backgroundHandlerThread = new HandlerThread("CameraBackgroundThread");
        backgroundHandlerThread.start();
        backgroundHandler = new Handler(backgroundHandlerThread.getLooper());
    }

    private void stopBackgroundThread(){
        backgroundHandlerThread.quitSafely();
        try {
            backgroundHandlerThread.join();
            backgroundHandlerThread = null;
            backgroundHandler = null;
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    private static Size chooseOptimalSize(Size[] choices, int width, int height) {
        List<Size> bigEnough = new ArrayList<>();
        for(Size option : choices) {
            if((float)option.getHeight()/option.getWidth() == (float)height/width) {
                bigEnough.add(option);
            }
        }
        if(bigEnough.size() > 0) {
            return Collections.min(bigEnough, new CompareSizeByArea());
        } else {
            return choices[0];
        }
    }

    private void createVideoFolder(){
        File videoFile = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_MOVIES);
        videoFolder = new File(videoFile, "dashCamRecoding");
        if(!videoFolder.exists()){
            videoFolder.mkdirs();
        }
    }

    private File createVideoFile() throws IOException{
        String timestamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String prepend = "DASH_CAM_" + timestamp + "_";

        File videoFile = File.createTempFile(prepend, ".mp4", videoFolder);
        sendBroadcast(new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE, Uri.fromFile(videoFile)));
        fileName = videoFile.getAbsolutePath();
        return  videoFile;
    }

    private void setupMediaRecorder() throws IOException{
        mediaRecorder.setVideoSource(MediaRecorder.VideoSource.SURFACE);
        mediaRecorder.setAudioSource(MediaRecorder.AudioSource.DEFAULT);
        mediaRecorder.setOutputFormat(MediaRecorder.OutputFormat.MPEG_4);
        mediaRecorder.setOutputFile(fileName);
        mediaRecorder.setVideoEncodingBitRate(8000000);
        mediaRecorder.setVideoFrameRate(30);
        mediaRecorder.setVideoSize(screenWidth, screenHeight);
        mediaRecorder.setOrientationHint(0);
        mediaRecorder.setVideoEncoder(MediaRecorder.VideoEncoder.H264);
        mediaRecorder.setAudioEncoder(MediaRecorder.AudioEncoder.AAC);
        mediaRecorder.prepare();
        surface = mediaRecorder.getSurface();
    }


    private void destroyMediaProjection() {
        if (mediaProjection != null) {
            mediaProjection.unregisterCallback(mediaProjectionCallback);
            mediaProjection.stop();
            mediaProjection = null;
        }
    }


    private class MediaProjectionCallback extends MediaProjection.Callback {
        @Override
        public void onStop() {
            if (isRecording) {
                isRecording = false;
                mediaRecorder.stop();
                mediaRecorder.reset();
            }
            mediaProjection = null;
            stopScreenSharing();
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        ((DividedByZero) getApplication()).setCameraView(false);
        destroyMediaProjection();
        closeCamera();
    }

    @Override
    public void onBackPressed() {
        if (isRecording) {
            Snackbar.make(findViewById(android.R.id.content), "Wanna Stop recording?",
                    Snackbar.LENGTH_INDEFINITE).setAction(getString(R.string.stop),
                    new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            mediaRecorder.stop();
                            mediaRecorder.reset();
                            stopScreenSharing();
                            record.setText(getString(R.string.start_recording));
                        }
                    }).show();
        } else {
            finish();
        }
    }

    private void transformImage(int width, int height) {

        if (textureView == null) {
            return;
        } else try {
            {
                Matrix matrix = new Matrix();
                int rotation = getWindowManager().getDefaultDisplay().getRotation();
                RectF textureRectF = new RectF(0, 0, width, height);
                RectF previewRectF = new RectF(0, 0, textureView.getHeight(), textureView.getWidth());
                float centerX = textureRectF.centerX();
                float centerY = textureRectF.centerY();
                if (rotation == Surface.ROTATION_90 || rotation == Surface.ROTATION_270) {
                    previewRectF.offset(centerX - previewRectF.centerX(), centerY - previewRectF.centerY());
                    matrix.setRectToRect(textureRectF, previewRectF, Matrix.ScaleToFit.FILL);
                    float scale = Math.max((float) width / width, (float) height / width);
                    matrix.postScale(scale, scale, centerX, centerY);
                    matrix.postRotate(90 * (rotation - 2), centerX, centerY);
                }
                textureView.setTransform(matrix);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        int id = item.getItemId();

        switch (id){
            case R.id.sensor_reading:
                Intent sensorReading = new Intent(DashCamActivity.this, SensorReadingActivity.class);
                startActivity(sensorReading);
                break;
            case R.id.emergency_sms:
                Intent emergencySms = new Intent(DashCamActivity.this, EmergencySmsActivity.class);
                startActivity(emergencySms);
                break;
            case R.id.vehicle_details:
                Intent vehicleDetails = new Intent(DashCamActivity.this, VehicleDetailsActivity.class);
                startActivity(vehicleDetails);
                break;
            case R.id.service_history:
                Intent vehicleService = new Intent(DashCamActivity.this, VehicleServiceActivity.class);
                startActivity(vehicleService);
                break;
            case R.id.bluetooth:
                Intent bluetooth = new Intent(DashCamActivity.this, BluetoothActivity.class);
                startActivity(bluetooth);
                break;
            case R.id.diagnostics:
                Intent diagnostics = new Intent(DashCamActivity.this, DiagnosticsActivity.class);
                startActivity(diagnostics);
                break;
            case R.id.dash_cam:
                Intent dashCam = new Intent(DashCamActivity.this, DashCamActivity.class);
                startActivity(dashCam);
                break;
            case R.id.controls:
                Intent controls = new Intent(DashCamActivity.this, ControlsActivity.class);
                startActivity(controls);
                break;
            case R.id.owner_details:
                Intent ownerDetails = new Intent(DashCamActivity.this, OwnerDetailsActivity.class);
                startActivity(ownerDetails);
                break;
            case R.id.lock:
                Intent lock = new Intent(DashCamActivity.this, PasswordLockActivity.class);
                startActivity(lock);
                break;
            case R.id.gauge:
                Intent gauge = new Intent(DashCamActivity.this, GaugeActivity.class);
                startActivity(gauge);
                break;
        }

        DrawerLayout drawerLayout = findViewById(R.id.drawer_layout);
        drawerLayout.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (toggle.onOptionsItemSelected(item)){
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public void showButtons(){
        buttonsVisible = true;
        record.setVisibility(View.VISIBLE);
        back.setVisibility(View.VISIBLE);
    }

    public void hideButtons(){
        buttonsVisible = false;
        record.setVisibility(View.GONE);
        back.setVisibility(View.GONE);
    }

    @Override
    public void turnOnSos() {
        ((DividedByZero) getApplication()).setSosModeOn(true);
    }

    @Override
    public void turnOffSos() {
        ((DividedByZero) getApplication()).setSosModeOn(false);
    }
}