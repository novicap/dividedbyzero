package com.primenumber.activities;

import android.app.ProgressDialog;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.Toast;

import com.primenumber.application.DividedByZero;
import com.primenumber.dividedbyzero.R;
import com.primenumber.fragmentsandadapters.sosfragment.SosFragment;
import com.primenumber.services.BluetoothOBD2Connection;
import com.primenumber.fragmentsandadapters.bluetooth.BluetoothDevicesRecyclerViewAdapter;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.UUID;

/**
 * Created by novica on 2/26/18.
 */

public class BluetoothActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener,
    SosFragment.SosClickListener{
    private static final String TAG = "BluetoothActivity";
    private static final int REQUEST_BLUETOOTH_ENABLE = 1;

    public static final UUID MY_UUID = UUID.fromString("00001101-0000-1000-8000-00805f9b34fb");

    private Button startScanner, makeConnection, pair;
    private BluetoothOBD2Connection bluetoothConnectionService;
    private BluetoothAdapter bluetoothAdapter;
    private Set<BluetoothDevice> pairedDevices;
    private List<BluetoothDevice> bluetoothDeviceList;
    private BluetoothDevice bluetoothDevice;
    private BluetoothDevicesRecyclerViewAdapter.BluetoothDeviceClickListener listener;
    private BluetoothDevicesRecyclerViewAdapter bluetoothDevicesRecyclerViewAdapter;
    private RecyclerView bluetoothDevicesRecyclerView;
    private DrawerLayout drawerLayout;
    private ActionBarDrawerToggle toggle;
    private ProgressDialog progressDialog;
    private SosFragment sosFragment;
    private boolean isTablet;
    private int selected;

    private final BroadcastReceiver bondBluetoothBond = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            final String action = intent.getAction();
            if(action.equals(BluetoothDevice.ACTION_BOND_STATE_CHANGED)){
                BluetoothDevice dev = intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);
                if(dev.getBondState() == BluetoothDevice.BOND_BONDING){
                    progressDialog = ProgressDialog.show(context,"Pairing."
                            ,"Please Wait...",true);
                } else if (dev.getBondState() == BluetoothDevice.BOND_BONDED) {
                    if(progressDialog != null){
                        progressDialog.dismiss();
                    }
                    bluetoothDevice = dev;
                    pair.setEnabled(false);
                    makeConnection.setEnabled(true);
                    Toast.makeText(BluetoothActivity.this, "Paired.", Toast.LENGTH_SHORT).show();
                } else if (dev.getBondState() == BluetoothDevice.BOND_NONE) {
                    if(progressDialog != null){
                        progressDialog.dismiss();
                    }
                    Toast.makeText(BluetoothActivity.this, "Pairing failed.", Toast.LENGTH_SHORT).show();
                }
            }
        }
    };

    private final BroadcastReceiver scanner = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            final String action = intent.getAction();
            if(action.equals(BluetoothDevice.ACTION_FOUND)){
                BluetoothDevice device = intent.getParcelableExtra (BluetoothDevice.EXTRA_DEVICE);

                if(!bluetoothDeviceList.contains(device)) {
                    bluetoothDeviceList.add(device);
                    bluetoothDevicesRecyclerViewAdapter.notifyDataSetChanged();
                }
            } else if (action.equals(BluetoothAdapter.ACTION_DISCOVERY_STARTED)){
                progressDialog = ProgressDialog.show(context,"Searching for bluetooth devices."
                        ,"Please Wait...",true);
            } else if (action.equals(BluetoothAdapter.ACTION_DISCOVERY_FINISHED)){
                if(progressDialog != null)
                    progressDialog.dismiss();
            }
        }
    };

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bluetooth_layout);
        setTitle("Bluetooth OBD2");
        isTablet = getResources().getBoolean(R.bool.isTablet);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        startScanner = findViewById(R.id.start_scanner);
        pair = findViewById(R.id.pair);
        makeConnection = findViewById(R.id.make_connection);
        startScanner.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                scanForDevices();
            }
        });
        makeConnection.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                tryToConnect(bluetoothDevice, MY_UUID);
            }
        });
        makeConnection.setEnabled(false);
        pair.setEnabled(false);
        bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        pairedDevices = bluetoothAdapter.getBondedDevices();
        bluetoothDeviceList = new ArrayList<>(pairedDevices);
        enableBluetooth();
        bluetoothDevicesRecyclerView = findViewById(R.id.bluetooth_devices_recycler_view);
        listener = new BluetoothDevicesRecyclerViewAdapter.BluetoothDeviceClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                selected = position;
                if(bluetoothDeviceList.get(position).getBondState() != BluetoothDevice.BOND_BONDED){
                    pair.setEnabled(true);
                    makeConnection.setEnabled(false);
                } else {
                    bluetoothDevice = bluetoothDeviceList.get(position);
                    pair.setEnabled(false);
                    makeConnection.setEnabled(true);
                }
            }
        };
        pair.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pairDevice(bluetoothDeviceList.get(selected));
            }
        });
        bluetoothDevicesRecyclerViewAdapter = new BluetoothDevicesRecyclerViewAdapter(this, bluetoothDeviceList, listener);
        bluetoothDevicesRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        bluetoothDevicesRecyclerView.addItemDecoration(new DividerItemDecoration(this ,LinearLayoutManager.VERTICAL));
        bluetoothDevicesRecyclerView.setAdapter(bluetoothDevicesRecyclerViewAdapter);

        bluetoothConnectionService = new BluetoothOBD2Connection(this);
        ((DividedByZero) getApplicationContext()).setBluetoothConnection(bluetoothConnectionService);

        sosFragment = new SosFragment();
        getFragmentManager().beginTransaction()
                .replace(R.id.sos_fragment, sosFragment)
                .commit();
        sosFragment.setSosActive(((DividedByZero) getApplicationContext()).getSosModeOn());

        Toolbar toolbar = findViewById(R.id.toolbar);
        drawerLayout = findViewById(R.id.drawer_layout);
        if(!isTablet) {
            toggle = new ActionBarDrawerToggle(this, drawerLayout, toolbar,
                    R.string.navigation_drawer_open, R.string.navigation_drawer_close);
            drawerLayout.addDrawerListener(toggle);
            toggle.syncState();
        }
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        NavigationView navigationView = findViewById(R.id.navigation_view);
        navigationView.setNavigationItemSelectedListener(this);
    }



    public void enableBluetooth(){
        if(!bluetoothAdapter.isEnabled()){
            Intent enableBluetoothAdapter = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
            startActivityForResult(enableBluetoothAdapter, REQUEST_BLUETOOTH_ENABLE);
        }
    }

    public void scanForDevices(){
        if(bluetoothAdapter.isDiscovering()){
            bluetoothAdapter.cancelDiscovery();
            bluetoothAdapter.startDiscovery();

            IntentFilter discovery = new IntentFilter(BluetoothAdapter.ACTION_DISCOVERY_STARTED);
            discovery.addAction(BluetoothDevice.ACTION_FOUND);
            discovery.addAction(BluetoothDevice.ACTION_UUID);
            discovery.addAction(BluetoothDevice.ACTION_ACL_CONNECTED);
            discovery.addAction(BluetoothAdapter.ACTION_DISCOVERY_FINISHED);

            registerReceiver(scanner, discovery);
        }
        else if(!bluetoothAdapter.isDiscovering()){
            bluetoothAdapter.startDiscovery();

            IntentFilter discovery = new IntentFilter(BluetoothAdapter.ACTION_DISCOVERY_STARTED);
            discovery.addAction(BluetoothDevice.ACTION_FOUND);
            discovery.addAction(BluetoothDevice.ACTION_UUID);
            discovery.addAction(BluetoothDevice.ACTION_ACL_CONNECTED);
            discovery.addAction(BluetoothAdapter.ACTION_DISCOVERY_FINISHED);
            registerReceiver(scanner, discovery);
        }
    }

    public void stopScan(){
        bluetoothAdapter.cancelDiscovery();
        IntentFilter discovery = new IntentFilter(BluetoothAdapter.ACTION_DISCOVERY_FINISHED);
        registerReceiver(scanner, discovery);
    }

    public void pairDevice(BluetoothDevice device){
        stopScan();
        device.createBond();
        IntentFilter bond = new IntentFilter(BluetoothDevice.ACTION_BOND_STATE_CHANGED);
        registerReceiver(bondBluetoothBond, bond);
    }

    public void tryToConnect(BluetoothDevice device, UUID uuid){
        bluetoothConnectionService.startClient(device, uuid);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        try {
            unregisterReceiver(scanner);
            unregisterReceiver(bondBluetoothBond);
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onBackPressed() {
        if (drawerLayout.isDrawerOpen(GravityCompat.START)) {
            drawerLayout.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        int id = item.getItemId();

        switch (id){
            case R.id.sensor_reading:
                Intent sensorReading = new Intent(BluetoothActivity.this, SensorReadingActivity.class);
                startActivity(sensorReading);
                break;
            case R.id.emergency_sms:
                Intent emergencySms = new Intent(BluetoothActivity.this, EmergencySmsActivity.class);
                startActivity(emergencySms);
                break;
            case R.id.vehicle_details:
                Intent vehicleDetails = new Intent(BluetoothActivity.this, VehicleDetailsActivity.class);
                startActivity(vehicleDetails);
                break;
            case R.id.service_history:
                Intent vehicleService = new Intent(BluetoothActivity.this, VehicleServiceActivity.class);
                startActivity(vehicleService);
                break;
            case R.id.bluetooth:
                break;
            case R.id.diagnostics:
                Intent diagnostics = new Intent(BluetoothActivity.this, DiagnosticsActivity.class);
                startActivity(diagnostics);
                break;
            case R.id.dash_cam:
                Intent dashCam = new Intent(BluetoothActivity.this, DashCamActivity.class);
                startActivity(dashCam);
                break;
            case R.id.controls:
                Intent controls = new Intent(BluetoothActivity.this, ControlsActivity.class);
                startActivity(controls);
                break;
            case R.id.owner_details:
                Intent ownerDetails = new Intent(BluetoothActivity.this, OwnerDetailsActivity.class);
                startActivity(ownerDetails);
                break;
            case R.id.lock:
                Intent lock = new Intent(BluetoothActivity.this, PasswordLockActivity.class);
                startActivity(lock);
                break;
            case R.id.gauge:
                Intent gauge = new Intent(BluetoothActivity.this, GaugeActivity.class);
                startActivity(gauge);
                break;
        }
        if(!isTablet) {
            DrawerLayout drawerLayout = findViewById(R.id.drawer_layout);
            drawerLayout.closeDrawer(GravityCompat.START);
        }
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (toggle.onOptionsItemSelected(item)){
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void turnOnSos() {
        ((DividedByZero) getApplication()).setSosModeOn(true);
    }

    @Override
    public void turnOffSos() {
        ((DividedByZero) getApplication()).setSosModeOn(false);
    }
}
