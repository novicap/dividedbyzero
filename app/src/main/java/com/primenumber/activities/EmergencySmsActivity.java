package com.primenumber.activities;

import android.Manifest;
import android.app.FragmentManager;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.NavigationView;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.WindowManager;

import com.primenumber.application.DividedByZero;
import com.primenumber.dividedbyzero.R;
import com.primenumber.fragmentsandadapters.emergencysms.EmergencySmsCreateFragment;
import com.primenumber.fragmentsandadapters.emergencysms.EmergencySmsEditFragment;
import com.primenumber.fragmentsandadapters.emergencysms.EmergencySmsListFragment;
import com.primenumber.fragmentsandadapters.sosfragment.SosFragment;

/**
 * Created by novica on 2/19/18.
 */

public class EmergencySmsActivity extends AppCompatActivity
        implements EmergencySmsListFragment.OnItemSelectedListener,
        EmergencySmsCreateFragment.EmergencySmsCreatedListener, EmergencySmsEditFragment.ItemSelectedListener,
        NavigationView.OnNavigationItemSelectedListener, SosFragment.SosClickListener{

    private static final int SMS_ACCESS_GRANTED = 1;
    private ActionBarDrawerToggle toggle;
    private EmergencySmsListFragment listFragment;
    private EmergencySmsEditFragment editFragment;
    private EmergencySmsCreateFragment createFragment;
    private FragmentManager fragmentManager;
    private DrawerLayout drawerLayout;
    private SosFragment sosFragment;
    private boolean isTablet;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_emergency_sms_layout);
        setTitle("Emergency");
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        isTablet = getResources().getBoolean(R.bool.isTablet);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if(ContextCompat.checkSelfPermission(this, Manifest.permission.SEND_SMS) !=
                    PackageManager.PERMISSION_GRANTED) {
                requestPermissions(new String[] {Manifest.permission.SEND_SMS}, SMS_ACCESS_GRANTED);
            }
        }
        fragmentManager = getFragmentManager();
        listFragment = new EmergencySmsListFragment();
        createFragment = new EmergencySmsCreateFragment();
        editFragment = new EmergencySmsEditFragment();
        sosFragment = new SosFragment();
        fragmentManager.beginTransaction()
                .replace(R.id.fragment_holder, listFragment)
                .replace(R.id.sos_fragment, sosFragment)
                .commit();

        sosFragment.setSosActive(((DividedByZero) getApplicationContext()).getSosModeOn());
        Toolbar toolbar = findViewById(R.id.toolbar);
        drawerLayout = findViewById(R.id.drawer_layout);
        if(!isTablet) {
            toggle = new ActionBarDrawerToggle(this, drawerLayout, toolbar,
                    R.string.navigation_drawer_open, R.string.navigation_drawer_close);
            drawerLayout.addDrawerListener(toggle);
            toggle.syncState();
        }

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        NavigationView navigationView = findViewById(R.id.navigation_view);
        navigationView.setNavigationItemSelectedListener(this);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void createNew() {
        fragmentManager.beginTransaction()
                .replace(R.id.fragment_holder, createFragment)
                .addToBackStack(null)
                .commit();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (toggle.onOptionsItemSelected(item)){
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onItemSelected(int id) {
        Bundle bundle = new Bundle();
        bundle.putInt("id", id);
        editFragment.setArguments(bundle);
        fragmentManager.beginTransaction()
                .replace(R.id.fragment_holder, editFragment)
                .addToBackStack(null)
                .commit();
    }

    @Override
    public void newEntryStored() {
        fragmentManager.popBackStack();
        fragmentManager.beginTransaction()
                .replace(R.id.fragment_holder, listFragment)
                .commit();
    }

    @Override
    public void selectItem(int id) {
        editFragment.getEmergencySms(id);
    }

    @Override
    public void itemDeleted() {
        fragmentManager.popBackStack();
        fragmentManager.beginTransaction()
                .replace(R.id.fragment_holder, listFragment)
                .commit();
    }

    public void itemUpdated(){
        fragmentManager.popBackStack();
        fragmentManager.beginTransaction()
                .replace(R.id.fragment_holder, listFragment)
                .commit();
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        int id = item.getItemId();

        switch (id){
            case R.id.sensor_reading:
                Intent sensorReading = new Intent(EmergencySmsActivity.this, SensorReadingActivity.class);
                startActivity(sensorReading);
                break;
            case R.id.emergency_sms:
                break;
            case R.id.vehicle_details:
                Intent vehicleDetails = new Intent(EmergencySmsActivity.this, VehicleDetailsActivity.class);
                startActivity(vehicleDetails);
                break;
            case R.id.service_history:
                Intent vehicleService = new Intent(EmergencySmsActivity.this, VehicleServiceActivity.class);
                startActivity(vehicleService);
                break;
            case R.id.bluetooth:
                Intent bluetooth = new Intent(EmergencySmsActivity.this, BluetoothActivity.class);
                startActivity(bluetooth);
                break;
            case R.id.diagnostics:
                Intent diagnostics = new Intent(EmergencySmsActivity.this, DiagnosticsActivity.class);
                startActivity(diagnostics);
                break;
            case R.id.dash_cam:
                Intent dashCam = new Intent(EmergencySmsActivity.this, DashCamActivity.class);
                startActivity(dashCam);
                break;
            case R.id.controls:
                Intent controls = new Intent(EmergencySmsActivity.this, ControlsActivity.class);
                startActivity(controls);
                break;
            case R.id.owner_details:
                Intent ownerDetails = new Intent(EmergencySmsActivity.this, OwnerDetailsActivity.class);
                startActivity(ownerDetails);
                break;
            case R.id.lock:
                Intent lock = new Intent(EmergencySmsActivity.this, PasswordLockActivity.class);
                startActivity(lock);
                break;
            case R.id.gauge:
                Intent gauge = new Intent(EmergencySmsActivity.this, GaugeActivity.class);
                startActivity(gauge);
                break;
        }
        if(!isTablet) {
            DrawerLayout drawerLayout = findViewById(R.id.drawer_layout);
            drawerLayout.closeDrawer(GravityCompat.START);
        }
        return true;
    }

    @Override
    public void onBackPressed() {
        if (drawerLayout.isDrawerOpen(GravityCompat.START)) {
            drawerLayout.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults){
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        if(requestCode == 1){
            if(grantResults[0] != PackageManager.PERMISSION_GRANTED){
                finish();
            }
        }
    }

    @Override
    public void turnOnSos() {
        ((DividedByZero) getApplication()).setSosModeOn(true);
    }

    @Override
    public void turnOffSos() {
        ((DividedByZero) getApplication()).setSosModeOn(false);
    }
}
