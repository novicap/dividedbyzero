package com.primenumber.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.primenumber.application.DividedByZero;
import com.primenumber.data.Database;
import com.primenumber.dividedbyzero.R;
import com.primenumber.fragmentsandadapters.sosfragment.SosFragment;
import com.primenumber.helpers.HashHelper;

import java.security.NoSuchAlgorithmException;

/**
 * Created by novica on 4/9/18.
 */

public class PasswordLockActivity extends AppCompatActivity implements View.OnClickListener,
        NavigationView.OnNavigationItemSelectedListener, SosFragment.SosClickListener{

    private EditText oldPassword, newPassword, repeatNewPassword;
    private Button save;
    private Database database;
    private HashHelper hashHelper;
    private DrawerLayout drawerLayout;
    private ActionBarDrawerToggle toggle;
    private SosFragment sosFragment;
    private boolean isTablet;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_password_lock_reset_password_layout);
        isTablet = getResources().getBoolean(R.bool.isTablet);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        setTitle("Password");
        database = Database.getInstance(this);
        hashHelper = new HashHelper();
        save = findViewById(R.id.save);
        oldPassword = findViewById(R.id.old_password);
        newPassword = findViewById(R.id.password);
        repeatNewPassword = findViewById(R.id.repeat_password);
        save.setOnClickListener(this);
        sosFragment = new SosFragment();
        sosFragment.setSosActive(((DividedByZero) getApplicationContext()).getSosModeOn());

        Toolbar toolbar = findViewById(R.id.toolbar);
        drawerLayout = findViewById(R.id.drawer_layout);
        if(!isTablet) {
            toggle = new ActionBarDrawerToggle(this, drawerLayout, toolbar,
                    R.string.navigation_drawer_open, R.string.navigation_drawer_close);
            drawerLayout.addDrawerListener(toggle);
            toggle.syncState();
        }

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        NavigationView navigationView = findViewById(R.id.navigation_view);
        navigationView.setNavigationItemSelectedListener(this);
    }

    @Override
    public void onClick(View view) {
        if(view.getId() == R.id.save){
            try {
                if(database.getPassword().equals(hashHelper.sha1((oldPassword.getText()).toString()))) {
                    if ((newPassword.getText()).toString().length() == 5) {
                        if ((newPassword.getText()).toString().equals((repeatNewPassword.getText()).toString())) {
                            try {
                                database.updatePassword(hashHelper.sha1((newPassword.getText()).toString()));
                                Toast.makeText(this, "Password successfully updated.", Toast.LENGTH_SHORT).show();
                                Intent sensorReading = new Intent(PasswordLockActivity.this, SensorReadingActivity.class);
                                startActivity(sensorReading);

                            } catch (NoSuchAlgorithmException e) {
                                e.printStackTrace();
                            }
                        } else {
                            Toast.makeText(this, "Password and repeated password must match.", Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        Toast.makeText(this, "Password must be five numbers long.", Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Toast.makeText(this, "Wrong password.", Toast.LENGTH_SHORT).show();
                }
            } catch (NoSuchAlgorithmException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (toggle.onOptionsItemSelected(item)){
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        int id = item.getItemId();

        switch (id){
            case R.id.sensor_reading:
                Intent sensorReading = new Intent(PasswordLockActivity.this, SensorReadingActivity.class);
                startActivity(sensorReading);
                break;
            case R.id.emergency_sms:
                Intent emergencySms = new Intent(PasswordLockActivity.this, EmergencySmsActivity.class);
                startActivity(emergencySms);
                break;
            case R.id.vehicle_details:
                Intent vehicleDetails = new Intent(PasswordLockActivity.this, VehicleDetailsActivity.class);
                startActivity(vehicleDetails);
                break;
            case R.id.service_history:
                Intent vehicleService = new Intent(PasswordLockActivity.this, VehicleServiceActivity.class);
                startActivity(vehicleService);
                break;
            case R.id.bluetooth:
                Intent bluetooth = new Intent(PasswordLockActivity.this, BluetoothActivity.class);
                startActivity(bluetooth);
                break;
            case R.id.diagnostics:
                Intent dashCam = new Intent(PasswordLockActivity.this, DiagnosticsActivity.class);
                startActivity(dashCam);
                break;
            case R.id.lock:
                break;
            case R.id.controls:
                Intent controls = new Intent(PasswordLockActivity.this, ControlsActivity.class);
                startActivity(controls);
                break;
            case R.id.owner_details:
                Intent ownerDetails = new Intent(PasswordLockActivity.this, OwnerDetailsActivity.class);
                startActivity(ownerDetails);
                break;
            case R.id.gauge:
                Intent gauge = new Intent(PasswordLockActivity.this, GaugeActivity.class);
                startActivity(gauge);
                break;
        }
        if(!isTablet) {
            DrawerLayout drawerLayout = findViewById(R.id.drawer_layout);
            drawerLayout.closeDrawer(GravityCompat.START);
        }
        return true;
    }

    @Override
    public void onBackPressed() {
        if (drawerLayout.isDrawerOpen(GravityCompat.START)) {
            drawerLayout.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public void turnOnSos() {
        ((DividedByZero) getApplication()).setSosModeOn(true);
    }

    @Override
    public void turnOffSos() {
        ((DividedByZero) getApplication()).setSosModeOn(false);
    }
}
