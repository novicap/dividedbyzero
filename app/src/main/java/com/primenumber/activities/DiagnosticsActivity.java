package com.primenumber.activities;

import android.app.ProgressDialog;
import android.bluetooth.BluetoothAdapter;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.Toast;

import com.primenumber.application.DividedByZero;
import com.primenumber.data.Database;
import com.primenumber.data.models.TroubleCode;
import com.primenumber.dividedbyzero.R;
import com.primenumber.fragmentsandadapters.sosfragment.SosFragment;
import com.primenumber.services.BluetoothOBD2Connection;
import com.primenumber.fragmentsandadapters.diagnostics.TroubleCodeRecyclerViewAdapter;

import java.util.ArrayList;
import java.util.List;

public class DiagnosticsActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener,
        SosFragment.SosClickListener{

    private Database database;
    private ActionBarDrawerToggle toggle;
    private BluetoothOBD2Connection bluetoothOBD2Connection;
    private RecyclerView recyclerView;
    private TroubleCodeRecyclerViewAdapter troubleCodeRecyclerViewAdapter;
    private List<TroubleCode> troubleCodeList;
    private List<String> troubleCodes;
    private Button diagnostics;
    private DrawerLayout drawerLayout;
    private SosFragment sosFragment;
    private BluetoothAdapter bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
    private boolean isTablet;
    private ProgressDialog progressDialog;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        setContentView(R.layout.activity_diagnostics_layout);
        setTitle("Diagnostics");
        isTablet = getResources().getBoolean(R.bool.isTablet);
        diagnostics = findViewById(R.id.diagnostics);
        database = Database.getInstance(this);
        troubleCodeList = new ArrayList<>();
        bluetoothOBD2Connection = ((DividedByZero)getApplication()).getBluetoothConnection();
        recyclerView = findViewById(R.id.trouble_codes_recycler_view);
        troubleCodeRecyclerViewAdapter = new TroubleCodeRecyclerViewAdapter(this, troubleCodeList);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setAdapter(troubleCodeRecyclerViewAdapter);
        diagnostics();
        diagnostics.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                diagnostics();
            }
        });

        sosFragment = new SosFragment();
        getFragmentManager().beginTransaction()
                .replace(R.id.sos_fragment, sosFragment)
                .commit();
        sosFragment.setSosActive(((DividedByZero) getApplicationContext()).getSosModeOn());

        Toolbar toolbar = findViewById(R.id.toolbar);
        drawerLayout = findViewById(R.id.drawer_layout);
        if(!isTablet) {
            toggle = new ActionBarDrawerToggle(this, drawerLayout, toolbar,
                    R.string.navigation_drawer_open, R.string.navigation_drawer_close);
            drawerLayout.addDrawerListener(toggle);
            toggle.syncState();
        }

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        NavigationView navigationView = findViewById(R.id.navigation_view);
        navigationView.setNavigationItemSelectedListener(this);
    }

    @Override
    protected void onResume() {
        super.onResume();


    }

    public void diagnostics(){
        if(bluetoothOBD2Connection != null && bluetoothAdapter.isEnabled()){
            troubleCodeList.clear();
            troubleCodes = bluetoothOBD2Connection.getTroubleCodes();
            progressDialog = ProgressDialog.show(DiagnosticsActivity.this,"Checking error codes."
                    ,"Please Wait...",true);

            // it's here only for looks n' swag
            Handler handler = new Handler();
            handler.postDelayed(new Runnable() {
                public void run() {
                    progressDialog.dismiss();
                }
            }, 5000);
            //

            for(String troubleCode : troubleCodes){
                troubleCodeList.add(database.getTroubleCode(troubleCode));
                troubleCodeRecyclerViewAdapter.notifyDataSetChanged();
            }
        } else {
            Toast.makeText(this, "Application is not connected to vehicle.", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (toggle.onOptionsItemSelected(item)){
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        int id = item.getItemId();

        switch (id){
            case R.id.sensor_reading:
                Intent sensorReading = new Intent(DiagnosticsActivity.this, SensorReadingActivity.class);
                startActivity(sensorReading);
                break;
            case R.id.emergency_sms:
                Intent emergencySms = new Intent(DiagnosticsActivity.this, EmergencySmsActivity.class);
                startActivity(emergencySms);
                break;
            case R.id.vehicle_details:
                Intent vehicleDetails = new Intent(DiagnosticsActivity.this, VehicleDetailsActivity.class);
                startActivity(vehicleDetails);
                break;
            case R.id.service_history:
                Intent vehicleService = new Intent(DiagnosticsActivity.this, VehicleServiceActivity.class);
                startActivity(vehicleService);
                break;
            case R.id.bluetooth:
                Intent bluetooth = new Intent(DiagnosticsActivity.this, BluetoothActivity.class);
                startActivity(bluetooth);
                break;
            case R.id.diagnostics:
                break;
            case R.id.dash_cam:
                Intent dashCam = new Intent(DiagnosticsActivity.this, DashCamActivity.class);
                startActivity(dashCam);
                break;
            case R.id.controls:
                Intent controls = new Intent(DiagnosticsActivity.this, ControlsActivity.class);
                startActivity(controls);
                break;
            case R.id.owner_details:
                Intent ownerDetails = new Intent(DiagnosticsActivity.this, OwnerDetailsActivity.class);
                startActivity(ownerDetails);
                break;
            case R.id.lock:
                Intent lock = new Intent(DiagnosticsActivity.this, PasswordLockActivity.class);
                startActivity(lock);
                break;
            case R.id.gauge:
                Intent gauge = new Intent(DiagnosticsActivity.this, GaugeActivity.class);
                startActivity(gauge);
                break;
        }
        if(!isTablet) {
            DrawerLayout drawerLayout = findViewById(R.id.drawer_layout);
            drawerLayout.closeDrawer(GravityCompat.START);
        }
        return true;
    }

    @Override
    public void onBackPressed() {
        if (drawerLayout.isDrawerOpen(GravityCompat.START)) {
            drawerLayout.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public void turnOnSos() {
        ((DividedByZero) getApplication()).setSosModeOn(true);
    }

    @Override
    public void turnOffSos() {
        ((DividedByZero) getApplication()).setSosModeOn(false);
    }
}
