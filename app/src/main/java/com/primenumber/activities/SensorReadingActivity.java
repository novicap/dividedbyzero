package com.primenumber.activities;

import android.Manifest;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.MenuItem;
import android.support.v7.widget.Toolbar;
import android.view.WindowManager;

import com.primenumber.application.DividedByZero;
import com.primenumber.data.Database;
import com.primenumber.data.models.RecyclerViewData;
import com.primenumber.dividedbyzero.R;
import com.primenumber.fragmentsandadapters.sensorreading.SensorReadingRecyclerViewAdapter;
import com.primenumber.fragmentsandadapters.sosfragment.SosFragment;
import com.primenumber.services.AccelerometerService;
import com.primenumber.services.LocationService;
import com.primenumber.services.VehicleLockService;

import java.util.ArrayList;
import java.util.List;

public class SensorReadingActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener, SosFragment.SosClickListener{

    private static final int LOCATION_ACCESS_GRANTED = 1;
    private ActionBarDrawerToggle toggle;
    private BroadcastReceiver locationDataReceiver, obdDataReceiver, accelerometerReceiver;
    private SosFragment sosFragment;
    private DrawerLayout drawerLayout;
    private Database database;
    private boolean isTablet;
    private RecyclerView myRecyclerView;
    private List<RecyclerViewData> recyclerViewDataList;
    private RecyclerViewData speed, location, bearing, rpm, engineTemperature, vehicleSpeed,
            throttlePosition, errorCodes, gforce;
    private SensorReadingRecyclerViewAdapter recyclerAdapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sensor_readings_layout);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        setTitle("Car data feed");
        isTablet = getResources().getBoolean(R.bool.isTablet);
        database = Database.getInstance(this);
        location = new RecyclerViewData("Location", "---", R.drawable.my_location);
        bearing = new RecyclerViewData("Bearing", "---", R.drawable.compass);
        speed = new RecyclerViewData("Speed", "---", R.drawable.speedometer);
        rpm = new RecyclerViewData("RPM", "---", R.drawable.vehicle_speed_icon);
        vehicleSpeed = new RecyclerViewData("Vehicle speed", "---", R.drawable.speedometer);
        throttlePosition = new RecyclerViewData("Throttle position", "---", R.drawable.throttle_percentage_icon);
        engineTemperature = new RecyclerViewData("Temperature", "---", R.drawable.temperature_icon);
        gforce = new RecyclerViewData("G-force", "---", R.drawable.gforce_icon);
        errorCodes = new RecyclerViewData("Error codes", "---", R.drawable.spanner);

        recyclerViewDataList = new ArrayList<>();
        recyclerViewDataList.add(speed);
        recyclerViewDataList.add(location);
        recyclerViewDataList.add(bearing);
        recyclerViewDataList.add(rpm);
        recyclerViewDataList.add(vehicleSpeed);
        recyclerViewDataList.add(throttlePosition);
        recyclerViewDataList.add(engineTemperature);
        recyclerViewDataList.add(errorCodes);
        recyclerViewDataList.add(gforce);

        sosFragment = new SosFragment();
        myRecyclerView = findViewById(R.id.sensor_data_recycler_view);
        recyclerAdapter = new SensorReadingRecyclerViewAdapter(this, recyclerViewDataList);
        myRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        myRecyclerView.addItemDecoration(new DividerItemDecoration(this,LinearLayoutManager.VERTICAL));
        myRecyclerView.setAdapter(recyclerAdapter);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if(ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) ==
                    PackageManager.PERMISSION_GRANTED) {
                Intent intent = new Intent(getApplicationContext(), LocationService.class);
                startService(intent);
            }
            else{
                requestPermissions(new String[] {Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION}, LOCATION_ACCESS_GRANTED);
            }
        }

        Intent accelerometer = new Intent(getApplicationContext(), AccelerometerService.class);
        startService(accelerometer);

        getFragmentManager().beginTransaction()
                .replace(R.id.sos_fragment, sosFragment)
                .commit();
        sosFragment.setSosActive(((DividedByZero) getApplicationContext()).getSosModeOn());

        Toolbar toolbar = findViewById(R.id.toolbar);
        drawerLayout = findViewById(R.id.drawer_layout);
        if(!isTablet) {
            toggle = new ActionBarDrawerToggle(this, drawerLayout, toolbar,
                    R.string.navigation_drawer_open, R.string.navigation_drawer_close);
            drawerLayout.addDrawerListener(toggle);
            toggle.syncState();
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }

        NavigationView navigationView = findViewById(R.id.navigation_view);
        navigationView.setNavigationItemSelectedListener(this);
    }

    @Override
    protected void onResume(){
        super.onResume();
        if(VehicleLockService.isServiceRunning || database.getLockState() == 1){
            Intent intent = new Intent(this, UnlockActivity.class);
            startActivity(intent);
        } else {
            if(locationDataReceiver == null){
                locationDataReceiver = new BroadcastReceiver() {
                    @Override
                    public void onReceive(Context context, Intent intent) {
                        recyclerViewDataList.get(0).setReading(String.valueOf(intent.getExtras().get("speed"))+" m/s"+
                                "\n"+String.valueOf(Math.round(((float)intent.getExtras().get("speed")*18/5)*100)/100)+" km/h");
                        recyclerViewDataList.get(1).setReading("Longitude: "+String.valueOf(intent.getExtras().get("longitude"))+
                                "\nLatitude: "+String.valueOf(intent.getExtras().get("latitude"))+
                                "\nAccuracy: "+String.valueOf(Math.round((float)intent.getExtras().get("accuracy")))+" m"+
                                "\nAltitude: "+String.valueOf(intent.getExtras().get("altitude"))+ " m");
                        recyclerViewDataList.get(2).setReading(String.valueOf(intent.getExtras().get("bearing"))+"deg");
                        recyclerAdapter.notifyDataSetChanged();
                    }
                };
            }
            if(accelerometerReceiver == null){
                accelerometerReceiver = new BroadcastReceiver() {
                    @Override
                    public void onReceive(Context context, Intent intent) {
                    if(intent.getExtras().get("gforce") != null) {
                        recyclerViewDataList.get(8).setReading(String.valueOf(intent.getExtras().get("gforce")) + " G");
                        recyclerAdapter.notifyDataSetChanged();
                    }
                    }
                };
            }
            if(obdDataReceiver == null){
                obdDataReceiver = new BroadcastReceiver() {
                    @Override
                    public void onReceive(Context context, Intent intent) {
                        recyclerViewDataList.get(3).setReading(String.valueOf(intent.getExtras().get("rpm")) + " RPM");
                        recyclerViewDataList.get(4).setReading(String.valueOf(intent.getExtras().get("speed")) + " km/h");
                        recyclerViewDataList.get(5).setReading(String.valueOf(intent.getExtras().get("throttle_position")) + "%");
                        recyclerViewDataList.get(6).setReading(String.valueOf(intent.getExtras().get("engine_temperature")) + " C");
                        recyclerViewDataList.get(7).setReading(String.valueOf(intent.getExtras().get("trouble_codes_string")));
                        recyclerAdapter.notifyDataSetChanged();
                    }
                };
            }
            registerReceiver(locationDataReceiver, new IntentFilter("location_service"));
            registerReceiver(accelerometerReceiver, new IntentFilter("gforce"));
            registerReceiver(obdDataReceiver, new IntentFilter("obd_reading"));
        }
    }

    @Override

    protected void onDestroy(){
        super.onDestroy();
        Intent location = new Intent(getApplicationContext(), LocationService.class);
        stopService(location);
        Intent accelerometer = new Intent(getApplicationContext(), AccelerometerService.class);
        stopService(accelerometer);
        try {
            unregisterReceiver(locationDataReceiver);
            unregisterReceiver(accelerometerReceiver);
            unregisterReceiver(obdDataReceiver);
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (toggle.onOptionsItemSelected(item)){
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        int id = item.getItemId();

        switch (id){
            case R.id.sensor_reading:
                break;
            case R.id.emergency_sms:
                Intent emergencySms = new Intent(SensorReadingActivity.this, EmergencySmsActivity.class);
                startActivity(emergencySms);
                break;
            case R.id.vehicle_details:
                Intent vehicleDetails = new Intent(SensorReadingActivity.this, VehicleDetailsActivity.class);
                startActivity(vehicleDetails);
                break;
            case R.id.service_history:
                Intent vehicleService = new Intent(SensorReadingActivity.this, VehicleServiceActivity.class);
                startActivity(vehicleService);
                break;
            case R.id.bluetooth:
                Intent bluetooth = new Intent(SensorReadingActivity.this, BluetoothActivity.class);
                startActivity(bluetooth);
                break;
            case R.id.diagnostics:
                Intent diagnostics = new Intent(SensorReadingActivity.this, DiagnosticsActivity.class);
                startActivity(diagnostics);
                break;
            case R.id.dash_cam:
                Intent dashCam = new Intent(SensorReadingActivity.this, DashCamActivity.class);
                startActivity(dashCam);
                break;
            case R.id.controls:
                Intent controls = new Intent(SensorReadingActivity.this, ControlsActivity.class);
                startActivity(controls);
                break;
            case R.id.owner_details:
                Intent ownerDetails = new Intent(SensorReadingActivity.this, OwnerDetailsActivity.class);
                startActivity(ownerDetails);
                break;
            case R.id.lock:
                Intent lock = new Intent(SensorReadingActivity.this, PasswordLockActivity.class);
                startActivity(lock);
                break;
            case R.id.gauge:
                Intent gauge = new Intent(SensorReadingActivity.this, GaugeActivity.class);
                startActivity(gauge);
                break;
        }
        if(!isTablet) {
            DrawerLayout drawerLayout = findViewById(R.id.drawer_layout);
            drawerLayout.closeDrawer(GravityCompat.START);
        }
        return true;
    }

    @Override
    public void onBackPressed() {
        if (drawerLayout.isDrawerOpen(GravityCompat.START) || drawerLayout.isDrawerOpen(GravityCompat.END)) {
            drawerLayout.closeDrawer(GravityCompat.START);
            drawerLayout.closeDrawer(GravityCompat.END);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults){
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        if(requestCode == 1){
            if(grantResults[0] != PackageManager.PERMISSION_GRANTED){
                finish();
            } else {
                Intent intent = new Intent(getApplicationContext(), LocationService.class);
                startService(intent);
            }
        }
    }

    @Override
    public void turnOnSos() {
        ((DividedByZero) getApplication()).setSosModeOn(true);
    }

    @Override
    public void turnOffSos() {
        ((DividedByZero) getApplication()).setSosModeOn(false);
    }
}
