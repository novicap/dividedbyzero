package com.primenumber.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.primenumber.data.Database;
import com.primenumber.dividedbyzero.R;
import com.primenumber.helpers.HashHelper;
import com.primenumber.services.SecuritySmsService;
import com.primenumber.services.VehicleLockService;

import java.security.NoSuchAlgorithmException;

/**
 * Created by novica on 4/10/18.
 */

public class PasswordUnlockActivity extends AppCompatActivity implements View.OnClickListener{

    private EditText password;
    private Button unlock;
    private Database database;
    private HashHelper hashHelper;

    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setTitle("Security");
        setContentView(R.layout.activity_password_unlock_activity_layout);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        database = Database.getInstance(this);
        hashHelper = new HashHelper();
        password = findViewById(R.id.password);
        unlock = findViewById(R.id.unlock);
        unlock.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        if(view.getId() == R.id.unlock){
            try {
                if(database.getPassword().equals(hashHelper.sha1((password.getText()).toString()))){
                    database.updateLockState(0);
                    Intent lockService = new Intent(this, VehicleLockService.class);
                    stopService(lockService);
                    Intent securitySmsService = new Intent(this, SecuritySmsService.class);
                    stopService(securitySmsService);
                    Intent intent = new Intent(this, SensorReadingActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(intent);
                } else {
                    Toast.makeText(this, "Wrong password.", Toast.LENGTH_SHORT).show();
                }
            } catch (NoSuchAlgorithmException e) {
                e.printStackTrace();
            }
        }
    }
}
