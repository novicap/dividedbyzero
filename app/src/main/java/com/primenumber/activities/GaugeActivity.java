package com.primenumber.activities;

import android.app.FragmentManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.WindowManager;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.primenumber.application.DividedByZero;
import com.primenumber.data.Database;
import com.primenumber.dividedbyzero.R;
import com.primenumber.fragmentsandadapters.sosfragment.SosFragment;
import com.primenumber.services.BluetoothOBD2Connection;
import com.primenumber.views.GaugeView;

/**
 * Created by novica on 4/23/18.
 */

public class GaugeActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener, SosFragment.SosClickListener{
    private GaugeView gaugeView;
    private BroadcastReceiver locationDataReceiver, obdDataReceiver;
    private Database database;
    private ActionBarDrawerToggle toggle;
    private DrawerLayout drawerLayout;
    private SosFragment sosFragment;
    private BluetoothOBD2Connection bluetoothOBD2Connection;
    private FragmentManager fragmentManager;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_gauge_layout);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        gaugeView = new GaugeView(this);
        bluetoothOBD2Connection = ((DividedByZero)getApplication()).getBluetoothConnection();
        fragmentManager = getFragmentManager();
        RelativeLayout relativeLayout = findViewById(R.id.gauge_view);
        relativeLayout.addView(gaugeView);
        database = Database.getInstance(this);
        sosFragment = new SosFragment();
        fragmentManager.beginTransaction()
                .replace(R.id.sos_fragment, sosFragment)
                .commit();
        sosFragment.setSosActive(((DividedByZero) getApplicationContext()).getSosModeOn());

        Toolbar toolbar = findViewById(R.id.toolbar);
        drawerLayout = findViewById(R.id.drawer_layout);

        toggle = new ActionBarDrawerToggle(this, drawerLayout, toolbar,
                R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawerLayout.addDrawerListener(toggle);
        toggle.syncState();
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        NavigationView navigationView = findViewById(R.id.navigation_view);
        navigationView.setNavigationItemSelectedListener(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (database.getGaugeSpeedType() == 1) {
            if (locationDataReceiver == null) {
                locationDataReceiver = new BroadcastReceiver() {
                    @Override
                    public void onReceive(Context context, Intent intent) {
                        updateGauge((float) intent.getExtras().get("speed"));
                    }
                };
            }
        }
        else if(database.getGaugeSpeedType() == 2 && bluetoothOBD2Connection != null) {
            if (obdDataReceiver == null) {
                obdDataReceiver = new BroadcastReceiver() {
                    @Override
                    public void onReceive(Context context, Intent intent) {
                        updateGauge((int)intent.getExtras().get("speed"));
                    }
                };
            }
        } else {
            Toast.makeText(this, "Car connection is not active.", Toast.LENGTH_SHORT).show();
        }
        registerReceiver(locationDataReceiver, new IntentFilter("location_service"));
        registerReceiver(obdDataReceiver, new IntentFilter("obd_reading"));
    }

    public void updateGauge(float reading){
        if(gaugeView != null) {
            if(database.getGaugeSpeedType() == 2) {
                gaugeView.setNeedleAngle(reading);
            } else if (database.getGaugeSpeedType() == 1) {
                gaugeView.setNeedleAngle(reading*18/5);
            }
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (toggle.onOptionsItemSelected(item)){
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        int id = item.getItemId();

        switch (id){
            case R.id.sensor_reading:
                Intent sensorReading = new Intent(GaugeActivity.this, SensorReadingActivity.class);
                startActivity(sensorReading);
                break;
            case R.id.emergency_sms:
                Intent emergencySms = new Intent(GaugeActivity.this, EmergencySmsActivity.class);
                startActivity(emergencySms);
                break;
            case R.id.vehicle_details:
                Intent vehicleDetails = new Intent(GaugeActivity.this, VehicleDetailsActivity.class);
                startActivity(vehicleDetails);
                break;
            case R.id.service_history:
                Intent vehicleService = new Intent(GaugeActivity.this, VehicleServiceActivity.class);
                startActivity(vehicleService);
                break;
            case R.id.bluetooth:
                Intent bluetooth = new Intent(GaugeActivity.this, BluetoothActivity.class);
                startActivity(bluetooth);
                break;
            case R.id.diagnostics:
                Intent diagnostics = new Intent(GaugeActivity.this, DiagnosticsActivity.class);
                startActivity(diagnostics);
                break;
            case R.id.dash_cam:
                Intent dashCam = new Intent(GaugeActivity.this, DashCamActivity.class);
                startActivity(dashCam);
                break;
            case R.id.controls:
                Intent controls = new Intent(GaugeActivity.this, ControlsActivity.class);
                startActivity(controls);
                break;
            case R.id.owner_details:
                Intent ownerDetails = new Intent(GaugeActivity.this, OwnerDetailsActivity.class);
                startActivity(ownerDetails);
                break;
            case R.id.lock:
                Intent lock = new Intent(GaugeActivity.this, PasswordLockActivity.class);
                startActivity(lock);
                break;
            case R.id.gauge:
                break;
        }

        DrawerLayout drawerLayout = findViewById(R.id.drawer_layout);
        drawerLayout.closeDrawer(GravityCompat.START);

        return true;
    }

    @Override
    public void onBackPressed() {
        if (drawerLayout.isDrawerOpen(GravityCompat.START) || drawerLayout.isDrawerOpen(GravityCompat.END)) {
            drawerLayout.closeDrawer(GravityCompat.START);
            drawerLayout.closeDrawer(GravityCompat.END);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        try {
            unregisterReceiver(locationDataReceiver);
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
        }
        try{
            unregisterReceiver(obdDataReceiver);
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        try {
            unregisterReceiver(locationDataReceiver);
            unregisterReceiver(obdDataReceiver);
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void turnOnSos() {
        ((DividedByZero) getApplication()).setSosModeOn(true);
    }

    @Override
    public void turnOffSos() {
        ((DividedByZero) getApplication()).setSosModeOn(false);
    }
}
