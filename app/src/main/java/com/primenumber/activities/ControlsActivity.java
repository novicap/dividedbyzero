package com.primenumber.activities;

import android.app.ProgressDialog;
import android.bluetooth.BluetoothAdapter;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.Toast;
import android.widget.ToggleButton;

import com.primenumber.application.DividedByZero;
import com.primenumber.data.Database;
import com.primenumber.dividedbyzero.R;
import com.primenumber.fragmentsandadapters.sosfragment.SosFragment;
import com.primenumber.services.BlackBoxService;
import com.primenumber.services.BluetoothOBD2Connection;
import com.primenumber.services.DrivingSessionService;
import com.primenumber.services.VehicleLockService;

public class ControlsActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener,
    SosFragment.SosClickListener{

    private DrawerLayout drawerLayout;
    private ActionBarDrawerToggle toggle;
    private Button lockCar, drive;
    private ToggleButton toggleButton;
    private Database database;
    private SosFragment sosFragment;
    private BluetoothOBD2Connection bluetoothOBD2Connection;
    private ProgressDialog progressDialog;
    private BroadcastReceiver obdDataReceiver;
    private boolean isTablet, checkErrors = true;
    private BluetoothAdapter bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        setContentView(R.layout.activity_controls_layout);
        setTitle("Controls");
        isTablet = getResources().getBoolean(R.bool.isTablet);
        bluetoothOBD2Connection = ((DividedByZero) getApplication()).getBluetoothConnection();
        toggleButton = findViewById(R.id.gaugeSpeedFeed);
        lockCar = findViewById(R.id.lock);
        drive = findViewById(R.id.drive);
        database = Database.getInstance(this);
        drive.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });
        lockCar.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.O)
            @Override
            public void onClick(View v) {
                database.updateLockState(1);
                Intent lock = new Intent(getApplicationContext(), VehicleLockService.class);
                lock.setAction(getApplicationContext().getPackageName()+"lock");
                startService(lock);
                Intent unlock = new Intent(ControlsActivity.this, UnlockActivity.class);
                unlock.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NO_HISTORY);
                startActivity(unlock);
            }
        });
        if(((DividedByZero)getApplication()).getDrivingModeOn()){
            drive.setText(getString(R.string.finish_driving_session));
        } else {
            drive.setText(getString(R.string.start_driving_session));
        }
        drive.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
            if(!((DividedByZero) getApplicationContext()).getDrivingModeOn()) {
                ((DividedByZero) getApplicationContext()).setDrivingModeOn(true);
                if (bluetoothOBD2Connection == null || !bluetoothAdapter.isEnabled()) {
                    Toast.makeText(ControlsActivity.this, "Can't do diagnostics check, vehicle not connected.", Toast.LENGTH_SHORT).show();
                    Intent intent = new Intent(ControlsActivity.this, BlackBoxService.class);
                    startService(intent);
                    Intent session = new Intent(ControlsActivity.this, DrivingSessionService.class);
                    startService(session);
                    drive.setText(R.string.finish_driving_session);
                } else {
                    bluetoothOBD2Connection.setErrorCodesCheck(0);
                    checkErrors = true;
                    progressDialog = ProgressDialog.show(ControlsActivity.this,"Checking error codes."
                            ,"Please Wait...",true);
                }
            } else {
                ((DividedByZero) getApplicationContext()).setDrivingModeOn(false);
                Intent intent = new Intent(ControlsActivity.this, BlackBoxService.class);
                stopService(intent);
                Intent session = new Intent(ControlsActivity.this, DrivingSessionService.class);
                stopService(session);
                drive.setText(R.string.start_driving_session);
            }
            }
        });
        if(database.getGaugeSpeedType() == 2){
            toggleButton.setTextOn(getString(R.string.obd2));
            toggleButton.setChecked(true);

        } else {
            toggleButton.setTextOff(getString(R.string.gps));
            toggleButton.setChecked(false);
        }

        toggleButton.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    toggleButton.setTextOn(getString(R.string.obd2));
                    database.updateGaugeSpeedType(2);
                } else {
                    toggleButton.setTextOff(getString(R.string.gps));
                    database.updateGaugeSpeedType(1);
                }
            }
        });

        sosFragment = new SosFragment();
        getFragmentManager().beginTransaction()
                .replace(R.id.sos_fragment, sosFragment)
                .commit();
        sosFragment.setSosActive(((DividedByZero) getApplicationContext()).getSosModeOn());

        Toolbar toolbar = findViewById(R.id.toolbar);
        drawerLayout = findViewById(R.id.drawer_layout);
        if(!isTablet) {
            toggle = new ActionBarDrawerToggle(this, drawerLayout, toolbar,
                    R.string.navigation_drawer_open, R.string.navigation_drawer_close);
            drawerLayout.addDrawerListener(toggle);
            toggle.syncState();
        }

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        NavigationView navigationView = findViewById(R.id.navigation_view);
        navigationView.setNavigationItemSelectedListener(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (obdDataReceiver == null) {
            obdDataReceiver = new BroadcastReceiver() {
                @Override
                public void onReceive(Context context, Intent intent) {
                    if((int)intent.getExtras().get("trouble_codes_checked") == 1 && progressDialog != null && checkErrors) {
                        if (!String.valueOf(intent.getExtras().get("trouble_codes_string")).equals("")) {
                            Toast.makeText(ControlsActivity.this, "It is not safe to drive, please " +
                                    "check your vehicle.", Toast.LENGTH_LONG).show();
                        } else {
                            Toast.makeText(ControlsActivity.this, "No trouble codes detected.",
                                    Toast.LENGTH_LONG).show();
                        }
                        checkErrors = false;
                        progressDialog.dismiss();
                        Intent blackBox = new Intent(ControlsActivity.this, BlackBoxService.class);
                        startService(blackBox);
                        Intent session = new Intent(ControlsActivity.this, DrivingSessionService.class);
                        startService(session);
                        drive.setText(R.string.finish_driving_session);
                    }
                }
            };
            registerReceiver(obdDataReceiver, new IntentFilter("obd_reading"));
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    protected void onPause() {
        super.onPause();
        try {
            unregisterReceiver(obdDataReceiver);
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        try {
            unregisterReceiver(obdDataReceiver);
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
        }
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        int id = item.getItemId();

        switch (id){
            case R.id.sensor_reading:
                Intent sensorReading = new Intent(ControlsActivity.this, SensorReadingActivity.class);
                startActivity(sensorReading);
                break;
            case R.id.emergency_sms:
                Intent emergencySms = new Intent(ControlsActivity.this, EmergencySmsActivity.class);
                startActivity(emergencySms);
                break;
            case R.id.vehicle_details:
                Intent vehicleDetails = new Intent(ControlsActivity.this, VehicleDetailsActivity.class);
                startActivity(vehicleDetails);
                break;
            case R.id.service_history:
                Intent vehicleService = new Intent(ControlsActivity.this, VehicleServiceActivity.class);
                startActivity(vehicleService);
                break;
            case R.id.bluetooth:
                Intent bluetooth = new Intent(ControlsActivity.this, BluetoothActivity.class);
                startActivity(bluetooth);
                break;
            case R.id.diagnostics:
                Intent diagnostics = new Intent(ControlsActivity.this, DiagnosticsActivity.class);
                startActivity(diagnostics);
                break;
            case R.id.dash_cam:
                Intent dashCam = new Intent(ControlsActivity.this, DashCamActivity.class);
                startActivity(dashCam);
                break;
            case R.id.controls:
                break;
            case R.id.owner_details:
                Intent ownerDetails = new Intent(ControlsActivity.this, OwnerDetailsActivity.class);
                startActivity(ownerDetails);
                break;
            case R.id.lock:
                Intent lock = new Intent(ControlsActivity.this, PasswordLockActivity.class);
                startActivity(lock);
                break;
            case R.id.gauge:
                Intent gauge = new Intent(ControlsActivity.this, GaugeActivity.class);
                startActivity(gauge);
                break;
        }
        if(!isTablet) {
            DrawerLayout drawerLayout = findViewById(R.id.drawer_layout);
            drawerLayout.closeDrawer(GravityCompat.START);
        }
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (toggle.onOptionsItemSelected(item)){
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void turnOnSos() {
        ((DividedByZero) getApplication()).setSosModeOn(true);
    }

    @Override
    public void turnOffSos() {
        ((DividedByZero) getApplication()).setSosModeOn(false);
    }
}
