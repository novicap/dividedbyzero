package com.primenumber.activities;

import android.app.FragmentManager;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.WindowManager;

import com.primenumber.application.DividedByZero;
import com.primenumber.dividedbyzero.R;
import com.primenumber.fragmentsandadapters.ownerdetails.OwnerDetailsEditFragment;
import com.primenumber.fragmentsandadapters.ownerdetails.OwnerDetailsFragment;
import com.primenumber.fragmentsandadapters.sosfragment.SosFragment;

/**
 * Created by novica on 3/17/18.
 */

public class OwnerDetailsActivity extends AppCompatActivity implements OwnerDetailsEditFragment.OwnerDetailsEditedListener,
    OwnerDetailsFragment.EditOwnerDetailsListener, NavigationView.OnNavigationItemSelectedListener, SosFragment.SosClickListener{

    private OwnerDetailsFragment ownerDetailsFragment;
    private OwnerDetailsEditFragment ownerDetailsEditFragment;
    private FragmentManager fragmentManager;
    private DrawerLayout drawerLayout;
    private ActionBarDrawerToggle toggle;
    private SosFragment sosFragment;
    private boolean isTablet;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setTitle("Security details");
        isTablet = getResources().getBoolean(R.bool.isTablet);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        setContentView(R.layout.activity_owner_details_layout);
        fragmentManager = getFragmentManager();
        ownerDetailsEditFragment = new OwnerDetailsEditFragment();
        ownerDetailsFragment = new OwnerDetailsFragment();
        sosFragment = new SosFragment();
        fragmentManager.beginTransaction()
                .replace(R.id.fragment_holder, ownerDetailsFragment)
                .replace(R.id.sos_fragment, sosFragment)
                .commit();

        sosFragment.setSosActive(((DividedByZero) getApplicationContext()).getSosModeOn());

        Toolbar toolbar = findViewById(R.id.toolbar);

        drawerLayout = findViewById(R.id.drawer_layout);
        if(!isTablet) {
            toggle = new ActionBarDrawerToggle(this, drawerLayout, toolbar,
                    R.string.navigation_drawer_open, R.string.navigation_drawer_close);
            drawerLayout.addDrawerListener(toggle);
            toggle.syncState();
        }

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        NavigationView navigationView = findViewById(R.id.navigation_view);
        navigationView.setNavigationItemSelectedListener(this);
    }

    @Override
    public void editOwner() {
        fragmentManager.beginTransaction()
                .replace(R.id.fragment_holder, ownerDetailsEditFragment)
                .addToBackStack(null)
                .commit();
    }

    @Override
    public void edited() {
        fragmentManager.popBackStack();
        fragmentManager.beginTransaction()
                .replace(R.id.fragment_holder, ownerDetailsFragment)
                .commit();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (toggle.onOptionsItemSelected(item)){
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        int id = item.getItemId();

        switch (id){
            case R.id.sensor_reading:
                Intent sensorReading = new Intent(OwnerDetailsActivity.this, SensorReadingActivity.class);
                startActivity(sensorReading);
                break;
            case R.id.emergency_sms:
                Intent emergencySms = new Intent(OwnerDetailsActivity.this, EmergencySmsActivity.class);
                startActivity(emergencySms);
                break;
            case R.id.vehicle_details:
                Intent vehicleDetails = new Intent(OwnerDetailsActivity.this, VehicleDetailsActivity.class);
                startActivity(vehicleDetails);
                break;
            case R.id.service_history:
                Intent vehicleService = new Intent(OwnerDetailsActivity.this, VehicleServiceActivity.class);
                startActivity(vehicleService);
                break;
            case R.id.diagnostics:
                Intent diagnostics = new Intent(OwnerDetailsActivity.this, DiagnosticsActivity.class);
                startActivity(diagnostics);
                break;
            case R.id.dash_cam:
                Intent dashCam = new Intent(OwnerDetailsActivity.this, DashCamActivity.class);
                startActivity(dashCam);
                break;
            case R.id.controls:
                Intent controls = new Intent(OwnerDetailsActivity.this, ControlsActivity.class);
                startActivity(controls);
                break;
            case R.id.bluetooth:
                Intent bluetooth = new Intent(OwnerDetailsActivity.this, BluetoothActivity.class);
                startActivity(bluetooth);
                break;
            case R.id.owner_details:
                break;
            case R.id.lock:
                Intent lock = new Intent(OwnerDetailsActivity.this, PasswordLockActivity.class);
                startActivity(lock);
                break;
            case R.id.gauge:
                Intent gauge = new Intent(OwnerDetailsActivity.this, GaugeActivity.class);
                startActivity(gauge);
                break;
        }
        if(!isTablet) {
            DrawerLayout drawerLayout = findViewById(R.id.drawer_layout);
            drawerLayout.closeDrawer(GravityCompat.START);
        }
        return true;
    }

    @Override
    public void turnOnSos() {
        ((DividedByZero) getApplication()).setSosModeOn(true);
    }

    @Override
    public void turnOffSos() {
        ((DividedByZero) getApplication()).setSosModeOn(false);
    }
}
