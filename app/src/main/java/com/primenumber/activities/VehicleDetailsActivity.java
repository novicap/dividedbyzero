package com.primenumber.activities;

import android.app.FragmentManager;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.WindowManager;

import com.primenumber.application.DividedByZero;
import com.primenumber.dividedbyzero.R;
import com.primenumber.fragmentsandadapters.sosfragment.SosFragment;
import com.primenumber.fragmentsandadapters.vehicledetails.VehicleDetailsEditFragment;
import com.primenumber.fragmentsandadapters.vehicledetails.VehicleDetailsFragment;

/**
 * Created by novica on 2/21/18.
 */

public class VehicleDetailsActivity extends AppCompatActivity implements
        NavigationView.OnNavigationItemSelectedListener, VehicleDetailsFragment.CarDetailsListListener,
        VehicleDetailsEditFragment.VehicleDetailsListener, SosFragment.SosClickListener{

    private ActionBarDrawerToggle toggle;
    private VehicleDetailsFragment vehicleDetailsFragment;
    private VehicleDetailsEditFragment vehicleDetailsEditFragment;
    private FragmentManager fragmentManager;
    private DrawerLayout drawerLayout;
    private SosFragment sosFragment;
    private boolean isTablet;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_vehicle_details_layout);
        setTitle("Vehicle details");
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        isTablet = getResources().getBoolean(R.bool.isTablet);
        fragmentManager = getFragmentManager();
        vehicleDetailsFragment = new VehicleDetailsFragment();
        vehicleDetailsEditFragment = new VehicleDetailsEditFragment();
        sosFragment = new SosFragment();

        fragmentManager.beginTransaction()
                .replace(R.id.fragment_holder, vehicleDetailsFragment)
                .replace(R.id.sos_fragment, sosFragment)
                .commit();
        Toolbar toolbar = findViewById(R.id.toolbar);

        drawerLayout = findViewById(R.id.drawer_layout);
        if(!isTablet) {
            toggle = new ActionBarDrawerToggle(this, drawerLayout, toolbar,
                    R.string.navigation_drawer_open, R.string.navigation_drawer_close);
            drawerLayout.addDrawerListener(toggle);
            toggle.syncState();
        }

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        NavigationView navigationView = findViewById(R.id.navigation_view);
        navigationView.setNavigationItemSelectedListener(this);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (toggle.onOptionsItemSelected(item)){
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        int id = item.getItemId();

        switch (id){
            case R.id.sensor_reading:
                Intent sensorReading = new Intent(VehicleDetailsActivity.this, SensorReadingActivity.class);
                startActivity(sensorReading);
                break;
            case R.id.emergency_sms:
                Intent emergencySms = new Intent(VehicleDetailsActivity.this, EmergencySmsActivity.class);
                startActivity(emergencySms);
                break;
            case R.id.vehicle_details:
                break;
            case R.id.service_history:
                Intent vehicleService = new Intent(VehicleDetailsActivity.this, VehicleServiceActivity.class);
                startActivity(vehicleService);
                break;
            case R.id.diagnostics:
                Intent diagnostics = new Intent(VehicleDetailsActivity.this, DiagnosticsActivity.class);
                startActivity(diagnostics);
                break;
            case R.id.dash_cam:
                Intent dashCam = new Intent(VehicleDetailsActivity.this, DashCamActivity.class);
                startActivity(dashCam);
                break;
            case R.id.controls:
                Intent controls = new Intent(VehicleDetailsActivity.this, ControlsActivity.class);
                startActivity(controls);
                break;
            case R.id.bluetooth:
                Intent bluetooth = new Intent(VehicleDetailsActivity.this, BluetoothActivity.class);
                startActivity(bluetooth);
                break;
            case R.id.owner_details:
                Intent ownerDetails = new Intent(VehicleDetailsActivity.this, OwnerDetailsActivity.class);
                startActivity(ownerDetails);
                break;
            case R.id.lock:
                Intent lock = new Intent(VehicleDetailsActivity.this, PasswordLockActivity.class);
                startActivity(lock);
                break;
            case R.id.gauge:
                Intent gauge = new Intent(VehicleDetailsActivity.this, GaugeActivity.class);
                startActivity(gauge);
                break;
        }
        if(!isTablet) {
            DrawerLayout drawerLayout = findViewById(R.id.drawer_layout);
            drawerLayout.closeDrawer(GravityCompat.START);
        }
        return true;
    }

    @Override
    public void editCarDetails() {
        fragmentManager.beginTransaction()
                .replace(R.id.fragment_holder, vehicleDetailsEditFragment)
                .addToBackStack(null)
                .commit();
    }

    @Override
    public void vehicleDetailsUpdated() {
        fragmentManager.popBackStack();
        fragmentManager.beginTransaction()
                .replace(R.id.fragment_holder, vehicleDetailsFragment)
                .commit();
    }

    @Override
    public void onBackPressed() {
        if (drawerLayout.isDrawerOpen(GravityCompat.START)) {
            drawerLayout.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public void turnOnSos() {
        ((DividedByZero) getApplication()).setSosModeOn(true);
    }

    @Override
    public void turnOffSos() {
        ((DividedByZero) getApplication()).setSosModeOn(false);
    }
}
