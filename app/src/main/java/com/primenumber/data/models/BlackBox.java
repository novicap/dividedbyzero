package com.primenumber.data.models;


public class BlackBox {

    private int id, rpm, gasPedalPosition;
    private double latitude, longitude;
    private float speed, speedGps;
    private String carError, createdAt, gforce;

    public BlackBox(){}

    public String getGforce(){
        return gforce;
    }

    public void setGforce(String gforce){
        this.gforce = gforce;
    }

    public float getSpeedGps(){
        return speedGps;
    }

    public void setSpeedGps(float speedGps){
        this.speedGps = speedGps;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getRpm() {
        return rpm;
    }

    public void setRpm(int rpm) {
        this.rpm = rpm;
    }

    public int getGasPedalPosition() {
        return gasPedalPosition;
    }

    public void setGasPedalPosition(int gasPedalPosition) {
        this.gasPedalPosition = gasPedalPosition;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public float getSpeed() {
        return speed;
    }

    public void setSpeed(float speed) {
        this.speed = speed;
    }

    public String getCarError() {
        return carError;
    }

    public void setCarError(String carError) {
        this.carError = carError;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }
}
