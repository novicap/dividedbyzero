package com.primenumber.data.models;

/**
 * Created by novica on 3/5/18.
 */

public class TroubleCode {
    private String code, description;

    public TroubleCode(String code, String description){
        this.code = code;
        this.description = description;
    }

    public TroubleCode(){
    }

    public void setCode(String code){
        this.code = code;
    }

    public void setDescription(String description){
        this.description = description;
    }

    public String getCode(){
        return code;
    }

    public String getDescription(){
        return description;
    }
}
