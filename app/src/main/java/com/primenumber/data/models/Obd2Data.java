package com.primenumber.data.models;

import java.util.ArrayList;
import java.util.HashSet;

import static java.lang.Integer.parseInt;

/**
 * Created by novica on 3/2/18.
 */

public class Obd2Data {
    private boolean initialized = false;
    private int rpm = 0, speed = 0, engineTemperature = 0, throtthePosition = 0, errorsChecked = 0;
    private String[] INITIALIZATION_SEQUENCE = { "AT D", "AT Z", "AT L0", "AT H0", "AT SP 0", "AT E0"};
    private HashSet<String> troubleCodesSet = new HashSet<>();
    private ArrayList<String> troubleCodes = new ArrayList<>();

    public Obd2Data(){
    }

    public String acquireRpm(){
        return "01 0c";
    }

    public void setRpm(String rpm){
        this.rpm = (parseInt(rpm.substring(0,2), 16)*256+parseInt(rpm.substring(2,4), 16))/4;
    }

    public int getRpm(){
        return rpm;
    }

    public String acquireThrottlePosition(){
        return "01 11";
    }

    public void setThrotthePosition(String throttlePosition){
        this.throtthePosition = 100*parseInt(throttlePosition.substring(0, 2), 16)/255;
    }

    public int getThrotthePosition(){
        return throtthePosition;
    }

    public String acquireSpeed(){
        return "01 0D";
    }

    public void setSpeed(String speed){
        this.speed = parseInt(speed.substring(0, 2), 16);
    }

    public int getSpeed(){
        return speed;
    }

    public String acquireEngineTemperature(){
        return "01 05";
    }

    public void setEngineTemperature(String engineTemperature){
        this.engineTemperature = parseInt(engineTemperature.substring(0, 2), 16) - 40;
    }

    public int getEngineTemperature(){
       return engineTemperature;
    }

    public String acquireErrorCodes(){
        return "03";
    }

    public ArrayList<String> getTroubleCodes(){
        return troubleCodes;
    }

    public String getTroubleCodesString(){
        String codes = "";

        //to avoid ConcurrentModificationException
        String[] tcs = new String[troubleCodes.size()];
        tcs = troubleCodes.toArray(tcs);

        for(String troubleCode : tcs){
            codes += troubleCode+"\r";
        }

        return codes;
    }

    public void setErrorCodesChecked(int status){
        errorsChecked = status;
    }

    public int getErrorsChecked() {
        return errorsChecked;
    }

    private void setTroubleCodes(String troubleCode){
        int steps = troubleCode.length()/4;

        for (int i = 0; i < steps; i++) {
            if (!troubleCode.substring(4 * i, 4 * (i + 1)).equals("0000")) {
                String code = getCodeType(troubleCode.substring(4 * i, 4 * i + 1)) + troubleCode.substring(4 * i + 1, 4 * (i + 1));

                troubleCodesSet.add(code);
            }
        }

        troubleCodes = new ArrayList<>(troubleCodesSet);
    }

    private String getCodeType(String code){
        switch (code){
            case "0":
                return "P0";
            case "1":
                return "P1";
            case "2":
                return "P2";
            case "3":
                return "P3";
            case "4":
                return "C0";
            case "5":
                return "C1";
            case "6":
                return "C2";
            case "7":
                return "C3";
            case "8":
                return "B0";
            case "9":
                return "B1";
            case "A":
                return "B2";
            case "B":
                return "B3";
            case "C":
                return "U0";
            case "D":
                return "U1";
            case "E":
                return "U2";
            case "F":
                return "U3";
        }

        return "00";
    }

    public String[] getInitializationSequense(){
        return INITIALIZATION_SEQUENCE;
    }

    public boolean getInitialized(){
        return initialized;
    }

    public void setInitialized(boolean initialized){
        this.initialized = initialized;
    }

    public String getDrivingData(int request){
        if (request % 50 == 0) {
            return acquireErrorCodes();
        } else if (request % 10 == 0) {
            return acquireEngineTemperature();
        } else if (request % 3 == 2) {
            return acquireThrottlePosition();
        } else if (request % 3 == 1) {
            return acquireSpeed();
        } else if (request % 3 == 0) {
            return acquireRpm();
        }

        return acquireRpm();
    }

    public void setParameter(String message){
        if(message.length() == 8 && message.substring(0, 4).equals("410C")){
            setRpm(message.substring(4, 8));
        }
        else if(message.length() == 6 && message.substring(0, 4).equals("410D")){
            setSpeed(message.substring(4, 6));
        }
        else if(message.length() == 6 && message.substring(0, 4).equals("4111")){
            setThrotthePosition(message.substring(4, 6));
        }
        else if(message.length() == 6 && message.substring(0, 4).equals("4105")){
            setEngineTemperature(message.substring(4, 6));
        }
        else if(message.length() == 6 && message.equals("NODATA")){
            troubleCodes.clear();
            errorsChecked = 1;
        }
        else if(message.length() > 4 && message.substring(0, 2).equals("43")){
            setTroubleCodes(message.substring(2, message.length()));
            errorsChecked = 1;
        }
    }
}
