package com.primenumber.data.models;

/**
 * Created by novica on 2/15/18.
 */

public class EmergencySms {

    private int id;
    private String phoneNumber, message, createdAt;

    public EmergencySms() {
    }

    public EmergencySms(int id, String phoneNumber, String message, String createdAt) {
        this.id = id;
        this.phoneNumber = phoneNumber;
        this.message = message;
        this.createdAt = createdAt;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        if(message.length() != 0){
            this.message = message;
        }
        else{
            this.message = "I had car accident, send help to location.";
        }
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }


}
