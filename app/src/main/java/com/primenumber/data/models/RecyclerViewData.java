package com.primenumber.data.models;

/**
 * Created by novica on 2/10/18.
 */

public class RecyclerViewData {
    private String title;
    private int icon;
    private String reading;

    public RecyclerViewData(String title, String reading, int icon)
    {
        this.title = title;
        this.reading = reading;
        this.icon = icon;
    }

    public void setTitle(String title){
        this.title = title;
    }

    public void setIcon(int icon){
        this.icon = icon;
    }

    public void setReading(String reading)
    {
        this.reading = reading;
    }

    public String getTitle(){
        return this.title;
    }

    public String getReading(){
        return this.reading;
    }

    public int getIcon(){
        return this.icon;
    }
}
