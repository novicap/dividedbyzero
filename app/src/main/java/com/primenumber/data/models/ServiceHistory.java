package com.primenumber.data.models;

/**
 * Created by novica on 2/15/18.
 */

public class ServiceHistory {

    private int id, engineOil, oilFilter, airFilter, cabinFilter, fuelFilter, brakeFluid, powerSteeringFluid,
        transmissionFluid, timingBelt, timingChain, odometer;
    private String createdAt;

    public ServiceHistory() {}

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getEngineOil() {
        return engineOil;
    }

    public void setEngineOil(int engineOil) {
        this.engineOil = engineOil;
    }

    public int getOilFilter() {
        return oilFilter;
    }

    public void setOilFilter(int oilFilter) {
        this.oilFilter = oilFilter;
    }

    public int getAirFilter() {
        return airFilter;
    }

    public void setAirFilter(int airFilter) {
        this.airFilter = airFilter;
    }

    public int getCabinFilter() {
        return cabinFilter;
    }

    public void setCabinFilter(int cabinFilter) {
        this.cabinFilter = cabinFilter;
    }

    public int getFuelFilter() {
        return fuelFilter;
    }

    public void setFuelFilter(int fuelFilter) {
        this.fuelFilter = fuelFilter;
    }

    public int getBrakeFluid() {
        return brakeFluid;
    }

    public void setBrakeFluid(int brakeFluid) {
        this.brakeFluid = brakeFluid;
    }

    public int getPowerSteeringFluid() {
        return powerSteeringFluid;
    }

    public void setPowerSteeringFluid(int powerSteeringFluid) {
        this.powerSteeringFluid = powerSteeringFluid;
    }

    public int getTransmissionFluid() {
        return transmissionFluid;
    }

    public void setTransmissionFluid(int transmissionFluid) {
        this.transmissionFluid = transmissionFluid;
    }

    public int getTimingBelt() {
        return timingBelt;
    }

    public void setTimingBelt(int timingBelt) {
        this.timingBelt = timingBelt;
    }

    public int getTimingChain() {
        return timingChain;
    }

    public void setTimingChain(int timingChain) {
        this.timingChain = timingChain;
    }

    public int getOdometer() {
        return odometer;
    }

    public void setOdometer(int odometer) {
        this.odometer = odometer;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }
}
