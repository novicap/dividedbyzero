package com.primenumber.data;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.primenumber.data.models.BlackBox;
import com.primenumber.data.models.CarDetails;
import com.primenumber.data.models.EmergencySms;
import com.primenumber.data.models.Owner;
import com.primenumber.data.models.ServiceHistory;
import com.primenumber.data.models.TroubleCode;
import com.primenumber.helpers.HashHelper;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by novica on 2/8/18.
 */

public class Database extends SQLiteOpenHelper{

    private static Database instance = null;
    private Context context;
    private String DB_PATH;
    private String defaultPassword = "12345";
    private static final String DATABASE_TO_COPY_NAME = "obd-trouble-codes.sqlite";
    private static final String DATABASE_NAME = "divided_by_zero.db";
    private static final int DATABASE_VERSION = 1;
    private static final int BLACK_BOX_MAX_ENTRY = 604800;

    //Tables
    private static final String TABLE_BLACK_BOX = "black_box";
    private static final String TABLE_CAR_DETAILS = "car_details";
    private static final String TABLE_EMERGENCY_SMS = "emergency_sms";
    private static final String TABLE_OWNER = "owner";
    private static final String TABLE_SERVICE_HISTORY = "service_history";
    private static final String TABLE_TROUBLE_CODES = "trouble_codes";
    private static final String TABLE_LOCK_STATE = "lock_state";
    private static final String TABLE_LOCK = "table_lock";
    private static final String TABLE_GAUGE_SPEED_TYPE = "gauge_speed_type";

    //Common columns
    private static final String KEY_ID = "id";
    private static final String KEY_CREATED_AT = "created_at";

    //Lock state columns
    private static final String KEY_LOCK_STATE = "lock_state";

    //Gauge speed type columns
    private static final String GAUGE_SPEED_TYPE = "gauge_speed_type";

    //Black box columns
    private static final String KEY_LATITUDE = "latitude";
    private static final String KEY_LONGITUDE = "longitude";
    private static final String KEY_SPEED = "speed";
    private static final String KEY_SPEED_GPS = "speed_gps";
    private static final String KEY_GFORCE = "gforce";
    private static final String KEY_RPM = "rpm";
    private static final String KEY_GAS_PEDAL_POSITION = "gas_pedal_position";
    private static final String KEY_CAR_ERROR = "car_error";

    //Car details columns
    private static final String KEY_CAR_MANUFACTURER = "car_manufacturer";
    private static final String KEY_CAR_MODEL = "car_model";
    private static final String KEY_CAR_MANUFACTURING_YEAR = "car_manufacturing_year";
    private static final String KEY_ENGINE_DISPLACEMENT = "engine_displacement";
    private static final String KEY_ENGINE_POWER = "engine_power";
    private static final String KEY_FUEL_TYPE = "fuel_type";
    private static final String KEY_TRANSMISSION_TYPE = "transmission_type";
    private static final String KEY_MAX_SPEED = "max_speed";
    private static final String KEY_MAX_RPM = "max_rpm";

    //Car owners details
    private static final String KEY_OWNERS_NAME = "name";
    private static final String KEY_OWNERS_EMERGENCY_PHONE = "emergency_phone";
    private static final String KEY_OWNERS_MESSAGE = "message";

    //Emergency phones columns
    private static final String KEY_PHONE_NUMBER = "phone_number";
    private static final String KEY_MESSAGE = "message";

    //Service history keys
    private static final String KEY_ENGINE_OIL = "engine_oil";
    private static final String KEY_OIL_FILTER = "oil_filter";
    private static final String KEY_AIR_FILTER = "air_filter";
    private static final String KEY_FUEL_FILTER = "fuel_filter";
    private static final String KEY_CABIN_FILTER = "cabin_filter";
    private static final String KEY_POWER_STEERING_FLUID = "power_steering_fluid";
    private static final String KEY_TRANSMISSION_FLUID = "transmission_fluid";
    private static final String KEY_BRAKE_FLUID = "brake_fluid";
    private static final String KEY_TIMING_BELT = "timing_belt";
    private static final String KEY_TIMING_CHAIN = "timing_chain";
    private static final String KEY_ODOMETER = "odometer";

    //Lock key
    private static final String KEY_PASSWORD = "password";

    //Trouble codes
    private static final String KEY_CODE = "id";
    private static final String KEY_DESCRIPTION = "desc";

    //Table create statements
    private static final String CREATE_TABLE_BLACK_BOX = "CREATE TABLE " +
            TABLE_BLACK_BOX + "(" + KEY_ID + " INTEGER PRIMARY KEY AUTOINCREMENT," +
            KEY_LATITUDE + " REAL," + KEY_LONGITUDE + " REAL," + KEY_SPEED + " REAL," + KEY_SPEED_GPS + " REAL," +
            KEY_GFORCE + " REAL," + KEY_RPM + " INTEGER," + KEY_GAS_PEDAL_POSITION + " INTEGER,"
            + KEY_CAR_ERROR + " TEXT," + KEY_CREATED_AT + " DATETIME)";


    private static final String CREATE_TABLE_CAR_DETAILS = "CREATE TABLE " +
            TABLE_CAR_DETAILS + "(" + KEY_ID + " INTEGER PRIMARY KEY AUTOINCREMENT," + KEY_CAR_MANUFACTURER
            + " TEXT," + KEY_CAR_MANUFACTURING_YEAR + " INTEGER," + KEY_CAR_MODEL + " TEXT,"
            + KEY_ENGINE_DISPLACEMENT + " INTEGER," + KEY_ENGINE_POWER + " INTEGER," +  KEY_FUEL_TYPE
            + " TEXT," + KEY_TRANSMISSION_TYPE + " TEXT," + KEY_MAX_SPEED + " INTEGER," + KEY_MAX_RPM
            + " INTEGER," + KEY_CREATED_AT + " DATETIME)";

    private static final String CREATE_TABLE_EMERGENCY_SMS = "CREATE TABLE " +
            TABLE_EMERGENCY_SMS + "(" + KEY_ID + " INTEGER PRIMARY KEY AUTOINCREMENT," + KEY_PHONE_NUMBER
            + " TEXT," + KEY_MESSAGE + " TEXT," + KEY_CREATED_AT + " DATETIME)";

    private static final String CREATE_TABLE_OWNER = "CREATE TABLE " +
            TABLE_OWNER + "(" + KEY_ID + " INTEGER PRIMARY KEY AUTOINCREMENT," + KEY_OWNERS_NAME +
            " TEXT," + KEY_OWNERS_EMERGENCY_PHONE + " TEXT," +
            KEY_OWNERS_MESSAGE + " TEXT," + KEY_CREATED_AT + " DATETIME)";

    private static final String CREATE_TABLE_LOCK = "CREATE TABLE " + TABLE_LOCK + " (" + KEY_PASSWORD + ")";

    private static final String CREATE_TABLE_GAUGE_SPEED_TYPE = "CREATE TABLE " + TABLE_GAUGE_SPEED_TYPE + " (" + GAUGE_SPEED_TYPE + " INTEGER)";

    private static final String CREATE_TABLE_SERVICE_HISTORY = "CREATE TABLE " +
            TABLE_SERVICE_HISTORY + "(" + KEY_ID + " INTEGER PRIMARY KEY AUTOINCREMENT," +
            KEY_ENGINE_OIL + " INTEGER," + KEY_OIL_FILTER + " INTEGER," + KEY_AIR_FILTER +
            " INTEGER," + KEY_CABIN_FILTER + " INTEGER," + KEY_FUEL_FILTER + " INTEGER,"  +
            KEY_BRAKE_FLUID + " INTEGER," + KEY_POWER_STEERING_FLUID + " INTEGER," +
            KEY_TRANSMISSION_FLUID + " INTEGER,"  + KEY_TIMING_BELT + " INTEGER," +
            KEY_TIMING_CHAIN + " INTEGER," + KEY_ODOMETER + " INTEGER," + KEY_CREATED_AT +
            " DATETIME)";

    private static final String CREATE_TABLE_TROUBLE_CODES = "CREATE TABLE " + TABLE_TROUBLE_CODES +
            "( " + KEY_CODE + " TEXT NOT NULL PRIMARY KEY," + KEY_DESCRIPTION + " TEXT NOT NULL )";

    private static final String INSERT_INITIAL_CAR_DETAILS = "INSERT INTO " + TABLE_CAR_DETAILS +
            " (" + KEY_CAR_MANUFACTURER + "," + KEY_CAR_MANUFACTURING_YEAR + "," + KEY_CAR_MODEL + ","
            + KEY_ENGINE_DISPLACEMENT + "," + KEY_ENGINE_POWER + "," + KEY_FUEL_TYPE + ","
            + KEY_TRANSMISSION_TYPE + "," + KEY_MAX_SPEED + "," + KEY_MAX_RPM + ")" +
            "VALUES ('manufacturer', '2000', 'model', '1600', '120', 'diesel', 'manual', '240', '6000')";

    private static final String INSERT_INITIAL_LOCK_STATE = "INSERT INTO " + TABLE_LOCK_STATE + " (" +
            KEY_LOCK_STATE + " ) VALUES " + "( '0' )";

    private static final String INSERT_INITIAL_OWNER = "INSERT INTO " + TABLE_OWNER + " (" + KEY_OWNERS_NAME + ", " +
             KEY_OWNERS_EMERGENCY_PHONE + ", " + KEY_OWNERS_MESSAGE + " ) VALUES ('Name not set.', 'Phone not set'," +
            " 'Message not set')";


    private static final String CREATE_TABLE_LOCK_STATE = "CREATE TABLE " + TABLE_LOCK_STATE + " (" +
            KEY_LOCK_STATE + " INTEGER )";

    private static final String COPY_DATA_FROM_TC_DATABASE = "INSERT INTO '" + TABLE_TROUBLE_CODES
            + "' (" + KEY_CODE + "," + KEY_DESCRIPTION + ") SELECT 'id', 'desc' FROM '"
            + DATABASE_TO_COPY_NAME + ".codes'";
    private static final String DETACH_TC_DATABASE = "DETACH DATABASE 'TC'";

    public static Database getInstance(Context context){
        if(instance == null){
            instance = new Database(context);
        }

        return instance;
    }

    private Database(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);

        this.context = context;

        DB_PATH = "/data/data/" + context.getPackageName() + "/databases/";
    }

    private boolean checkDataBase()
    {
        File dbFile = new File(DB_PATH + DATABASE_TO_COPY_NAME);
        return dbFile.exists();
    }

    private void copyDataBase() throws IOException
    {
        InputStream mInput = context.getAssets().open(DATABASE_TO_COPY_NAME);
        String outFileName = DB_PATH + DATABASE_TO_COPY_NAME;
        OutputStream mOutput = new FileOutputStream(outFileName);
        byte[] mBuffer = new byte[1024];
        int mLength;
        while ((mLength = mInput.read(mBuffer))>0)
        {
            mOutput.write(mBuffer, 0, mLength);
        }
        mOutput.flush();
        mOutput.close();
        mInput.close();
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        sqLiteDatabase.execSQL(CREATE_TABLE_BLACK_BOX);
        sqLiteDatabase.execSQL(CREATE_TABLE_CAR_DETAILS);
        sqLiteDatabase.execSQL(INSERT_INITIAL_CAR_DETAILS);
        sqLiteDatabase.execSQL(CREATE_TABLE_OWNER);
        sqLiteDatabase.execSQL(CREATE_TABLE_LOCK);
        sqLiteDatabase.execSQL(CREATE_TABLE_LOCK_STATE);
        sqLiteDatabase.execSQL(CREATE_TABLE_EMERGENCY_SMS);
        sqLiteDatabase.execSQL(CREATE_TABLE_SERVICE_HISTORY);
        sqLiteDatabase.execSQL(CREATE_TABLE_TROUBLE_CODES);
        sqLiteDatabase.execSQL(CREATE_TABLE_GAUGE_SPEED_TYPE);
        sqLiteDatabase.execSQL(INSERT_INITIAL_LOCK_STATE);
        sqLiteDatabase.execSQL(INSERT_INITIAL_OWNER);
        createDefaultPassword(sqLiteDatabase);
        createDefaultGaugeSpeedType(sqLiteDatabase);
        sqLiteDatabase.setTransactionSuccessful();
        sqLiteDatabase.endTransaction();
        if(!checkDataBase()){
            try{
                copyDataBase();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        sqLiteDatabase.execSQL("ATTACH DATABASE '" + DB_PATH + DATABASE_TO_COPY_NAME +"' AS 'TC'");
        sqLiteDatabase.execSQL("INSERT INTO " + TABLE_TROUBLE_CODES +" SELECT * FROM TC.codes");
        sqLiteDatabase.execSQL(DETACH_TC_DATABASE);
        sqLiteDatabase.beginTransaction();
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {

    }

    public long createDefaultGaugeSpeedType(SQLiteDatabase db) {
        ContentValues contentValues = new ContentValues();
        contentValues.put(GAUGE_SPEED_TYPE, 1);
        return db.insert(TABLE_GAUGE_SPEED_TYPE, null, contentValues);
    }

    public int getGaugeSpeedType(){
        SQLiteDatabase db = this.getWritableDatabase();
        int gaugeSpeedType = 0;
        String selectQuery = "SELECT * FROM "+TABLE_GAUGE_SPEED_TYPE;
        Cursor cursor = db.rawQuery(selectQuery, null);
        if(cursor.moveToFirst()){
            gaugeSpeedType = cursor.getInt(0);
        }

        closeCursor(cursor);
        return gaugeSpeedType;
    }

    public long updateGaugeSpeedType(int speedType) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues contentValues = new ContentValues();
        contentValues.put(GAUGE_SPEED_TYPE, speedType);

        return db.update(TABLE_GAUGE_SPEED_TYPE, contentValues, null,
                null );
    }


    public long createDefaultPassword(SQLiteDatabase db){
        HashHelper hashHelper = new HashHelper();
        ContentValues contentValues = new ContentValues();
        try {
            contentValues.put("password", hashHelper.sha1(defaultPassword));
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }

        return db.insert(TABLE_LOCK, null, contentValues);
    }

    public String getPassword(){
        SQLiteDatabase db = this.getWritableDatabase();
        String selectQuery = "SELECT * FROM " + TABLE_LOCK;
        Cursor cursor = db.rawQuery(selectQuery, null);
        String password = null;
        if(cursor.moveToFirst()){
            password = cursor.getString(0);
        }

        closeCursor(cursor);
        return password;
    }

    public long updatePassword(String password){
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues contentValues = new ContentValues();
        contentValues.put(KEY_PASSWORD, password);

        return db.update(TABLE_LOCK, contentValues, null,
                null );
    }

    public int getLockState(){
        SQLiteDatabase db = this.getWritableDatabase();
        String selectQuery = "SELECT * FROM " + TABLE_LOCK_STATE;
        Cursor cursor = db.rawQuery(selectQuery, null);
        int state = 0;
        if(cursor.moveToFirst()){
            state = cursor.getInt(0);
        }

        closeCursor(cursor);
        return state;
    }

    public long updateLockState(int lockState){
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues contentValues = new ContentValues();
        contentValues.put(KEY_LOCK_STATE, lockState);

        return db.update(TABLE_LOCK_STATE, contentValues, null,
                null );
    }

    public long createBlackBoxEntry(BlackBox blackBox){
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues contentValues = new ContentValues();
        contentValues.put(KEY_LATITUDE, blackBox.getLatitude());
        contentValues.put(KEY_LONGITUDE, blackBox.getLongitude());
        contentValues.put(KEY_SPEED, blackBox.getSpeed());
        contentValues.put(KEY_SPEED_GPS, blackBox.getSpeedGps());
        contentValues.put(KEY_RPM, blackBox.getRpm());
        contentValues.put(KEY_GFORCE, blackBox.getGforce());
        contentValues.put(KEY_GAS_PEDAL_POSITION, blackBox.getGasPedalPosition());
        contentValues.put(KEY_CAR_ERROR, blackBox.getCarError());
        contentValues.put(KEY_CREATED_AT, blackBox.getCreatedAt());

        long id = db.insert(TABLE_BLACK_BOX, null, contentValues);
        long step = id / BLACK_BOX_MAX_ENTRY;
        if(step > 0) {
            long delete = id - step*BLACK_BOX_MAX_ENTRY;
            db.delete(TABLE_BLACK_BOX, KEY_ID + " = ?",
                    new String[] {String.valueOf(delete)});
        }

        return id;
    }

    public long createEmergencySms(EmergencySms emergencySms){
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues contentValues = new ContentValues();
        contentValues.put(KEY_PHONE_NUMBER, emergencySms.getPhoneNumber());
        contentValues.put(KEY_MESSAGE, emergencySms.getMessage());
        contentValues.put(KEY_CREATED_AT, emergencySms.getCreatedAt());

        return db.insert(TABLE_EMERGENCY_SMS, null, contentValues);
    }

    public int updateEmergencySms(EmergencySms emergencySms){
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues contentValues = new ContentValues();
        contentValues.put(KEY_PHONE_NUMBER, emergencySms.getPhoneNumber());
        contentValues.put(KEY_MESSAGE, emergencySms.getMessage());

        return db.update(TABLE_EMERGENCY_SMS, contentValues, KEY_ID + " = ?",
                new String[] { String.valueOf(emergencySms.getId()) } );
    }

    public void deleteEmergencySms(long id){
        SQLiteDatabase db = this.getWritableDatabase();

        db.delete(TABLE_EMERGENCY_SMS, KEY_ID + " = ?",
                new String[] {String.valueOf(id)});
    }

    public EmergencySms getEmergencySms(long id){
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.query(TABLE_EMERGENCY_SMS, new String[] { KEY_ID, KEY_PHONE_NUMBER,
                KEY_MESSAGE, KEY_CREATED_AT }, KEY_ID + " = ?", new String[] { String.valueOf(id) },
                null, null, null, null);

        if(cursor != null)
            cursor.moveToFirst();

        EmergencySms emergencySms = new EmergencySms(Integer.parseInt(cursor.getString(0)),
                cursor.getString(1), cursor.getString(2), cursor.getString(3));

        closeCursor(cursor);

        return emergencySms;
    }

    public List<EmergencySms> getAllEmergencySms(){
        List<EmergencySms> emergencySmsList = new ArrayList<EmergencySms>();
        String selectQuery = "SELECT * FROM " + TABLE_EMERGENCY_SMS;
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        if(cursor.moveToFirst()){
            do{
                EmergencySms emergencySms = new EmergencySms();
                emergencySms.setId(cursor.getInt(0));
                emergencySms.setPhoneNumber(cursor.getString(1));
                emergencySms.setMessage(cursor.getString(2));

                emergencySmsList.add(emergencySms);
            } while (cursor.moveToNext());
        }

        closeCursor(cursor);

        return emergencySmsList;
    }

    public Owner getOwnerDetails(){
        Owner owner = new Owner();
        String selectQuery = "SELECT * FROM " + TABLE_OWNER;
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        if(cursor.moveToFirst()){
            owner.setName(cursor.getString(1));
            owner.setPhoneNumber(cursor.getString(2));
            owner.setMessage(cursor.getString(3));
        }

        closeCursor(cursor);

        return owner;
    }

    public int updateOwnersDetails(Owner owner){
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues contentValues = new ContentValues();
        contentValues.put(KEY_OWNERS_NAME, owner.getName());
        contentValues.put(KEY_OWNERS_EMERGENCY_PHONE, owner.getPhoneNumber());
        contentValues.put(KEY_OWNERS_MESSAGE, owner.getMessage());

        return db.update(TABLE_OWNER, contentValues, KEY_ID + "= ?",
                new String[] { "1" });
    }

    public void deleteCarOwnersDetails(long id){
        SQLiteDatabase db = this.getWritableDatabase();

        db.delete(TABLE_OWNER, KEY_ID + " = ?",
                new String[] {String.valueOf(id)});
    }

    public int updateCarDetails(CarDetails carDetails){
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues contentValues = new ContentValues();
        contentValues.put(KEY_CAR_MANUFACTURER, carDetails.getManufacturer());
        contentValues.put(KEY_CAR_MANUFACTURING_YEAR, carDetails.getManufacturingYear());
        contentValues.put(KEY_CAR_MODEL, carDetails.getModel());
        contentValues.put(KEY_ENGINE_DISPLACEMENT, carDetails.getEngineDisplacement());
        contentValues.put(KEY_ENGINE_POWER, carDetails.getEnginePower());
        contentValues.put(KEY_FUEL_TYPE, carDetails.getFuelType());
        contentValues.put(KEY_TRANSMISSION_TYPE, carDetails.getTransmissionType());
        contentValues.put(KEY_MAX_SPEED, carDetails.getMaxSpeed());
        contentValues.put(KEY_MAX_RPM, carDetails.getMaxRpm());

        return db.update(TABLE_CAR_DETAILS, contentValues, KEY_ID + " = ?", new String[] { String.valueOf(carDetails.getId()) });
    }

    public CarDetails getCarDetails(){
        CarDetails carDetails = new CarDetails();
        String selectQuery = "SELECT * FROM " + TABLE_CAR_DETAILS;
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        if(cursor.moveToFirst()){
            carDetails.setId(cursor.getInt(0));
            carDetails.setManufacturer(cursor.getString(1));
            carDetails.setManufacturingYear(cursor.getInt(2));
            carDetails.setModel(cursor.getString(3));
            carDetails.setEngineDisplacement(cursor.getInt(4));
            carDetails.setEnginePower(cursor.getInt(5));
            carDetails.setFuelType(cursor.getString(6));
            carDetails.setTransmissionType(cursor.getString(7));
            carDetails.setMaxSpeed(cursor.getInt(8));
            carDetails.setMaxRpm(cursor.getInt(9));
        }

        closeCursor(cursor);

        return carDetails;
    }

    public long createServiceHistory(ServiceHistory serviceHistory){
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues contentValues = new ContentValues();

        contentValues.put(KEY_ENGINE_OIL, serviceHistory.getEngineOil());
        contentValues.put(KEY_OIL_FILTER, serviceHistory.getOilFilter());
        contentValues.put(KEY_AIR_FILTER, serviceHistory.getAirFilter());
        contentValues.put(KEY_FUEL_FILTER, serviceHistory.getFuelFilter());
        contentValues.put(KEY_CABIN_FILTER, serviceHistory.getCabinFilter());
        contentValues.put(KEY_BRAKE_FLUID, serviceHistory.getBrakeFluid());
        contentValues.put(KEY_POWER_STEERING_FLUID, serviceHistory.getPowerSteeringFluid());
        contentValues.put(KEY_TRANSMISSION_FLUID, serviceHistory.getTransmissionFluid());
        contentValues.put(KEY_TIMING_BELT, serviceHistory.getTimingBelt());
        contentValues.put(KEY_TIMING_CHAIN, serviceHistory.getTimingChain());
        contentValues.put(KEY_ODOMETER, serviceHistory.getOdometer());
        contentValues.put(KEY_CREATED_AT, serviceHistory.getCreatedAt());

        return db.insert(TABLE_SERVICE_HISTORY, null, contentValues);
    }

    public List<ServiceHistory> getServiceHistory(){
        List<ServiceHistory> serviceHistories = new ArrayList<>();
        SQLiteDatabase db = this.getWritableDatabase();
        String selectQuery = "SELECT * FROM " + TABLE_SERVICE_HISTORY;
        Cursor cursor = db.rawQuery(selectQuery, null);

        if(cursor.moveToFirst()){
            do {
                ServiceHistory serviceHistory = new ServiceHistory();

                serviceHistory.setId(cursor.getInt(0));
                serviceHistory.setOdometer(cursor.getInt(11));
                serviceHistory.setCreatedAt(cursor.getString(12));

                serviceHistories.add(serviceHistory);
            } while (cursor.moveToNext());
        }
        closeCursor(cursor);

        return serviceHistories;
    }

    public ServiceHistory getServiceHistory(long id){
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.query(TABLE_SERVICE_HISTORY, new String[] { "*" }, KEY_ID + " = ?", new String[] { String.valueOf(id) },
                null, null, null, null);

        if(cursor != null)
            cursor.moveToFirst();

        ServiceHistory serviceHistory = new ServiceHistory();
        serviceHistory.setId(cursor.getInt(0));
        serviceHistory.setEngineOil(cursor.getInt(1));
        serviceHistory.setOilFilter(cursor.getInt(2));
        serviceHistory.setAirFilter(cursor.getInt(3));
        serviceHistory.setCabinFilter(cursor.getInt(4));
        serviceHistory.setFuelFilter(cursor.getInt(5));
        serviceHistory.setBrakeFluid(cursor.getInt(6));
        serviceHistory.setPowerSteeringFluid(cursor.getInt(7));
        serviceHistory.setTransmissionFluid(cursor.getInt(8));
        serviceHistory.setTimingBelt(cursor.getInt(9));
        serviceHistory.setTimingChain(cursor.getInt(10));
        serviceHistory.setOdometer(cursor.getInt(11));
        serviceHistory.setCreatedAt(cursor.getString(12));

        closeCursor(cursor);

        return serviceHistory;
    }

    public void deleteServiceHistory(long id){
        SQLiteDatabase db = this.getWritableDatabase();

        db.delete(TABLE_SERVICE_HISTORY, "WHERE " + KEY_ID + " = ?",
                new String[] {String.valueOf(id)});
    }

    public TroubleCode getTroubleCode(String code){
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.query(TABLE_TROUBLE_CODES, new String[] { KEY_DESCRIPTION }, KEY_CODE + " = ?", new String[] { code },
                null, null, null, null);

        if(cursor.moveToFirst()){
            cursor.moveToFirst();
            return new TroubleCode(code, cursor.getString(0));
        }

        return new TroubleCode(code, "Code not in database.");
    }

    public void closeCursor(Cursor cursor){
        if(!cursor.isClosed()){
            cursor.close();
        }
    }
}
