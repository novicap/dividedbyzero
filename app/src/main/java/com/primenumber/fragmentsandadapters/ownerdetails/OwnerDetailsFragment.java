package com.primenumber.fragmentsandadapters.ownerdetails;

import android.app.Activity;
import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.primenumber.data.Database;
import com.primenumber.data.models.Owner;
import com.primenumber.dividedbyzero.R;

/**
 * Created by novica on 3/17/18.
 */

public class OwnerDetailsFragment extends Fragment implements View.OnClickListener {

    private View view;
    private TextView name, phone, message;
    private Button edit;
    private Database database = Database.getInstance(getActivity());
    private EditOwnerDetailsListener listener;
    private Owner owner;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_owner_details_layout, null, false);
        name = view.findViewById(R.id.name);
        phone = view.findViewById(R.id.phone_number);
        message = view.findViewById(R.id.message);
        edit = view.findViewById(R.id.edit_details);
        edit.setOnClickListener(this);

        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        owner = database.getOwnerDetails();
        name.setText(owner.getName());
        phone.setText(owner.getPhoneNumber());
        message.setText(owner.getMessage());
    }

    @Override
    public void onAttach(Activity context) {
        super.onAttach(context);
        try {
            listener = (EditOwnerDetailsListener) context;
        } catch (ClassCastException e) {
            throw new ClassCastException(context.toString() + " must implement EditOwnerDetailsListener");
        }
    }

    @Override
    public void onClick(View view) {
        if(view.getId() == R.id.edit_details){
            listener.editOwner();
        }
    }

    public interface EditOwnerDetailsListener{
        void editOwner();
    }
}
