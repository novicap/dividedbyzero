package com.primenumber.fragmentsandadapters.ownerdetails;

import android.app.Activity;
import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.primenumber.data.Database;
import com.primenumber.data.models.Owner;
import com.primenumber.dividedbyzero.R;

/**
 * Created by novica on 3/17/18.
 */

public class OwnerDetailsEditFragment extends Fragment implements View.OnClickListener{

    private View view;
    private EditText name, phone, message;
    private Button save;
    private Database database = Database.getInstance(getActivity());
    private Owner owner;
    private OwnerDetailsEditedListener listener;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        view  = inflater.inflate(R.layout.fragment_owner_details_edit_layout, container, false);
        name = view.findViewById(R.id.name);
        phone = view.findViewById(R.id.phone_number);
        message = view.findViewById(R.id.message);
        save = view.findViewById(R.id.save_details);
        owner = new Owner();
        save.setOnClickListener(this);
        return view;
    }


    @Override
    public void onClick(View view) {
        if(view.getId() == R.id.save_details){
            if(name.getText().toString().length() != 0 &&
                    phone.getText().toString().length() > 0 &&
                    phone.getText().toString().length() < 20 &&
                    message.getText().toString().length() <= 60) {
                owner.setName(name.getText().toString());
                owner.setPhoneNumber(phone.getText().toString());
                owner.setMessage(message.getText().toString());
                database.updateOwnersDetails(owner);
                listener.edited();
            } else {
                Toast.makeText(getActivity(), "Check name, phone number and message.\n" +
                        "Name should be filled.\n" +
                        "Message should be less than 60 characters long.\n" +
                        "Phone number must be stored and should be less than 20 numbers long.", Toast.LENGTH_LONG).show();
            }
        }
    }

    @Override
    public void onResume() {
        owner = database.getOwnerDetails();
        name.setText(owner.getName());
        phone.setText(owner.getPhoneNumber());
        message.setText(owner.getMessage());
        super.onResume();
    }

    @Override
    public void onAttach(Activity context) {
        super.onAttach(context);
        try {
            listener = (OwnerDetailsEditedListener) context;
        } catch (ClassCastException e) {
            throw new ClassCastException(context.toString() + " must implement OwnerDetailsEditedListener");
        }
    }

    public interface OwnerDetailsEditedListener{
        void edited();
    }
}
