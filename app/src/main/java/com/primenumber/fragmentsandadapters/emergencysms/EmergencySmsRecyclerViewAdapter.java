package com.primenumber.fragmentsandadapters.emergencysms;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.primenumber.data.models.EmergencySms;
import com.primenumber.dividedbyzero.R;

import java.util.List;

public class EmergencySmsRecyclerViewAdapter extends RecyclerView.Adapter<EmergencySmsRecyclerViewAdapter.EmergencySmsViewHolder>{

    private Context context;
    private List<EmergencySms> emergencySms;
    private EmergencySmsClickListener emergencySmsClickListener;

    public EmergencySmsRecyclerViewAdapter(Context context, List<EmergencySms> emergencySms, EmergencySmsClickListener emergencySmsClickListener){
        this.context = context;
        this.emergencySms = emergencySms;
        this.emergencySmsClickListener = emergencySmsClickListener;
    }

    @Override
    public EmergencySmsViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view;
        view = LayoutInflater.from(context).inflate(R.layout.recycler_view_adapter_emergency_sms, parent, false);
        EmergencySmsViewHolder viewHolder = new EmergencySmsViewHolder(view, emergencySmsClickListener);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(EmergencySmsViewHolder holder, int position) {
        holder.number.setText(emergencySms.get(position).getPhoneNumber());
        holder.message.setText(emergencySms.get(position).getMessage());
    }

    @Override
    public int getItemCount() {
        return emergencySms.size();
    }

    public interface EmergencySmsClickListener {
        void onItemClick(View view, int position);
    }

    public class EmergencySmsViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
        private TextView number, message;
        private EmergencySmsClickListener listener;

        public EmergencySmsViewHolder(View itemView, EmergencySmsClickListener listener){
            super(itemView);
            itemView.setOnClickListener(this);
            this.listener = listener;
            number = itemView.findViewById(R.id.recipient);
            message = itemView.findViewById(R.id.message);
        }

        @Override
        public void onClick(View view) {
            listener.onItemClick(view, getAdapterPosition());
        }
    }
}
