package com.primenumber.fragmentsandadapters.emergencysms;

import android.app.Activity;
import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.primenumber.data.Database;
import com.primenumber.data.models.EmergencySms;
import com.primenumber.dividedbyzero.R;

import java.text.DateFormat;
import java.util.Date;

public class EmergencySmsCreateFragment extends Fragment implements View.OnClickListener{

    private View view;
    private EditText phoneNumber, message;
    private Button saveSms;
    private Database database;
    private EmergencySms emergencySms;
    private EmergencySmsCreatedListener listener;

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState)
    {
        view = inflater.inflate(R.layout.fragment_emergency_sms_create_layout, container, false);
        saveSms = view.findViewById(R.id.saveSms);
        phoneNumber = view.findViewById(R.id.phoneNumber);
        message = view.findViewById(R.id.message);
        saveSms.setOnClickListener(this);
        return view;
    }

    @Override
    public void onAttach(Activity context) {
        super.onAttach(context);
        database = Database.getInstance(context);
        emergencySms = new EmergencySms();
        try {
            listener = (EmergencySmsCreatedListener) context;
        } catch (ClassCastException e) {
            throw new ClassCastException(context.toString() + " must implement OnArticleSelectedListener");
        }
    }


    @Override
    public void onClick(View view) {
        if(view.getId() == R.id.saveSms){
            if(message.getText().toString().length() <= 60 &&
                    phoneNumber.getText().toString().length() > 0 &&
                    phoneNumber.getText().toString().length() < 20){
                emergencySms.setPhoneNumber(phoneNumber.getText().toString());
                emergencySms.setMessage(message.getText().toString());
                emergencySms.setCreatedAt(DateFormat.getDateTimeInstance().format(new Date()));
                database.createEmergencySms(emergencySms);
                listener.newEntryStored();
                phoneNumber.setText("");
                message.setText("");
            }
            else{
                Toast.makeText(getActivity(), "Check phone number and message.\n" +
                        "Message should be less than 60 characters long.\n" +
                        "Phone number must be stored and should be less than 20 numbers long.", Toast.LENGTH_LONG).show();
            }
        }
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    public interface EmergencySmsCreatedListener{
        void newEntryStored();
    }
}
