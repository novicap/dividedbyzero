package com.primenumber.fragmentsandadapters.emergencysms;

import android.app.Activity;
import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.primenumber.data.Database;
import com.primenumber.data.models.EmergencySms;
import com.primenumber.dividedbyzero.R;

import java.util.ArrayList;
import java.util.List;

public class EmergencySmsListFragment extends Fragment implements View.OnClickListener{

    private Database database;
    private Button createNew;
    private View view;
    private RecyclerView emergencySmsRecyclerView;
    private List<EmergencySms> emergencySmsList;
    private EmergencySmsRecyclerViewAdapter emergencySmsRecyclerViewAdapter;
    private EmergencySmsRecyclerViewAdapter.EmergencySmsClickListener smsListener;
    OnItemSelectedListener listener;

    public EmergencySmsListFragment() {}

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_emergency_sms_list_layout, container, false);
        emergencySmsRecyclerView = view.findViewById(R.id.emergency_sms_recycler_view);
        emergencySmsList = database.getAllEmergencySms();
        smsListener = new EmergencySmsRecyclerViewAdapter.EmergencySmsClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                listener.onItemSelected(emergencySmsList.get(position).getId());
            }
        };
        emergencySmsRecyclerViewAdapter = new EmergencySmsRecyclerViewAdapter(getActivity(),
                emergencySmsList, smsListener);
        emergencySmsRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        emergencySmsRecyclerView.addItemDecoration(new DividerItemDecoration(getActivity(),LinearLayoutManager.VERTICAL));
        emergencySmsRecyclerView.setAdapter(emergencySmsRecyclerViewAdapter);
        createNew = view.findViewById(R.id.create_new);
        createNew.setOnClickListener(this);
        return view;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        database = Database.getInstance(activity);
        emergencySmsList = new ArrayList<>();
        try {
            listener = (OnItemSelectedListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString() + " must implement OnArticleSelectedListener");
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        emergencySmsList = database.getAllEmergencySms();
        emergencySmsRecyclerViewAdapter.notifyDataSetChanged();
    }

    @Override
    public void onClick(View view) {
        if(view.getId() == R.id.create_new){
            listener.createNew();
        }
    }

    public interface OnItemSelectedListener{
        void createNew();
        void onItemSelected(int id);
    }
}
