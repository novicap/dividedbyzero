package com.primenumber.fragmentsandadapters.emergencysms;

import android.app.Activity;
import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.primenumber.data.Database;
import com.primenumber.data.models.EmergencySms;
import com.primenumber.dividedbyzero.R;

public class EmergencySmsEditFragment extends Fragment implements View.OnClickListener{

    private Database database;
    private EmergencySms emergencySms;
    private EditText recipient, message;
    private Button edit, delete;
    private View view;
    private ItemSelectedListener listener;

    public EmergencySmsEditFragment(){
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_emergency_sms_edit_layout, container, false);
        recipient = view.findViewById(R.id.edit_phone_number);
        message = view.findViewById(R.id.edit_message);
        edit = view.findViewById(R.id.edit);
        edit.setOnClickListener(this);
        delete = view.findViewById(R.id.delete);
        delete.setOnClickListener(this);
        emergencySms = new EmergencySms();
        listener.selectItem(getArguments().getInt("id"));
        return view;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        database = Database.getInstance(activity);
        try {
            listener = (ItemSelectedListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString() + " must implement OnArticleSelectedListener");
        }
    }

    public void getEmergencySms(int id){
        emergencySms = database.getEmergencySms(id);
    }


    //had to move it to onResume due to android issue
    @Override
    public void onResume() {
        super.onResume();
        recipient.setText(emergencySms.getPhoneNumber());
        message.setText(emergencySms.getMessage());
    }

    @Override
    public void onClick(View view) {
        if(view.getId() == R.id.delete){
            database.deleteEmergencySms(emergencySms.getId());
            listener.itemDeleted();
        }
        else if(view.getId() == R.id.edit){
            if(message.getText().toString().length() <= 60 &&
                    recipient.getText().toString().length() > 0 &&
                    recipient.getText().toString().length() < 20) {
                emergencySms.setPhoneNumber(recipient.getText().toString());
                emergencySms.setMessage(message.getText().toString());
                database.updateEmergencySms(emergencySms);
                listener.itemUpdated();
            }
            else{
                Toast.makeText(getActivity(), "Check phone number and message.\n" +
                        "Message should be less than 60 characters long.\n" +
                        "Phone number must be stored and should be less than 20 numbers long.", Toast.LENGTH_LONG).show();
            }
        }
    }

    public interface ItemSelectedListener{
        void selectItem(int id);
        void itemDeleted();
        void itemUpdated();
    }
}
