package com.primenumber.fragmentsandadapters.diagnostics;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.primenumber.data.models.TroubleCode;
import com.primenumber.dividedbyzero.R;

import java.util.List;

public class TroubleCodeRecyclerViewAdapter extends RecyclerView.Adapter<TroubleCodeRecyclerViewAdapter.TroubleCodeViewHolder>{

    private Context context;
    private List<TroubleCode> sensorDataList;

    public TroubleCodeRecyclerViewAdapter(Context context, List<TroubleCode> sensorDataList){
        this.context = context;
        this.sensorDataList = sensorDataList;
    }

    @Override
    public TroubleCodeViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view;
        view = LayoutInflater.from(context).inflate(R.layout.recycler_view_adapter_trouble_code, parent, false);
        TroubleCodeViewHolder viewHolder = new TroubleCodeViewHolder(view);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(TroubleCodeViewHolder holder, int position) {
        holder.code.setText(sensorDataList.get(position).getCode());
        holder.description.setText(sensorDataList.get(position).getDescription());
        holder.icon.setImageResource(R.drawable.spanner);
    }

    @Override
    public int getItemCount() {
        return sensorDataList.size();
    }


    public class TroubleCodeViewHolder extends RecyclerView.ViewHolder{

        private TextView code, description;
        private ImageView icon;

        public TroubleCodeViewHolder(View itemView) {
            super(itemView);
            code = itemView.findViewById(R.id.title);
            description = itemView.findViewById(R.id.value);
            icon = itemView.findViewById(R.id.icon);
        }
    }
}
