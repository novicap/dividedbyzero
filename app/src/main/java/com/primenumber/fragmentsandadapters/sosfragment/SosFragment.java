package com.primenumber.fragmentsandadapters.sosfragment;

import android.app.Activity;
import android.app.Fragment;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.primenumber.dividedbyzero.R;
import com.primenumber.services.EmergencySmsService;

/**
 * Created by novica on 3/21/18.
 */

public class SosFragment extends Fragment implements View.OnClickListener {

    private View view;
    private Button sos, stopSos;
    private SosClickListener listener;
    private BroadcastReceiver receiver;
    private boolean sosActive;

    public SosFragment(){}
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_sos, null, false);
        sos = view.findViewById(R.id.sos_call);
        sos.setOnClickListener(this);
        stopSos = view.findViewById(R.id.stop_sos_call);
        stopSos.setOnClickListener(this);
        if(sosActive){
            sos.setVisibility(View.GONE);
            stopSos.setVisibility(View.VISIBLE);
        } else {
            stopSos.setVisibility(View.GONE);
            sos.setVisibility(View.VISIBLE);
        }
        return view;
    }

    public void setSosActive(boolean sosActive)
    {
        this.sosActive = sosActive;
    }

    @Override
    public void onAttach(Activity context) {
        super.onAttach(context);
        try {
            listener = (SosClickListener) context;
        } catch (ClassCastException e) {
            throw new ClassCastException(context.toString() + " must implement SosClickListener");
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        if(receiver == null){
            receiver = new BroadcastReceiver() {
                @Override
                public void onReceive(Context context, Intent intent) {
                    if(Boolean.valueOf(String.valueOf(intent.getExtras().get("status")))){
                        sosActive = true;
                        sos.setVisibility(View.GONE);
                        stopSos.setVisibility(View.VISIBLE);
                    } else {
                        sosActive = false;
                        stopSos.setVisibility(View.GONE);
                        sos.setVisibility(View.VISIBLE);
                    }
                }
            };
        }

        getActivity().registerReceiver(receiver, new IntentFilter("sos_status"));
    }

    @Override
    public void onPause() {
        super.onPause();
        try{
            getActivity().unregisterReceiver(receiver);
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onClick(View view) {
        if(view.getId() == R.id.sos_call){
            listener.turnOnSos();
            sos.setVisibility(View.GONE);
            stopSos.setVisibility(View.VISIBLE);
            Intent intent = new Intent(getActivity(), EmergencySmsService.class);
            getActivity().startService(intent);
        } else if(view.getId() == R.id.stop_sos_call) {
            listener.turnOffSos();
            stopSos.setVisibility(View.GONE);
            sos.setVisibility(View.VISIBLE);
            Intent intent = new Intent(getActivity(), EmergencySmsService.class);
            getActivity().stopService(intent);
        }
    }

    public interface SosClickListener{
        void turnOnSos();
        void turnOffSos();
    }
}
