package com.primenumber.fragmentsandadapters.vehicleservice;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Fragment;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.net.Uri;
import android.os.Bundle;
import android.provider.CalendarContract;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;

import com.primenumber.data.Database;
import com.primenumber.data.models.ServiceHistory;
import com.primenumber.dividedbyzero.R;

import java.text.DateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

public class VehicleServiceCreateFragment extends Fragment implements View.OnClickListener{
    private View view;
    private CheckBox engineOilChange, oilFilterChange, airFilterChange, cabinFilterChange,
        fuelFilterChange, brakeFluidChange, powerSteeringFluidChange, transmissionFluidChange,
        timingBeltChange, timingChainChange;
    private EditText odometer;
    private Button save;
    private ServiceHistory serviceHistory;
    private Database database;
    private Listener listener;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_vehicle_service_create_layout, container, false);
        serviceHistory = new ServiceHistory();
        engineOilChange = view.findViewById(R.id.engine_oil_change);
        oilFilterChange = view.findViewById(R.id.oil_filter_change);
        fuelFilterChange = view.findViewById(R.id.fuel_filter_change);
        airFilterChange = view.findViewById(R.id.air_filter_change);
        cabinFilterChange = view.findViewById(R.id.cabin_filter_change);
        brakeFluidChange = view.findViewById(R.id.brake_fluid_change);
        powerSteeringFluidChange = view.findViewById(R.id.power_steering_fluid_change);
        transmissionFluidChange = view.findViewById(R.id.transmission_fluid_change);
        timingBeltChange = view.findViewById(R.id.timing_belt_change);
        timingChainChange = view.findViewById(R.id.timing_chain_change);
        odometer = view.findViewById(R.id.odometer_value);
        save = view.findViewById(R.id.save);
        save.setOnClickListener(this);

        return view;
    }

    @Override
    public void onClick(View v) {
        serviceHistory.setEngineOil(boolToInt(engineOilChange.isChecked()));
        serviceHistory.setOilFilter(boolToInt(oilFilterChange.isChecked()));
        serviceHistory.setAirFilter(boolToInt(airFilterChange.isChecked()));
        serviceHistory.setCabinFilter(boolToInt(cabinFilterChange.isChecked()));
        serviceHistory.setFuelFilter(boolToInt(fuelFilterChange.isChecked()));
        serviceHistory.setBrakeFluid(boolToInt(brakeFluidChange.isChecked()));
        serviceHistory.setPowerSteeringFluid(boolToInt(powerSteeringFluidChange.isChecked()));
        serviceHistory.setTransmissionFluid(boolToInt(transmissionFluidChange.isChecked()));
        serviceHistory.setTimingBelt(boolToInt(timingBeltChange.isChecked()));
        serviceHistory.setTimingChain(boolToInt(timingChainChange.isChecked()));
        serviceHistory.setOdometer(Integer.parseInt(odometer.getText().toString()));
        serviceHistory.setCreatedAt(DateFormat.getDateTimeInstance().format(new Date()));
        database.createServiceHistory(serviceHistory);
        addReminderToCalendar();
        listener.newEntryStored();
    }

    @Override
    public void onAttach(Activity context) {
        super.onAttach(context);
        database = Database.getInstance(context);
        try{
            this.listener = (Listener) context;
        } catch (ClassCastException e) {
            e.printStackTrace();
        }
    }

    @SuppressLint("MissingPermission")
    public void addReminderToCalendar()
    {
        ContentResolver contentResolver = getActivity().getContentResolver();
        ContentValues contentValues = new ContentValues();
        long dtend = getLongAsDate(Calendar.getInstance().get(Calendar.YEAR)+1,
                Calendar.getInstance().get(Calendar.MONTH),
                Calendar.getInstance().get(Calendar.DAY_OF_MONTH));
        contentValues.put(CalendarContract.Events.TITLE, "Vehicle service");
        contentValues.put(CalendarContract.Events.DESCRIPTION, "Vehicle service reminder.");
        contentValues.put(CalendarContract.Events.DTSTART, dtend);
        contentValues.put(CalendarContract.Events.DTEND, dtend);
        contentValues.put(CalendarContract.Events.CALENDAR_ID, 1);
        contentValues.put(CalendarContract.Events.EVENT_TIMEZONE, Calendar.getInstance().getTimeZone().getID());
        contentValues.put(CalendarContract.Events.HAS_ALARM, true);
        Uri uri = contentResolver.insert(CalendarContract.Events.CONTENT_URI, contentValues);

        long eventID = Long.parseLong(uri.getLastPathSegment());
        ContentValues firstReminder = new ContentValues();
        firstReminder.put(CalendarContract.Reminders.EVENT_ID, eventID);
        firstReminder.put(CalendarContract.Reminders.METHOD, CalendarContract.Reminders.METHOD_ALERT);
        firstReminder.put(CalendarContract.Reminders.MINUTES, 1440);
        contentResolver.insert(CalendarContract.Reminders.CONTENT_URI, firstReminder);
        ContentValues secondReminder = new ContentValues();
        secondReminder.put(CalendarContract.Reminders.EVENT_ID, eventID);
        secondReminder.put(CalendarContract.Reminders.METHOD, CalendarContract.Reminders.METHOD_ALERT);
        secondReminder.put(CalendarContract.Reminders.MINUTES, 60);
        contentResolver.insert(CalendarContract.Reminders.CONTENT_URI, secondReminder);
    }

    public long getLongAsDate(int year, int month, int date) {
        Calendar calendar = new GregorianCalendar();
        if(month != 2 && date != 29) {
            calendar.set(Calendar.DAY_OF_MONTH, date);
            calendar.set(Calendar.MONTH, month);
            calendar.set(Calendar.YEAR, year);
        } else {
            calendar.set(Calendar.DAY_OF_MONTH, date-1);
            calendar.set(Calendar.MONTH, month);
            calendar.set(Calendar.YEAR, year);
        }

        return calendar.getTimeInMillis();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        database = null;
    }

    private int boolToInt(boolean bool){
        return bool ? 1 : 0;
    }

    public interface Listener{
        void newEntryStored();
    }
}
