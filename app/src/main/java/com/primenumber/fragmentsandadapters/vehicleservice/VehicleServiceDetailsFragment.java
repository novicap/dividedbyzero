package com.primenumber.fragmentsandadapters.vehicleservice;

import android.app.Activity;
import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.TextView;

import com.primenumber.data.Database;
import com.primenumber.data.models.ServiceHistory;
import com.primenumber.dividedbyzero.R;

public class VehicleServiceDetailsFragment extends Fragment {
    private View view;
    private CheckBox engineOilChange, oilFilterChange, airFilterChange, cabinFilterChange,
            fuelFilterChange, brakeFluidChange, powerSteeringFluidChange, transmissionFluidChange,
            timingBeltChange, timingChainChange;
    private TextView date, odometer;
    private ItemSelectedInterface listener;
    private Database database;
    private ServiceHistory serviceHistory;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_vehicle_service_details_layout, container, false);
        engineOilChange = view.findViewById(R.id.engine_oil_change);
        oilFilterChange = view.findViewById(R.id.oil_filter_change);
        fuelFilterChange = view.findViewById(R.id.fuel_filter_change);
        airFilterChange = view.findViewById(R.id.air_filter_change);
        cabinFilterChange = view.findViewById(R.id.cabin_filter_change);
        brakeFluidChange = view.findViewById(R.id.brake_fluid_change);
        powerSteeringFluidChange = view.findViewById(R.id.power_steering_fluid_change);
        transmissionFluidChange = view.findViewById(R.id.transmission_fluid_change);
        timingBeltChange = view.findViewById(R.id.timing_belt_change);
        timingChainChange = view.findViewById(R.id.timing_chain_change);
        odometer = view.findViewById(R.id.odometer_value);
        date = view.findViewById(R.id.date);
        listener.itemSelected(getArguments().getInt("id"));

        return view;
    }

    @Override
    public void onAttach(Activity context) {
        super.onAttach(context);
        database = Database.getInstance(context);
        try{
            listener = (ItemSelectedInterface) context;
        } catch (ClassCastException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        checkCheckbox(engineOilChange, serviceHistory.getEngineOil());
        checkCheckbox(oilFilterChange, serviceHistory.getOilFilter());
        checkCheckbox(airFilterChange, serviceHistory.getAirFilter());
        checkCheckbox(fuelFilterChange, serviceHistory.getFuelFilter());
        checkCheckbox(cabinFilterChange, serviceHistory.getCabinFilter());
        checkCheckbox(brakeFluidChange, serviceHistory.getBrakeFluid());
        checkCheckbox(powerSteeringFluidChange, serviceHistory.getPowerSteeringFluid());
        checkCheckbox(transmissionFluidChange, serviceHistory.getTransmissionFluid());
        checkCheckbox(timingBeltChange, serviceHistory.getTimingBelt());
        checkCheckbox(timingChainChange, serviceHistory.getTimingChain());
        odometer.setText(String.valueOf(serviceHistory.getOdometer()));
        date.setText(serviceHistory.getCreatedAt());
    }

    public void getServiceHistory(int id){
        serviceHistory = database.getServiceHistory(id);
    }

    public void checkCheckbox(CheckBox checkBox, int value){
        if(value == 1){
            checkBox.setChecked(true);
            checkBox.setClickable(false);
        }
        else {
            checkBox.setChecked(false);
            checkBox.setClickable(false);
        }
    }

    public interface ItemSelectedInterface{
        void itemSelected(int id);
    }
}
