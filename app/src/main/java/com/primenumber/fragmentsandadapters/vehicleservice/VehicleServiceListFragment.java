package com.primenumber.fragmentsandadapters.vehicleservice;

import android.app.Activity;
import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.primenumber.data.Database;
import com.primenumber.data.models.ServiceHistory;
import com.primenumber.dividedbyzero.R;

import java.util.List;

/**
 * Created by novica on 2/28/18.
 */

public class VehicleServiceListFragment extends Fragment implements View.OnClickListener{

    private View view;
    private Database database;
    private Button createNew;
    private RecyclerView recyclerView;
    private List<ServiceHistory> serviceHistoryList;
    private OnItemSelectedListener listener;
    private VehicleServiceRecyclerViewAdapter.VehicleServiceSelectedListener vehicleListener;
    private VehicleServiceRecyclerViewAdapter vehicleServiceRecyclerViewAdapter;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_vehicle_service_list_layout, container, false);
        recyclerView = view.findViewById(R.id.service_list_recycler_view);
        serviceHistoryList = database.getServiceHistory();
        vehicleListener = new VehicleServiceRecyclerViewAdapter.VehicleServiceSelectedListener() {
            @Override
            public void itemSelected(View view, int position) {
                listener.serviceSelected(serviceHistoryList.get(position).getId());
            }
        };
        vehicleServiceRecyclerViewAdapter = new VehicleServiceRecyclerViewAdapter(getActivity(),
                serviceHistoryList, vehicleListener);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        recyclerView.addItemDecoration(new DividerItemDecoration(getActivity(),LinearLayoutManager.VERTICAL));
        recyclerView.setAdapter(vehicleServiceRecyclerViewAdapter);

        createNew = view.findViewById(R.id.create_new);
        createNew.setOnClickListener(this);
        return view;
    }

    @Override
    public void onAttach(Activity context) {
        super.onAttach(context);
        database = Database.getInstance(context);

        try{
            listener = (OnItemSelectedListener) context;
        } catch (ClassCastException e){
            e.printStackTrace();
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        database = null;
    }

    @Override
    public void onClick(View view) {
        if(view.getId() == R.id.create_new){
            listener.createNew();
        }
    }

    public interface OnItemSelectedListener{
        void createNew();
        void serviceSelected(int id);
    }
}
