package com.primenumber.fragmentsandadapters.vehicleservice;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.primenumber.data.models.ServiceHistory;
import com.primenumber.dividedbyzero.R;

import java.util.List;

/**
 * Created by novica on 2/28/18.
 */

public class VehicleServiceRecyclerViewAdapter extends RecyclerView.Adapter<VehicleServiceRecyclerViewAdapter.VehicleServiceViewHolder> {

    private Context context;
    private List<ServiceHistory> serviceHistoryList;
    private VehicleServiceSelectedListener listener;

    public VehicleServiceRecyclerViewAdapter(Context context, List<ServiceHistory> serviceHistoryList,
                                             VehicleServiceSelectedListener listener){
        this.context = context;
        this.serviceHistoryList = serviceHistoryList;
        this.listener = listener;
    }

    @Override
    public VehicleServiceViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view;
        view = LayoutInflater.from(context).inflate(R.layout.recycler_view_adapter_vehicle_details, parent, false);
        return new VehicleServiceViewHolder(view, listener);
    }

    @Override
    public void onBindViewHolder(VehicleServiceViewHolder holder, int position) {
        holder.title.setText(serviceHistoryList.get(position).getCreatedAt());
        holder.value.setText(serviceHistoryList.get(position).getOdometer() + " km");
        holder.icon.setImageResource(R.drawable.spanner);
    }

    @Override
    public int getItemCount() {
        return serviceHistoryList.size();
    }

    public interface VehicleServiceSelectedListener{
        void itemSelected(View view, int position);
    }

    public class VehicleServiceViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{

        private TextView title, value;
        private ImageView icon;
        private VehicleServiceSelectedListener listener;

        public VehicleServiceViewHolder(View itemView, VehicleServiceSelectedListener listener) {
            super(itemView);
            itemView.setOnClickListener(this);
            this.listener = listener;
            title = itemView.findViewById(R.id.title);
            value = itemView.findViewById(R.id.value);
            icon = itemView.findViewById(R.id.icon);
        }

        @Override
        public void onClick(View view) {
            listener.itemSelected(view, getAdapterPosition());
        }
    }
}
