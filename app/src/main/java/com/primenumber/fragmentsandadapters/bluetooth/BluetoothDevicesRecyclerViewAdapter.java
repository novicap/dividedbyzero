package com.primenumber.fragmentsandadapters.bluetooth;

import android.bluetooth.BluetoothDevice;
import android.content.Context;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.primenumber.dividedbyzero.R;

import java.util.List;


/**
 * Created by novica on 2/26/18.
 */

public class BluetoothDevicesRecyclerViewAdapter extends RecyclerView.Adapter<BluetoothDevicesRecyclerViewAdapter.BluetoothDeviceViewHolder> {

    private List<BluetoothDevice> bluetoothDeviceList;
    private Context context;
    private BluetoothDeviceClickListener listener;
    private int selected = -1;

    public BluetoothDevicesRecyclerViewAdapter(Context context, List<BluetoothDevice> bluetoothDeviceList,
                                               BluetoothDeviceClickListener listener){
        this.context = context;
        this.bluetoothDeviceList = bluetoothDeviceList;
        this.listener = listener;
    }

    @Override
    public BluetoothDeviceViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view;
        view = LayoutInflater.from(context).inflate(R.layout.recycler_view_adapter_bluetooth_devices, parent, false);
        BluetoothDeviceViewHolder viewHolder = new BluetoothDeviceViewHolder(view, listener);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(BluetoothDeviceViewHolder holder, int position) {
        final int selection = position;
        holder.icon.setImageResource(R.drawable.bluetooth_icon_2);
        holder.title.setText(bluetoothDeviceList.get(position).getName());
        holder.value.setText(bluetoothDeviceList.get(position).getAddress());

        if(selected == position) {
            holder.itemView.setBackgroundColor(Color.parseColor("#bcbcbc"));
        }
        else {
            holder.itemView.setBackgroundColor(Color.parseColor("#f8f8f8"));
        }
    }

    @Override
    public int getItemCount() {
        return bluetoothDeviceList.size();
    }

    public interface BluetoothDeviceClickListener{
        void onItemClick(View view, int position);
    }

    public class BluetoothDeviceViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{

        private TextView title, value;
        private ImageView icon;
        private BluetoothDeviceClickListener listener;

        public BluetoothDeviceViewHolder(View itemView, BluetoothDeviceClickListener listener){
            super(itemView);
            this.listener = listener;
            this.title = itemView.findViewById(R.id.title);
            this.value = itemView.findViewById(R.id.value);
            this.icon = itemView.findViewById(R.id.icon);
            itemView.setOnClickListener(this);
        }


        @Override
        public void onClick(View view) {
            listener.onItemClick(view, getAdapterPosition());
            selected = getAdapterPosition();
            notifyDataSetChanged();
        }
    }
}
