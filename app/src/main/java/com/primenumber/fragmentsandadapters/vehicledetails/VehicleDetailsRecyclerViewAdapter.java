package com.primenumber.fragmentsandadapters.vehicledetails;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.primenumber.data.models.RecyclerViewData;
import com.primenumber.dividedbyzero.R;

import java.util.List;

/**
 * Created by novica on 2/21/18.
 */

public class VehicleDetailsRecyclerViewAdapter extends RecyclerView.Adapter<VehicleDetailsRecyclerViewAdapter.VehicleDetailsViewHolder> {

    private Context context;
    private List<RecyclerViewData> recyclerViewDataList;

    public VehicleDetailsRecyclerViewAdapter(Context context, List<RecyclerViewData> recyclerViewDataList){
        this.context = context;
        this.recyclerViewDataList = recyclerViewDataList;
    }

    @Override
    public VehicleDetailsViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view;
        view = LayoutInflater.from(context).inflate(R.layout.recycler_view_adapter_vehicle_details, parent, false);
        VehicleDetailsViewHolder viewHolder = new VehicleDetailsViewHolder(view);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(VehicleDetailsRecyclerViewAdapter.VehicleDetailsViewHolder holder, int position) {
        holder.title.setText(recyclerViewDataList.get(position).getTitle());
        holder.value.setText(recyclerViewDataList.get(position).getReading());
        holder.icon.setImageResource(recyclerViewDataList.get(position).getIcon());
    }

    @Override
    public int getItemCount() {
        return recyclerViewDataList.size();
    }

    public class VehicleDetailsViewHolder extends RecyclerView.ViewHolder{

        private TextView title, value;
        private ImageView icon;

        public VehicleDetailsViewHolder(View itemView) {
            super(itemView);

            title = itemView.findViewById(R.id.title);
            value = itemView.findViewById(R.id.value);
            icon = itemView.findViewById(R.id.icon);
        }
    }
}
