package com.primenumber.fragmentsandadapters.vehicledetails;

import android.app.Activity;
import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.primenumber.data.Database;
import com.primenumber.data.models.CarDetails;
import com.primenumber.data.models.RecyclerViewData;
import com.primenumber.dividedbyzero.R;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by novica on 2/21/18.
 */

public class VehicleDetailsFragment extends Fragment implements View.OnClickListener{

    private CarDetails carDetails;
    private RecyclerView recyclerView;
    private List<RecyclerViewData> recyclerViewDataList;
    private VehicleDetailsRecyclerViewAdapter recyclerAdapter;
    private Database database;
    private CarDetailsListListener listener;
    private Button button;

    public VehicleDetailsFragment() {
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_vehicle_details_layout, container, false);

        recyclerView = view.findViewById(R.id.vehicle_details_recycler_view);
        recyclerAdapter = new VehicleDetailsRecyclerViewAdapter(getActivity(), recyclerViewDataList);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        recyclerView.addItemDecoration(new DividerItemDecoration(getActivity(),LinearLayoutManager.VERTICAL));
        recyclerView.setAdapter(recyclerAdapter);

        button = view.findViewById(R.id.edit_car_details);
        button.setOnClickListener(this);

        return view;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        database = Database.getInstance(activity);

        try {
            listener = (CarDetailsListListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString() + " must implement OnArticleSelectedListener");
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        carDetails = database.getCarDetails();

        RecyclerViewData manufacturer = new RecyclerViewData(getString(R.string.manufacturer_title),
                carDetails.getManufacturer(), R.drawable.manufacturer_icon);
        RecyclerViewData model = new RecyclerViewData(getString(R.string.model_title),
                carDetails.getModel(), R.drawable.car_details_icon);
        RecyclerViewData manufacturingYear = new RecyclerViewData(getString(R.string.manufacturing_year_title),
                String.valueOf(carDetails.getManufacturingYear()), R.drawable.date_icon);
        RecyclerViewData engineDisplacement = new RecyclerViewData(getString(R.string.engine_displacement_title),
                String.valueOf(carDetails.getEngineDisplacement()), R.drawable.engine);
        RecyclerViewData enginePower = new RecyclerViewData(getString(R.string.engine_power_title),
                String.valueOf(carDetails.getEnginePower()), R.drawable.engine_power_icon);
        RecyclerViewData fuelType = new RecyclerViewData(getString(R.string.fuel_type_title),
                carDetails.getFuelType(), R.drawable.fuel_type_icon);
        RecyclerViewData transmissionType = new RecyclerViewData(getString(R.string.transmission_type_title),
                carDetails.getTransmissionType(), R.drawable.gear);
        RecyclerViewData maxSpeed = new RecyclerViewData(getString(R.string.max_speed_title),
                String.valueOf(carDetails.getMaxSpeed()), R.drawable.speedometer);
        RecyclerViewData maxRpm = new RecyclerViewData(getString(R.string.max_rpm_title),
                String.valueOf(carDetails.getMaxRpm()), R.drawable.vehicle_speed_icon);

        recyclerViewDataList = new ArrayList<>();

        recyclerViewDataList.add(manufacturer);
        recyclerViewDataList.add(model);
        recyclerViewDataList.add(manufacturingYear);
        recyclerViewDataList.add(engineDisplacement);
        recyclerViewDataList.add(enginePower);
        recyclerViewDataList.add(fuelType);
        recyclerViewDataList.add(transmissionType);
        recyclerViewDataList.add(maxSpeed);
        recyclerViewDataList.add(maxRpm);
    }

    @Override
    public void onResume() {
        super.onResume();

        carDetails = database.getCarDetails();

        RecyclerViewData manufacturer = new RecyclerViewData(getString(R.string.manufacturer_title),
                carDetails.getManufacturer(), R.drawable.manufacturer_icon);
        RecyclerViewData model = new RecyclerViewData(getString(R.string.model_title),
                carDetails.getModel(), R.drawable.car_details_icon);
        RecyclerViewData manufacturingYear = new RecyclerViewData(getString(R.string.manufacturing_year_title),
                String.valueOf(carDetails.getManufacturingYear()), R.drawable.date_icon);
        RecyclerViewData engineDisplacement = new RecyclerViewData(getString(R.string.engine_displacement_title),
                String.valueOf(carDetails.getEngineDisplacement()), R.drawable.engine);
        RecyclerViewData enginePower = new RecyclerViewData(getString(R.string.engine_power_title),
                String.valueOf(carDetails.getEnginePower()), R.drawable.engine_power_icon);
        RecyclerViewData fuelType = new RecyclerViewData(getString(R.string.fuel_type_title),
                carDetails.getFuelType(), R.drawable.fuel_type_icon);
        RecyclerViewData transmissionType = new RecyclerViewData(getString(R.string.transmission_type_title),
                carDetails.getTransmissionType(), R.drawable.gear);
        RecyclerViewData maxSpeed = new RecyclerViewData(getString(R.string.max_speed_title),
                String.valueOf(carDetails.getMaxSpeed()), R.drawable.speedometer);
        RecyclerViewData maxRpm = new RecyclerViewData(getString(R.string.max_rpm_title),
                String.valueOf(carDetails.getMaxRpm()), R.drawable.vehicle_speed_icon);

        recyclerViewDataList.clear();

        recyclerViewDataList.add(manufacturer);
        recyclerViewDataList.add(model);
        recyclerViewDataList.add(manufacturingYear);
        recyclerViewDataList.add(engineDisplacement);
        recyclerViewDataList.add(enginePower);
        recyclerViewDataList.add(fuelType);
        recyclerViewDataList.add(transmissionType);
        recyclerViewDataList.add(maxSpeed);
        recyclerViewDataList.add(maxRpm);

        recyclerAdapter.notifyDataSetChanged();
    }

    @Override
    public void onClick(View view) {
        if(view.getId() == R.id.edit_car_details) {
            listener.editCarDetails();
        }
    }

    public interface CarDetailsListListener{
        void editCarDetails();
    }
}
