package com.primenumber.fragmentsandadapters.vehicledetails;

import android.app.Activity;
import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import com.primenumber.data.Database;
import com.primenumber.data.models.CarDetails;
import com.primenumber.dividedbyzero.R;

/**
 * Created by novica on 2/21/18.
 */

public class VehicleDetailsEditFragment extends Fragment
        implements View.OnClickListener{

    private CarDetails carDetails;
    private Database database;
    private EditText manufacturer, manufacturingYear, model, engineDisplacement, enginePower,
        maxSpeed, maxRpm;
    private RadioGroup fuelTypeSelect, transmissionTypeSelect;
    private Button edit;
    private View view;
    private VehicleDetailsListener listener;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_vehicle_details_edit_layout, container, false);
        manufacturer = view.findViewById(R.id.manufacturer);
        manufacturingYear = view.findViewById(R.id.manufacturing_year);
        model = view.findViewById(R.id.model);
        engineDisplacement = view.findViewById(R.id.engine_displacement);
        enginePower = view.findViewById(R.id.engine_power);
        maxSpeed = view.findViewById(R.id.max_speed);
        maxRpm = view.findViewById(R.id.max_rpm);
        fuelTypeSelect = view.findViewById(R.id.ft_group);
        transmissionTypeSelect = view.findViewById(R.id.tt_group);
        edit = view.findViewById(R.id.update_car_details);
        edit.setOnClickListener(this);

        return view;
    }

    @Override
    public void onClick(View view) {
        if(view.getId() == R.id.update_car_details){
            carDetails.setManufacturer(manufacturer.getText().toString());
            carDetails.setManufacturingYear(Integer.parseInt(manufacturingYear.getText().toString()));
            carDetails.setModel(model.getText().toString());
            carDetails.setEngineDisplacement(Integer.parseInt(engineDisplacement.getText().toString()));
            carDetails.setEnginePower(Integer.parseInt(enginePower.getText().toString()));
            carDetails.setFuelType(fuelType());
            carDetails.setTransmissionType(transmissionType());
            carDetails.setMaxSpeed(Integer.parseInt(maxSpeed.getText().toString()));
            carDetails.setMaxRpm(Integer.parseInt(maxRpm.getText().toString()));

            database.updateCarDetails(carDetails);
            listener.vehicleDetailsUpdated();
        }
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        database = Database.getInstance(activity);

        try {
            listener = (VehicleDetailsListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString() + " must implement OnArticleSelectedListener");
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        carDetails = database.getCarDetails();

        manufacturer.setText(carDetails.getManufacturer());
        manufacturingYear.setText(String.valueOf(carDetails.getManufacturingYear()));
        model.setText(carDetails.getModel());
        engineDisplacement.setText(String.valueOf(carDetails.getEngineDisplacement()));
        enginePower.setText(String.valueOf(carDetails.getEnginePower()));
        checkFTRadioButton(carDetails.getFuelType());
        checkTTRadioButton(carDetails.getTransmissionType());
        maxSpeed.setText(String.valueOf(carDetails.getMaxSpeed()));
        maxRpm.setText(String.valueOf(carDetails.getMaxRpm()));
    }

    private void checkFTRadioButton(String fuelType){
        if(fuelType.equals(getString(R.string.ft_gasoline))){
            fuelTypeSelect.check(R.id.gasoline);
        }
        else if(fuelType.equals(getString(R.string.ft_diesel))){
            fuelTypeSelect.check(R.id.diesel);
        }
        else if(fuelType.equals(getString(R.string.ft_lpg))){
            fuelTypeSelect.check(R.id.lpg);
        }
        else if(fuelType.equals(getString(R.string.ft_hybrid))){
            fuelTypeSelect.check(R.id.hybrid);
        }
        else{
            fuelTypeSelect.check(R.id.gasoline);
        }
    }

    private void checkTTRadioButton(String transmissionType){
        if(transmissionType.equals(getString(R.string.tt_manual))){
            transmissionTypeSelect.check(R.id.manual);
        }
        else if(transmissionType.equals(getString(R.string.tt_automatic))){
            transmissionTypeSelect.check(R.id.automated_manual);
        }
        else if(transmissionType.equals(getString(R.string.tt_cvt))){
            transmissionTypeSelect.check(R.id.cvt);
        }
        else if(transmissionType.equals(getString(R.string.tt_automated_manual))){
            transmissionTypeSelect.check(R.id.automated_manual);
        }
        else if(transmissionType.equals(getString(R.string.tt_dual_clutch_automated_manual))){
            transmissionTypeSelect.check(R.id.dual_clutch_automated_manual);
        }
        else{
            transmissionTypeSelect.check(R.id.manual);
        }
    }

    private String fuelType(){
        int fuelTypeSelectId = fuelTypeSelect.getCheckedRadioButtonId();
        if(fuelTypeSelectId == -1){
            return getString(R.string.ft_gasoline);
        }

        RadioButton radioButton = view.findViewById(fuelTypeSelectId);
        return radioButton.getText().toString();
    }

    private String transmissionType(){
        int transmissionTypeSelectId = transmissionTypeSelect.getCheckedRadioButtonId();
        if(transmissionTypeSelectId == -1){
            return getString(R.string.tt_manual);
        }

        RadioButton radioButton = view.findViewById(transmissionTypeSelectId);
        return radioButton.getText().toString();
    }

    public interface VehicleDetailsListener{
        void vehicleDetailsUpdated();
    }
}
