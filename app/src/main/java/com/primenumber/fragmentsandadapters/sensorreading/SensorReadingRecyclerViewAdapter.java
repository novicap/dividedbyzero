package com.primenumber.fragmentsandadapters.sensorreading;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.primenumber.data.models.RecyclerViewData;
import com.primenumber.dividedbyzero.R;

import java.util.List;

/**
 * Created by novica on 2/10/18.
 */

public class SensorReadingRecyclerViewAdapter extends RecyclerView.Adapter<SensorReadingRecyclerViewAdapter.MyViewHolder>{

    Context context;
    List<RecyclerViewData> recyclerViewData;

    public SensorReadingRecyclerViewAdapter(Context context, List<RecyclerViewData> recyclerViewData){
        this.context = context;
        this.recyclerViewData = recyclerViewData;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view;
        view = LayoutInflater.from(context).inflate(R.layout.recycler_view_adapter_sensor_data, parent, false);
        MyViewHolder viewHolder = new MyViewHolder(view);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        holder.sensor.setText(recyclerViewData.get(position).getTitle());
        holder.sensorReading.setText(recyclerViewData.get(position).getReading());
        holder.sensorIcon.setImageResource(recyclerViewData.get(position).getIcon());
    }

    @Override
    public int getItemCount() {
        return recyclerViewData.size();
    }

    public static class MyViewHolder extends RecyclerView.ViewHolder{
        private TextView sensor;
        private TextView sensorReading;
        private ImageView sensorIcon;

        public MyViewHolder(View itemView){
            super(itemView);
            sensor = itemView.findViewById(R.id.sensor_reading_title);
            sensorReading = itemView.findViewById(R.id.sensor_reading);
            sensorIcon = itemView.findViewById(R.id.sensor_image);
        }
    }
}
