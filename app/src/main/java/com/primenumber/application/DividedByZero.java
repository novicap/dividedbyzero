package com.primenumber.application;
import android.app.Application;

import com.primenumber.services.BluetoothOBD2Connection;

/**
 * Created by novica on 3/1/18.
 */

public class DividedByZero extends Application {

    private boolean drivingModeOn = false, sosModeOn = false, cameraView = false;
    private BluetoothOBD2Connection bluetoothConnection;

    public void setDrivingModeOn(boolean drivingModeStarted){
        this.drivingModeOn = drivingModeStarted;
    }

    public boolean getDrivingModeOn(){
        return drivingModeOn;
    }

    public void setBluetoothConnection(BluetoothOBD2Connection bluetoothConnection){
        this.bluetoothConnection = bluetoothConnection;
    }

    public void setSosModeOn(boolean sosMode){
        this.sosModeOn = sosMode;
    }

    public boolean getSosModeOn(){
        return sosModeOn;
    }

    public BluetoothOBD2Connection getBluetoothConnection(){
        return this.bluetoothConnection;
    }

    public void setCameraView(boolean status){
        cameraView = status;
    }

    public boolean getCameraView(){
        return cameraView;
    }
}
