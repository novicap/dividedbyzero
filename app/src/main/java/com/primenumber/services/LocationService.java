package com.primenumber.services;

import android.annotation.SuppressLint;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Looper;
import android.widget.Toast;

import com.primenumber.application.DividedByZero;

public class LocationService extends Service{

    private LocationListener locationListener;
    private LocationManager locationManager;
    private LocationCheckThread locationCheck;
    Intent intent = new Intent("location_service");
    public LocationService() {
    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @SuppressLint("MissingPermission")
    @Override
    public void onCreate(){
        locationCheck = new LocationCheckThread();
        locationCheck.start();
        locationListener = new LocationListener() {
            @Override
            public void onLocationChanged(Location location) {
                intent.putExtra("speed", location.getSpeed());

                intent.putExtra("longitude", location.getLongitude());
                intent.putExtra("latitude", location.getLatitude());
                intent.putExtra("accuracy", location.getAccuracy());

                intent.putExtra("altitude", location.getAltitude());
                intent.putExtra("bearing", location.getBearing());
                sendBroadcast(intent);
            }

            @Override
            public void onStatusChanged(String s, int i, Bundle bundle) {

            }

            @Override
            public void onProviderEnabled(String s) {
                locationCheck.setRunning(false);
                locationCheck.interrupt();
            }

            @Override
            public void onProviderDisabled(String s) {
                if (!locationCheck.getRunning()) {
                    locationCheck.setRunning(true);
                    if(!locationCheck.isAlive()) {
                        locationCheck.start();
                    }
                }
            }
        };

        locationManager = (LocationManager) getApplicationContext().getSystemService(Context.LOCATION_SERVICE);
        locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, locationListener);
    }

    @Override
    public void onDestroy(){
        super.onDestroy();
        locationCheck.interrupt();
        locationCheck = null;
        if(locationManager == null){
            locationManager.removeUpdates(locationListener);
        }
    }

    private class LocationCheckThread extends Thread{
        private boolean running = false;

        public LocationCheckThread(){}

        public void setRunning(boolean status){
            running = status;
        }

        public boolean getRunning(){
            return running;
        }

        public void run() {
            while(!Thread.interrupted() && running) {
                if (!locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
                    //Starts settings to turn on location
                    /*Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                    startActivity(intent);*/
                    if(!((DividedByZero) getApplication()).getCameraView()) {
                        new Handler(Looper.getMainLooper()).post(new Runnable() {
                            @Override
                            public void run() {
                                Toast.makeText(LocationService.this, "Please turn on GPS. This message will repeat until GPS is turned on or DividedByZero is closed.", Toast.LENGTH_LONG).show();
                            }
                        });
                    }
                }

                try {
                    Thread.sleep(5000);
                } catch (InterruptedException e) {
                    Thread.currentThread().interrupt();
                }
            }
        }
    }
}
