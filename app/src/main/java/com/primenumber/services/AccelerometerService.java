package com.primenumber.services;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.util.Log;
import android.widget.Toast;

import com.primenumber.application.DividedByZero;

import static android.content.ContentValues.TAG;

public class AccelerometerService extends Service implements SensorEventListener{

    private double gravity = SensorManager.STANDARD_GRAVITY;
    private Intent intent;
    private SensorManager sensorManager;
    private Sensor accelerometer;
    private GForceCalc gForceCalc;
    double x = 0, y = 0, z = 0, gForce, gForceOld = 0.0;
    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        intent = new Intent("gforce");
        sensorManager = (SensorManager)getSystemService(SENSOR_SERVICE);
        accelerometer = sensorManager.getDefaultSensor(Sensor.TYPE_LINEAR_ACCELERATION);
        sensorManager.registerListener(this, accelerometer, SensorManager.SENSOR_DELAY_UI);
        gForceCalc = new GForceCalc(intent);
        gForceCalc.start();
    }

    @Override
    public void onSensorChanged(SensorEvent event) {
        if(event.sensor.getType() == Sensor.TYPE_LINEAR_ACCELERATION) {
            x = event.values[0];
            y = event.values[1];
            z = event.values[2];

            gForceCalc.run();
        }
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (gForceCalc != null){
            gForceCalc.interrupt();
        }
    }

    private class GForceCalc extends Thread{
        private Intent intent;

        public GForceCalc(Intent intent){
            this.intent = intent;
        }

        public void run(){
            double acceleration = Math.round(Math.sqrt(Math.pow(x, 2) + Math.pow(y, 2) + Math.pow(z, 2)));
            gForce = (double) Math.round((acceleration / gravity) * 100) / 100;
            //UI updates too often, trying to resolve impact on performance
            if(gForce != gForceOld) {
                intent.putExtra("gforce", gForce);
                sendBroadcast(intent);
                gForceOld = gForce;
            }
            if(gForce > 2 && ((DividedByZero) getApplicationContext()).getDrivingModeOn()){
                ((DividedByZero) getApplicationContext()).setSosModeOn(true);
                Intent intent = new Intent(AccelerometerService.this, EmergencySmsService.class);
                startService(intent);
            }
        }
    }
}
