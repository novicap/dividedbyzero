package com.primenumber.services;

import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.util.Log;
import android.widget.Toast;

import com.primenumber.data.Database;
import com.primenumber.data.models.Owner;

import java.text.SimpleDateFormat;
import java.util.Calendar;

import static android.content.ContentValues.TAG;

public class SecuritySmsService extends Service {
    private Owner owner;

    private String latitude, longitude, accuracy;
    private Database database;
    private boolean started = false, threadStarted = false;
    private SmsThread smsThread;

    private BroadcastReceiver locationReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            latitude = String.format("%.6f", (double)intent.getExtras().get("latitude"));
            longitude = String.format("%.6f", (double)intent.getExtras().get("longitude"));
            accuracy = String.format("%.1f", (float)intent.getExtras().get("accuracy"));
            if(!threadStarted) {
                smsThread.start();
            }
        }
    };
    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        database = Database.getInstance(SecuritySmsService.this);
        smsThread = new SmsThread();
        registerReceiver(locationReceiver, new IntentFilter("location_service"));
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        if(!started) {
            started = true;
            owner = database.getOwnerDetails();
        }
        return super.onStartCommand(intent, flags, startId);
    }

    protected void sendSms(Owner owner){
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd.MM.yy HH:mm:ss");

        String number = owner.getPhoneNumber();
        String message = owner.getMessage();
        message = message + "\n" +
                "Movement detected:\n" +
                "La:" + latitude + "\n" +
                "Lo:" + longitude + "\n" +
                "Acc:" + accuracy + "\n" +
                "T:" + simpleDateFormat.format(Calendar.getInstance().getTime());
        Log.d("message", "sendSmsMessage: " + message+"\n"+number);
        /*PendingIntent pi = PendingIntent.getActivity(this, 0,
                new Intent(this, EmergencySmsService.class), 0);
        SmsManager smsManager = SmsManager.getDefault();
        smsManager.sendTextMessage(number, null, message, pi, null);*/
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        threadStarted = false;
        smsThread.interrupt();
        smsThread = null;
        try{
            unregisterReceiver(locationReceiver);
        } catch (IllegalArgumentException e){
            e.printStackTrace();
        }
    }

    private class SmsThread extends Thread{
        public SmsThread(){}

        public void run() {
            threadStarted = true;
            while(threadStarted) {
                sendSms(owner);
                try {
                    Thread.sleep(5000);
                } catch (InterruptedException e) {
                    Thread.currentThread().interrupt();
                }
            }
        }
    }
}
