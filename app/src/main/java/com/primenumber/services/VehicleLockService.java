package com.primenumber.services;

import android.app.AlarmManager;
import android.app.Notification;
import android.app.PendingIntent;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.IBinder;
import android.os.SystemClock;
import android.support.annotation.Nullable;
import android.support.v4.app.NotificationCompat;
import android.util.Log;
import android.widget.Toast;

import com.primenumber.activities.UnlockActivity;
import com.primenumber.dividedbyzero.R;

import static android.content.Intent.ACTION_MAIN;

/**
 * Created by novica on 3/14/18.
 */

public class VehicleLockService extends Service {

    private BroadcastReceiver locationReceiver, accelerometerReceiver;
    private static final int NOTIFICATION_ID = 1176287;
    public static boolean isServiceRunning = false;
    private double latitude = 1000, longitude = 1000;
    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        Intent location = new Intent(getApplicationContext(), LocationService.class);
        startService(location);
        Intent accelerometer = new Intent(getApplicationContext(), AccelerometerService.class);
        startService(accelerometer);
        startServiceWithNotification();
        if(isServiceRunning){
            Intent intent = new Intent(getApplicationContext(), UnlockActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
        }
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        if (intent != null) {
            startServiceWithNotification();
        }
        else stopMyService();

        return START_STICKY;
    }

    private void startServiceWithNotification() {
        if (isServiceRunning) return;
        isServiceRunning = true;

        Intent notificationIntent = new Intent(getApplicationContext(), UnlockActivity.class);
        notificationIntent.setAction(getApplicationContext().getPackageName()+ACTION_MAIN);
        notificationIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        PendingIntent contentPendingIntent = PendingIntent.getActivity(this, 0, notificationIntent, 0);

        Notification notification = new NotificationCompat.Builder(this, "random")
                .setContentTitle(getResources().getString(R.string.app_name))
                .setTicker(getResources().getString(R.string.app_name))
                .setContentText("Dont move your phone.")
                .setSmallIcon(R.drawable.engine)
                .setContentIntent(contentPendingIntent)
                .setPriority(Notification.PRIORITY_MAX)
                .setOngoing(true)
                .build();
        notification.flags = notification.flags | Notification.FLAG_NO_CLEAR;
        if(locationReceiver == null){
            locationReceiver = new BroadcastReceiver() {
                @Override
                public void onReceive(Context context, Intent intent) {
                    if(latitude == 1000) {
                        latitude = (double)intent.getExtras().get("latitude");
                    }
                    if(longitude == 1000) {
                        longitude = (double)intent.getExtras().get("longitude");
                    }
                    //difference in fourth decimal place is about 11 meters distance
                    if((Math.abs(latitude - (double)intent.getExtras().get("latitude")) >= 0.0001 ||
                            Math.abs(longitude - (double)intent.getExtras().get("longitude")) >= 0.0001) &&
                            (latitude != 1000 && longitude != 1000)){
                        Intent securityIntent = new Intent(VehicleLockService.this, SecuritySmsService.class);
                        startService(securityIntent);
                    }
                }
            };
        }

        if(accelerometerReceiver == null){
            accelerometerReceiver = new BroadcastReceiver() {
                @Override
                public void onReceive(Context context, Intent intent) {
                if((double)intent.getExtras().get("gforce") > 0.3){
                    Intent securityIntent = new Intent(VehicleLockService.this, SecuritySmsService.class);
                    startService(securityIntent);
                }
                }
            };
        }

        registerReceiver(locationReceiver, new IntentFilter("location_service"));
        registerReceiver(accelerometerReceiver, new IntentFilter("gforce"));
        startForeground(NOTIFICATION_ID, notification);
    }

    void stopMyService() {
        stopForeground(true);
        stopSelf();
        isServiceRunning = false;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        stopMyService();
        try{
            unregisterReceiver(locationReceiver);
            unregisterReceiver(accelerometerReceiver);
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
        }
    }
}
