package com.primenumber.services;

import android.annotation.SuppressLint;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.media.MediaPlayer;
import android.os.Handler;
import android.os.IBinder;
import android.util.Log;

import com.primenumber.application.DividedByZero;
import com.primenumber.data.Database;
import com.primenumber.data.models.BlackBox;
import com.primenumber.dividedbyzero.R;

import java.io.IOException;
import java.text.DateFormat;
import java.util.Date;

/**
 * Created by novica on 2/18/18.
 */

public class BlackBoxService extends Service{

    private Handler handler;
    private BlackBox blackBox = new BlackBox();
    private Database database = Database.getInstance(this);
    private Runnable runnable = new Runnable(){
        @Override
        public void run(){
            blackBox.setCreatedAt(DateFormat.getDateTimeInstance().format(new Date()));
            database.createBlackBoxEntry(blackBox);
            handler.postDelayed(this, 1000);
        }
    };
    private BroadcastReceiver locationReceiver, obd2Receiver, accelerometerReceiver;
    private MediaPlayer mediaPlayer;
    private BluetoothOBD2Connection bluetoothOBD2Connection;
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @SuppressLint("MissingPermission")
    @Override
    public void onCreate(){
        mediaPlayer = MediaPlayer.create(BlackBoxService.this, R.raw.warning);
        handler = new Handler();
        bluetoothOBD2Connection = ((DividedByZero)getApplicationContext()).getBluetoothConnection();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        locationReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                blackBox.setLongitude((double)intent.getExtras().get("longitude"));
                blackBox.setLatitude((double)intent.getExtras().get("latitude"));
                blackBox.setSpeedGps((float)intent.getExtras().get("speed"));
                if(bluetoothOBD2Connection == null || database.getGaugeSpeedType() == 1) {
                    if ((float) intent.getExtras().get("speed") > 44.444444) //~160KM/h
                    {
                        if (!mediaPlayer.isPlaying()) {
                            mediaPlayer.start();
                        }
                    } else {
                        if (mediaPlayer.isPlaying()) {
                            mediaPlayer.stop();
                            mediaPlayer.reset();
                            mediaPlayer.release();
                            mediaPlayer = MediaPlayer.create(BlackBoxService.this, R.raw.warning);
                        }
                    }
                }
            }
        };
        obd2Receiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                blackBox.setRpm((int)intent.getExtras().get("rpm"));
                blackBox.setSpeed((int)intent.getExtras().get("speed"));
                blackBox.setGasPedalPosition((int)intent.getExtras().get("throttle_position"));
                blackBox.setCarError(String.valueOf(intent.getExtras().get("trouble_codes_string")));
                if(bluetoothOBD2Connection != null && database.getGaugeSpeedType() == 2) {
                    if ((int) intent.getExtras().get("speed") >= 160) //~160KM/h
                    {
                        if (!mediaPlayer.isPlaying()) {
                            mediaPlayer.start();
                        }
                    } else {
                        if (mediaPlayer.isPlaying()) {
                            mediaPlayer.stop();
                            mediaPlayer.reset();
                            mediaPlayer.release();
                            mediaPlayer = MediaPlayer.create(BlackBoxService.this, R.raw.warning);
                        }
                    }
                }
            }
        };
        accelerometerReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                blackBox.setGforce(String.format("%.3f", intent.getExtras().get("gforce")));
                if((double)intent.getExtras().get("gforce") > 1){
                    database.createBlackBoxEntry(blackBox);
                }
            }
        };
        registerReceiver(locationReceiver, new IntentFilter("location_service"));
        registerReceiver(obd2Receiver, new IntentFilter("obd_reading"));
        registerReceiver(accelerometerReceiver, new IntentFilter("gforce"));
        handler.postDelayed(runnable, 100);
        return super.onStartCommand(intent, flags, startId);
    }



    @Override
    public void onDestroy(){
        super.onDestroy();
        handler.removeCallbacks(runnable);
        handler = null;
        if (mediaPlayer.isPlaying()){
            mediaPlayer.stop();
        }
        mediaPlayer.reset();
        mediaPlayer.release();
        try{
            unregisterReceiver(locationReceiver);
            unregisterReceiver(obd2Receiver);
            unregisterReceiver(accelerometerReceiver);
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
        }
    }
}
