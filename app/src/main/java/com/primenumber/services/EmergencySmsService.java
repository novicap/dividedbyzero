package com.primenumber.services;

import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.util.Log;

import com.primenumber.data.Database;
import com.primenumber.data.models.EmergencySms;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;

import static android.content.ContentValues.TAG;

/**
 * Created by novica on 3/17/18.
 */

public class EmergencySmsService extends Service {
    private List<EmergencySms> emergencySmsList;

    private String latitude, longitude, accuracy;
    private Database database;
    private boolean started = false, threadStarted = false;
    private SmsThread smsThread;
    private Intent intent = new Intent("sos_status");

    private BroadcastReceiver locationReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            latitude = String.format("%.6f", (double)intent.getExtras().get("latitude"));
            longitude = String.format("%.6f", (double)intent.getExtras().get("longitude"));
            accuracy = String.format("%.1f", (float)intent.getExtras().get("accuracy"));
            if(!threadStarted){
                smsThread.start();
            }
        }
    };
    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        smsThread = new SmsThread();
        database = Database.getInstance(EmergencySmsService.this);
        registerReceiver(locationReceiver, new IntentFilter("location_service"));
        intent.putExtra("status", true);
        sendBroadcast(intent);
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        if(!started) {
            started = true;
            emergencySmsList = database.getAllEmergencySms();
        }
        return super.onStartCommand(intent, flags, startId);
    }

    protected void sendSmsMessage(List<EmergencySms> emergencySmsArrayList){
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd.MM.yy HH:mm:ss");
        for(EmergencySms sms : emergencySmsArrayList){
            String number = sms.getPhoneNumber();
            String message = sms.getMessage();
            message = message + "\n" +
                    "Send help to location:\n" +
                    "La:" + latitude + "\n" +
                    "Lo:" + longitude + "\n" +
                    "Acc:" + accuracy + "\n" +
                    "T:" + simpleDateFormat.format(Calendar.getInstance().getTime());
            Log.d("emergency ", message+"\n"+number);
            /*PendingIntent pi = PendingIntent.getActivity(this, 0,
                    new Intent(this, EmergencySmsService.class), 0);
            SmsManager smsManager = SmsManager.getDefault();
            smsManager.sendTextMessage(number, null, message, pi, null);
            Toast.makeText(getApplicationContext(), "sent sms ", Toast.LENGTH_SHORT).show();*/
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        threadStarted = false;
        smsThread.interrupt();
        smsThread = null;
        intent.putExtra("status", false);
        sendBroadcast(intent);
        try{
            unregisterReceiver(locationReceiver);
        } catch (IllegalArgumentException e){
            e.printStackTrace();
        }
    }

    private class SmsThread extends Thread{
        public SmsThread(){}

        public void run() {
            threadStarted = true;
            while(threadStarted) {
                sendSmsMessage(emergencySmsList);
                try {
                    Thread.sleep(5000);
                } catch (InterruptedException e) {
                    Thread.currentThread().interrupt();
                }
            }
        }
    }
}
