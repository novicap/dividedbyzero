package com.primenumber.services;

import android.app.Service;
import android.content.Intent;
import android.media.MediaPlayer;
import android.os.CountDownTimer;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.util.Log;

import com.primenumber.dividedbyzero.R;

/**
 * Created by novica on 4/15/18.
 */

public class DrivingSessionService extends Service {
    private MediaPlayer mediaPlayer;
    private CountDownTimer countDownTimer;
    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        //replace with warning
        mediaPlayer = MediaPlayer.create(this, R.raw.imperialmarch);
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        //4.5 hours = 16200000ms
        countDownTimer = new CountDownTimer(162000, 1000) {

            public void onTick(long millisUntilFinished) {
                //do nothing
            }

            public void onFinish() {
                if(!mediaPlayer.isPlaying()){
                    mediaPlayer.start();
                }
            }
        }.start();
        return super.onStartCommand(intent, flags, startId);
    }



    @Override
    public void onDestroy() {
        if(mediaPlayer.isPlaying()){
            mediaPlayer.stop();
            mediaPlayer.reset();
        }
        mediaPlayer.release();
        countDownTimer.cancel();
        super.onDestroy();
    }
}
