package com.primenumber.services;

import android.app.ProgressDialog;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.content.Context;
import android.content.Intent;
import android.util.Log;


import com.primenumber.data.models.Obd2Data;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.reflect.InvocationTargetException;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.UUID;
import java.util.regex.Pattern;

public class BluetoothOBD2Connection {
    private static final String TAG = "BluetoothConnectionServ";
    private final BluetoothAdapter bluetoothAdapter;
    private Context context;
    private ConnectThread connectThread;
    private BluetoothDevice hostDevice;
    private UUID deviceUUID;
    private ProgressDialog progressDialog;
    private ConnectedThread connectedThread;
    private String message;
    private Obd2Data obd2Data;
    private boolean socketConnected, errorsChecked;
    private static Pattern WHITESPACE_PATTERN = Pattern.compile("\\s");
    private static Pattern BUSINIT_PATTERN = Pattern.compile("(BUS INIT)|(BUSINIT)|(\\.)");
    private Intent intent;

    public BluetoothOBD2Connection(Context context){
        this.context = context;
        bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        intent = new Intent("obd_reading");
    }

    public synchronized void start() {

        if (connectThread != null) {
            connectThread.cancel();
            connectThread = null;
        }
    }

    private class ConnectThread extends Thread {
        private BluetoothSocket socket;

        public ConnectThread(BluetoothDevice device, UUID uuid) {
            hostDevice = device;
            deviceUUID = uuid;
        }

        public void run(){
            BluetoothSocket tmp = null;

            try {
                tmp = hostDevice.createInsecureRfcommSocketToServiceRecord(deviceUUID);
            } catch (IOException e) {
                e.printStackTrace();
            }

            socket = tmp;

            bluetoothAdapter.cancelDiscovery();
            /*
            * Made with help of unicorns, fairy dust, Buddha and Jesus Christ in person.
            * */
            try {
                socket.connect();
            } catch (IOException e) {
                //try backup
                try {
                    try {
                        socket =(BluetoothSocket) hostDevice.getClass().
                                getMethod("createRfcommSocket", new Class[] {int.class}).
                                invoke(hostDevice,1);
                    } catch (IllegalAccessException e1) {
                        e1.printStackTrace();
                    } catch (InvocationTargetException e2) {
                        e2.printStackTrace();
                    } catch (NoSuchMethodException e3) {
                        e3.printStackTrace();
                    }
                    socket.connect();
                    setSocketConnected(socket.isConnected());
                } catch (IOException e4) {
                    e4.printStackTrace();
                }
                e.printStackTrace();
            }

            connected(socket);
        }
        public void cancel() {
            try {
                socket.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    private class ConnectedThread extends Thread {
        private final BluetoothSocket socket;
        private final InputStream inStream;
        private final OutputStream outStream;
        int step = 1;
        int request = 0;


        public ConnectedThread(BluetoothSocket socket) {
            obd2Data = new Obd2Data();
            this.socket = socket;
            InputStream tmpIn = null;
            OutputStream tmpOut = null;

            try{
                progressDialog.dismiss();
            }catch (NullPointerException e){
                e.printStackTrace();
            }

            try {
                tmpIn = socket.getInputStream();
                tmpOut = socket.getOutputStream();
            } catch (IOException e) {
                e.printStackTrace();
            }

            inStream = tmpIn;
            outStream = tmpOut;
            sendRequest("00 00");
        }

        public void run(){
            byte[] buffer = new byte[1024];
            int bytes;
            while (true) {
                try {
                    bytes = inStream.read(buffer);
                    message = new String(buffer, 0, bytes);
                    message = message.replaceAll(WHITESPACE_PATTERN.pattern(), "")
                            .replaceAll(BUSINIT_PATTERN.pattern(), "");
                    obd2Data.setParameter(message);
                    if(!obd2Data.getInitialized())
                    {
                        String[] initializationSequence = obd2Data.getInitializationSequense();
                        if(message.contains(">")){
                            sendRequest(initializationSequence[step]);
                            step++;
                        }
                        if(step == 6){
                            obd2Data.setInitialized(true);
                        }
                    }

                    if(obd2Data.getInitialized()) {
                        if (message.contains(">")) {
                            sendRequest(obd2Data.getDrivingData(request));
                            intent.putExtra("rpm", obd2Data.getRpm());
                            intent.putExtra("speed", obd2Data.getSpeed());
                            intent.putExtra("engine_temperature", obd2Data.getEngineTemperature());
                            intent.putExtra("throttle_position", obd2Data.getThrotthePosition());
                            intent.putExtra("trouble_codes_string", obd2Data.getTroubleCodesString());
                            intent.putExtra("trouble_codes_checked", obd2Data.getErrorsChecked());
                            context.sendBroadcast(intent);
                            request++;
                        }
                        if(request == 50){
                            request = 0;
                        }
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                    break;
                }
            }
        }

        public void sendRequest(String request) {
            try {
                request+="\r";
                outStream.write(request.getBytes(Charset.defaultCharset()));
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        public void cancel() {
            try {
                socket.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    private void connected(BluetoothSocket socket) {
        connectedThread = new ConnectedThread(socket);
        connectedThread.start();
    }

    public void startClient(BluetoothDevice device,UUID uuid){
        progressDialog = ProgressDialog.show(context,"Connecting Bluetooth"
                ,"Please Wait...",true);

        connectThread = new ConnectThread(device, uuid);
        connectThread.start();
    }

    public void setSocketConnected(boolean socketConnected){
        this.socketConnected = socketConnected;
    }

    public int getRpm(){
        return obd2Data.getRpm();
    }

    public int getSpeed(){
        return obd2Data.getSpeed();
    }

    public void setErrorCodesCheck(int status){
        obd2Data.setErrorCodesChecked(status);
    }

    public ArrayList<String> getTroubleCodes(){
        return obd2Data.getTroubleCodes();
    }
}
